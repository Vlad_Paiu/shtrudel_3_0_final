#!/bin/bash

SRC_DIR=/usr/local/src
OS_SRC_DIR=$SRC_DIR/opensips_shtrudel
GIT_REPO=https://shtrudel.unfuddle.com/git/shtrudel_voicenter-opensips/
GIT_BRANCH=2.4
ETC_FOLDER=/usr/local/etc/opensips/

# dependencies
apt-get install vim git curl libncurses-dev build-essential flex bison libmysqlclient-dev pkg-config libpcre++-dev libjson-c-dev libmicrohttpd-dev librabbitmq-dev libxml2-dev libcurl4-openssl-dev ctags uuid-dev mysql-server libhiredis-dev redis-server cmake

cd $SRC_DIR

wget https://github.com/alanxz/rabbitmq-c/archive/v0.9.0.tar.gz
tar -xzvf v0.9.0.tar.gz
cd rabbitmq-c-0.9.0
mkdir build && cd build
cmake ..
cmake --build . --target install
echo "/usr/local/lib/x86_64-linux-gnu/" >> /etc/ld.so.conf.d/x86_64-linux-gnu.conf
ldconfig

# fetch sources & scripts
git clone $GIT_REPO -b $GIT_BRANCH $OS_SRC_DIR
cd $OS_SRC_DIR

cp deploy_scripts/opensips/Makefile.conf .
make install

# init script
cp deploy_scripts/opensips/opensips.init /etc/init.d/opensips
chmod +x /etc/init.d/opensips
groupadd opensips
useradd -g opensips  opensips

cp deploy_scripts/opensips/opensips.default /etc/default/opensips
mkdir -p /usr/local/etc/opensips/core_files/
cp -r deploy_scripts/opensips/tls/ /usr/local/etc/opensips/
chown -R opensips:opensips /usr/local/etc/opensips/tls/

# ctl & DB schema
sed -i -e "s/# DBENGINE=MYSQL/DBENGINE=MYSQL/g" /usr/local/etc/opensips/opensipsctlrc
sed -i -e "s/# DBPORT=3306/DBPORT=3306/g" /usr/local/etc/opensips/opensipsctlrc
sed -i -e "s/# DBHOST=localhost/BHOST=localhost/g" /usr/local/etc/opensips/opensipsctlrc
sed -i -e "s/# DBNAME=opensips/DBNAME=opensips/g" /usr/local/etc/opensips/opensipsctlrc
sed -i -e "s/# DBRWUSER=opensips/DBRWUSER=opensips/g" /usr/local/etc/opensips/opensipsctlrc
sed -i -e "s/# DBRWPW/DBRWPW/g" /usr/local/etc/opensips/opensipsctlrc
sed -i -e "s/# DBOOTUSER/DBROOTUSER/g" /usr/local/etc/opensips/opensipsctlrc
/usr/local/sbin/opensipsdbctl create

# deploy scripts, fix them & start
cp deploy_scripts/opensips/local.m4.template deploy_scripts/opensips/opensips_proxy.m4 $ETC_FOLDER

ETH0IP=`/sbin/ifconfig eth0 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}'`
ETH1IP=`/sbin/ifconfig lo | grep 'inet addr' | cut -d: -f2 | awk '{print $1}'`

sed -i -e "s/ETH0IP/$ETH0IP/g" $ETC_FOLDER/local.m4.template
sed -i -e "s/ETH1IP/$ETH1IP/g" $ETC_FOLDER/local.m4.template
m4 $ETC_FOLDER/local.m4.template $ETC_FOLDER/opensips_proxy.m4 > $ETC_FOLDER/opensips_proxy.cfg

# TODO rtpproxy

service opensips start
