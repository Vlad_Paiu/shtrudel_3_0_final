# OfficeRing OpenSIPS....
# 
# Please refer to the Core CookBook at:
#      http://www.opensips.org/Resources/DocsCookbooks
# for a explanation of possible statements, functions and parameters.
#


####### Global Parameters #########

log_level=3
mem_log=6
mem_dump=3

log_stderror=no
log_facility=LOG_LOCAL0

children=4

#mhomed=1

/* uncomment the following lines to enable debugging */
#debug_mode=yes

/* uncomment the next line to enable the auto temporary blacklisting of
   not available destinations (default disabled) */
#disable_dns_blacklist=no

/* uncomment the next line to enable IPv6 lookup after IPv4 dns
   lookup failures (default disabled) */
#dns_try_ipv6=yes

/* comment the next line to enable the auto discovery of local aliases
   based on revers DNS on IPs */
auto_aliases=no

tcp_no_new_conn_bflag = NO_NEW_CONN

listen=udp:ANYCAST_IP:5060 anycast
#listen=udp:209.163.136.201:5060
listen=ws:MY_IP:8887
listen=wss:MY_IP:8888
listen=ws:ANYCAST_IP:8887
listen=wss:ANYCAST_IP:8888
listen=udp:MY_IP:5060
listen=udp:MY_PRIV_IP:5060



####### Modules Section ########

#set module path
mpath="/usr/local/lib64/opensips/modules/"

#### SIGNALING module
loadmodule "signaling.so"

#### StateLess module
loadmodule "sl.so"

#### Transaction Module
loadmodule "tm.so"
modparam("tm", "fr_timeout", 5)
modparam("tm", "fr_inv_timeout", 120)
modparam("tm", "restart_fr_on_each_reply", 0)
modparam("tm", "onreply_avp_mode", 1)
modparam("tm", "tm_replication_cluster", 1)

#### Record Route Module
loadmodule "rr.so"
/* do not append from tag to the RR (no need for this script) */
modparam("rr", "append_fromtag", 1)

#### MAX ForWarD module
loadmodule "maxfwd.so"

#### SIP MSG OPerationS module
loadmodule "sipmsgops.so"

#### FIFO Management Interface
loadmodule "mi_fifo.so"
#modparam("mi_fifo", "fifo_name", "/tmp/opensips_proxy_fifo")
modparam("mi_fifo", "fifo_name", "/tmp/opensips_fifo")
modparam("mi_fifo", "fifo_mode", 0666)

#### HTTPD server
loadmodule "httpd.so"
#### UAC
loadmodule "uac.so"
modparam("uac","restore_mode","auto")

#### MYSQL module
loadmodule "db_mysql.so"

#### USeR LOCation module
loadmodule "usrloc.so"
modparam("usrloc", "nat_bflag", "DST_NAT")
#modparam("usrloc", "db_mode",   2)
modparam("usrloc", "db_url",
	"mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME")
modparam("usrloc","working_mode_preset","full-sharing-cluster")
modparam("usrloc", "location_cluster", 1)

#### REGISTRAR module
loadmodule "registrar.so"
modparam("registrar", "tcp_persistent_flag", "TCP_PERSISTENT")
modparam("registrar", "received_avp", "$avp(received)")
modparam("registrar", "attr_avp", "$avp(subs_attrs)")
modparam("registrar", "min_expires", 10)

#### AUTHentication modules
loadmodule "auth.so"
modparam("auth", "calculate_ha1", yes)
loadmodule "auth_db.so"
modparam("auth_db", "calculate_ha1", yes)
modparam("auth_db", "password_column", "password")
modparam("auth_db|uri", "db_url",
	"mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME")
modparam("auth_db", "cachedb_url", "local://")
#subscriber mi commands
modparam("auth_db","reload_procedure","sp_upsert_sub")
modparam("auth_db","delete_procedure","sp_delete_subscriber")

modparam("auth_db", "load_credentials", "password;ds_set;pstn_ds_set;ds_alg;accountid;rtpproxy_enabled;max_contacts;$avp(subs_attrs)=attrs;pstn_dialplan")
modparam("auth", "username_spec", "$avp(auth_username)")
modparam("auth", "password_spec", "$avp(auth_password)")

modparam("auth", "disable_nonce_check", 1)


#### DOMAIN module
loadmodule "domain.so"
modparam("domain", "db_url",
	"mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME")
modparam("domain", "db_mode", 1)   # Use caching
modparam("auth_db|usrloc|uri", "use_domain", 1)
modparam("domain","upsert_procedure","sp_upsert_domain")
modparam("domain","delete_procedure","sp_delete_domain")


#### DIALOG module
loadmodule "dialog.so"
modparam("dialog", "dlg_match_mode", 1)
modparam("dialog", "default_timeout", 21600)  # 6 hours timeout
modparam("dialog", "db_mode", 2)
modparam("dialog", "db_url",
	"mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME")
modparam("dialog", "profiles_with_value", "device/b ; caller ; callee ; subscriber; tags")
modparam("dialog", "dialog_replication_cluster", 1)

loadmodule "topology_hiding.so"
modparam("topology_hiding", "force_dialog", 1)

#### Dispatcher module
loadmodule "dispatcher.so"
modparam("dispatcher", "db_url",
	"mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME")
modparam("dispatcher", "persistent_state", 1)
modparam("dispatcher", "ds_ping_interval", 5)
modparam("dispatcher", "ds_ping_method", "OPTIONS")
modparam("dispatcher", "ds_ping_from", "sip:PingTest@ANYCAST_IP")
modparam("dispatcher", "ds_probing_threshhold", 3)
modparam("dispatcher", "ds_probing_mode", 1)
modparam("dispatcher","upsert_procedure","sp_upsert_dispatcher")
modparam("dispatcher","delete_procedure","sp_delete_dispatcher")
modparam("dispatcher", "attrs_avp", "$avp(ds_attrs)")
modparam("dispatcher", "script_attrs_avp", "$avp(ds_script_attrs)")
#### Permisssions module
loadmodule "permissions.so"
modparam("permissions", "db_url",
	"mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME")

#### UAC Auth module
loadmodule "uac_auth.so"

#### NAT Helper module
loadmodule "nathelper.so"
modparam("nathelper", "natping_interval", 30)
modparam("nathelper", "ping_nated_only", 1)
modparam("nathelper", "received_avp", "$avp(received)")
modparam("nathelper", "sipping_bflag", "DST_NAT")
modparam("nathelper", "sipping_from", "sip:pinger@ANYCAST_IP")
#modparam("nathelper", "sipping_from_timestep_tag", "sgt")
modparam("nathelper", "remove_on_timeout_bflag", "RM_ONTO_FLAG")
modparam("nathelper", "sipping_latency_flag", "SIPPING_LATENCY_FLAG")


#### Core Presence modules
loadmodule "presence.so"
loadmodule "presence_xml.so"
modparam("presence","db_url",
	"mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME")
modparam("presence","server_address","sip:ANYCAST_IP:5060")
modparam("presence_xml","force_active",1)

modparam("presence", "cluster_id", CLUSTER_ID)
modparam("presence", "cluster_federation_mode", 1)
#modparam("presence", "cluster_pres_events" ,"dialog;sla")
modparam("presence", "db_update_period", 5)


#### Presence dialoginfo module
loadmodule "presence_dialoginfo.so"


#### Presence MWI module
loadmodule "presence_mwi.so"

#### pua and pua_dialoginfo parameters --
loadmodule "pua.so"
loadmodule "pua_dialoginfo.so"
modparam("pua","db_url",
	"mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME")
modparam("pua_dialoginfo", "presence_server", "sip:ANYCAST_IP:5060")
modparam("pua_dialoginfo", "callee_spec_param", "$avp(blf_callee)")
modparam("pua_dialoginfo", "caller_spec_param", "$avp(blf_caller)")

#### XCap parameters --
loadmodule "xcap.so"
modparam("xcap","db_url",
	"mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME")

#### TEXT OPerationS --
loadmodule "textops.so"

#### AVP OPerationS --
loadmodule "avpops.so"
modparam("avpops","db_url",
	"mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME")

#### RTPPROXY controller --
loadmodule "rtpproxy.so"
modparam("rtpproxy","db_url",
	"mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME")
#modparam("rtpproxy", "rtpproxy_autobridge", 1)
modparam("rtpproxy", "nortpproxy_str", "")
modparam("rtpproxy","upsert_procedure","sp_upsert_rtpproxy")
modparam("rtpproxy","delete_procedure","sp_delete_rtpproxy")

loadmodule "rtpengine.so"
modparam("rtpengine", "rtpengine_sock", "0 == udp:192.168.201.233:22222")
modparam("rtpengine", "rtpengine_sock", "1 == udp:192.168.201.233:22222")
modparam("rtpengine", "rtpengine_sock", "2 == udp:192.168.201.233:22222")



loadmodule "drouting.so"
modparam("drouting", "use_partitions", 1)
modparam("drouting", "no_concurrent_reload", 1)
modparam("drouting", "db_partitions_table", "dr_partitionsView")
modparam("drouting", "db_partitions_url", "mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME")

modparam("drouting", "mi_upsert_procedure", "sp_upsert_speeddial")
modparam("drouting", "mi_delete_procedure", "sp_delete_speeddial")


#### TLS certificates ManaGeMent --
loadmodule "tls_mgm.so"
modparam("tls_mgm", "server_domain", "ratetel.com")
#modparam("tls_mgm", "certificate", "[ratetel.com]TLS_PATH/officering.crt")
#modparam("tls_mgm", "private_key", "[ratetel.com]TLS_PATH/officering.key")

modparam("tls_mgm", "certificate", "[ratetel.com]TLS_PATH/officeringnet.crt")
modparam("tls_mgm", "private_key", "[ratetel.com]TLS_PATH/officeringnet.key")
modparam("tls_mgm", "require_cert", "[ratetel.com]0")
modparam("tls_mgm", "verify_cert", "[ratetel.com]0")
modparam("tls_mgm", "tls_method", "[ratetel.com]TLSv1_2")
#modparam("tls_mgm", "ciphers_list", "NULL")

#### XML body mangler
loadmodule "xml.so"

#### EVENT ROUTE handling
loadmodule "event_route.so"

modparam("httpd","ip","MY_PRIV_IP")
modparam("httpd","port",8080)

loadmodule "mi_http.so"

loadmodule "cfgutils.so" 
loadmodule "event_routing.so"
loadmodule "rest_client.so"
loadmodule "mathops.so"

loadmodule "proto_hep.so"
modparam("proto_hep", "hep_id",
"[hep_dst] 192.168.201.87:5080; transport=udp; version=3")

loadmodule "tracer.so"
modparam("tracer", "trace_on", 1)
modparam("tracer", "trace_id",
"[hep_tracer]uri=hep:hep_dst")

listen=hep_udp:MY_PRIV_IP:5080

#### RABBITMQ events
loadmodule "rabbitmq.so"
#modparam("rabbitmq", "server_id", "[rabbitmq_1] heartbeat = 3;
#	 exchange = RMQ_EXCH;
#	 uri = amqp://RMQ_USER:RMQ_PASS@RMQ_HOST:RMQ_PORT/%2f")

modparam("rabbitmq", "server_id", "[rabbitmq_location1] heartbeat = 3;
	exchange = RMQ_EXCH_LOCATION;
	frames = 262144; 
	retries = 3;
	timeout = 300;
        uri = amqp://RMQ_USER:RMQ_PASS@RMQ_HOST:RMQ_PORT/%2f")

modparam("rabbitmq", "server_id", "[rabbitmq_realtime1] heartbeat = 3;
        exchange = RMQ_EXCH_REALTIME;
        retries = 3;
        timeout = 300;
        uri = amqp://RMQ_USER:RMQ_PASS@RMQ_HOST:RMQ_PORT/%2f")


modparam("rabbitmq", "server_id", "[rabbitmq_cdr1] heartbeat = 3;
        exchange = RMQ_EXCH_CDR;
	frames = 262144; 
        retries = 3;
        timeout = 300;
        uri = amqp://CDR_RMQ_USER:CDR_RMQ_PASS@CDR_RMQ_HOST:CDR_RMQ_PORT/%2f")

modparam("rabbitmq", "server_id", "[rabbitmq_ratelimit1] heartbeat = 3;
        exchange = RMQ_EXCH_RATELIMIT;
	frames = 262144; 
        retries = 3;
        timeout = 300;
        uri = amqp://RATELIMIT_RMQ_USER:RATELIMIT_RMQ_PASS@RATELIMIT_RMQ_HOST:RATELIMIT_RMQ_PORT/%2f")


modparam("rabbitmq", "server_id", "[rabbitmq_location2] heartbeat = 3;
        exchange = RMQ_EXCH_LOCATION;
	frames = 262144; 
        retries = 3;
        timeout = 300;
        uri = amqp://RMQ_USER2:RMQ_PASS2@RMQ_HOST2:RMQ_PORT2/%2f")

modparam("rabbitmq", "server_id", "[rabbitmq_realtime2] heartbeat = 3;
        exchange = RMQ_EXCH_REALTIME;
	frames = 262144; 
        retries = 3;
        timeout = 300;
        uri = amqp://RMQ_USER2:RMQ_PASS2@RMQ_HOST2:RMQ_PORT2/%2f")

modparam("rabbitmq", "server_id", "[rabbitmq_cdr2] heartbeat = 3;
        exchange = RMQ_EXCH_CDR;
	frames = 262144; 
        retries = 3;
        timeout = 300;
        uri = amqp://RMQ_USER:RMQ_PASS@RMQ_HOST:RMQ_PORT/%2f")

modparam("rabbitmq", "server_id", "[rabbitmq_mi1] heartbeat = 3;
        exchange = opensips_mi_request;
	frames = 262144; 
        retries = 3;
        timeout = 300;
        receiving_queue = RMQ_MI_REQUEST;
	topic = RMQ_MI_TOPIC;
        uri = amqp://RMQ_USER:RMQ_PASS@RMQ_HOST:RMQ_PORT2/%2f")

modparam("rabbitmq", "server_id", "[rabbitmq_mi2] heartbeat = 3;
        exchange = opensips_mi_request;
	frames = 262144; 
        retries = 3;
        timeout = 300;
        receiving_queue = RMQ_MI_REQUEST;
	topic = RMQ_MI_TOPIC;
	uri = amqp://RMQ_USER2:RMQ_PASS2@RMQ_HOST2:RMQ_PORT2/%2f")
modparam("rabbitmq", "server_id", "[rabbitmq_mi_response1] heartbeat = 3;
        exchange = opensips_mi_response;
	frames = 262144; 
        retries = 3;
        timeout = 300;
        uri = amqp://RMQ_USER:RMQ_PASS@RMQ_HOST:RMQ_PORT2/%2f")

modparam("rabbitmq", "server_id", "[rabbitmq_mi_response2] heartbeat = 3;
        exchange = opensips_mi_response;
	frames = 262144; 
        retries = 3;
        timeout = 300;
        uri = amqp://RMQ_USER2:RMQ_PASS2@RMQ_HOST2:RMQ_PORT2/%2f")

modparam("rabbitmq", "server_id", "[rabbitmq_ip1] heartbeat = 3;
	exchange = RMQ_EXCH_SECURITY;
	frames = 262144; 
	retries = 3;
	timeout = 300;
        uri = amqp://RMQ_USER:RMQ_PASS@RMQ_HOST:RMQ_PORT/%2f")

modparam("rabbitmq", "server_id", "[rabbitmq_ip2] heartbeat = 3;
        exchange = RMQ_EXCH_SECURITY;
	frames = 262144; 
        retries = 3;
        timeout = 300;
        uri = amqp://RMQ_USER2:RMQ_PASS2@RMQ_HOST2:RMQ_PORT2/%2f")

loadmodule "proto_udp.so"
loadmodule "proto_tls.so"
loadmodule "proto_ws.so"
loadmodule "proto_wss.so"

loadmodule "pua_mi.so"
modparam("pua_mi", "presence_server", "sip:ANYCAST_IP:5060")
modparam("pua_mi", "db_url",
	"mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME")
modparam("pua_mi", "cachedb_url", "local://")


#### dialog variables module
loadmodule "cachedb_local.so"
#modparam("cachedb_local", "cachedb_url", "local://")

#### cachedb_redis module
loadmodule "cachedb_redis.so"
modparam("cachedb_redis", "cachedb_url","redis://localhost:6379/")

#### json module
loadmodule "json.so"

#### SQL CACHER module
#modparam("sql_cacher", "cache_table", "id=repl_sets
#db_url=mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME
#cachedb_url=local://
#table=vw_get_publish_group_servers
#key=groupid
#columns=servers
#on_demand=0")
#modparam("sql_cacher", "full_caching_expire", 3600)

#loadmodule "snmpstats.so"
#modparam("snmpstats", "sipEntityType", "registrarServer")
#modparam("snmpstats", "snmpgetPath", "/usr/bin/")
#modparam("snmpstats", "snmpCommunity", "starkeypublic")

loadmodule "clusterer.so"
modparam("clusterer", "db_url",
	"mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME")
modparam("clusterer", "my_node_id", MY_CLUSTER_NODE_ID)
modparam("clusterer", "sharing_tag", "MY_CLUSTER_NODE_ID/MY_CLUSTER_NODE_ID=active")

loadmodule "proto_bin.so"
listen = bin:MY_PRIV_IP:5555


loadmodule "acc.so"
modparam("acc", "report_cancels", 1)
modparam("acc", "detect_direction", 1)
modparam("acc", "extra_fields", "evi: from_uri ; to_uri ; domain ; ruri ; src_ip ; src_port ; accountid; direction; rpi; contact ; rtpproxy_setid ; original_callid; uid ; guid")

loadmodule "pike.so"
modparam("pike", "sampling_time_unit", 60)
modparam("pike", "reqs_density_per_unit",100)
modparam("pike", "remove_latency", 4320)

loadmodule "exec.so"

loadmodule "auth_jwt.so"
modparam("auth_jwt", "tag_claim", "iss")
modparam("auth_jwt", "load_credentials", "profile_info")
modparam("auth_jwt", "db_url","mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME")
modparam("auth_jwt", "mi_upsert_procedure", "upsert_jwt_data")
modparam("auth_jwt", "mi_delete_procedure", "delete_jwt_data")
modparam("auth_jwt", "cachedb_url", "local://")
modparam("auth_jwt", "cachedb_ttl", 3600)

loadmodule "dialplan.so"
modparam("dialplan", "db_url","mysql://DB_USER:DB_PASS@DB_HOST/DB_NAME")

loadmodule "ratelimit.so"
modparam("ratelimit", "timer_interval", 20)
modparam("ratelimit", "limit_per_interval", 0)


####### Routing Logic ########

# main request routing logic

startup_route {
	avp_db_query("select profile_id,ua from opensips.banned_uas","$avp(profile_id),$avp(ua)");
	$var(i) = 0;
	while ($(avp(profile_id)[$var(i)]) != NULL) {
		xlog("XXXX - caching $(avp(profile_id)[$var(i)])_$(avp(ua)[$var(i)]) \n");
		cache_store("local","bannedua_$(avp(profile_id)[$var(i)])_$(avp(ua)[$var(i)])","1");
		$var(i) = $var(i) + 1;
	}
}

route{
	xlog("SCRIPT:DBG: incoming $rm from $si:$sp, from $fu to $ru ($ci) \n");
		$log_level=6;
#		$var(token) = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNjE2MjM5MDIyLCJ0YWciOiJ2bGFkIiwiYXBwX2RhdGEiOnsidGFyZ2V0IjoyOCwidGFyZ2V0X3R5cGUiOiJDYWxsR3JvdXAifX0.z_BlokxyC8qbR2LDSaR1xYazRRkVZw5Flw766lnGQhY";
		$var(token) = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImJjNDk1MzBlMWZmOTA4M2RkNWVlYWEwNmJlMmNlNDM3ZjQ5YzkwNWUiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiNzcyNTkxNTgyNDI0LTZwaGYwcDJrcWpiYWJnYmhxdm9kbjFjcm9kMGdpZmQ4LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiNzcyNTkxNTgyNDI0LTZwaGYwcDJrcWpiYWJnYmhxdm9kbjFjcm9kMGdpZmQ4LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTA1NDExNDA2MDY1NTY0MTk2MzI3IiwiaGQiOiJzdGFya2V5LmNvLmlsIiwiZW1haWwiOiJzaGxvbWlAc3RhcmtleS5jby5pbCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoiRXFHcDFGck4wRFdBZnVjdVhlYWxZZyIsImlhdCI6MTU5OTUxNDg1NywiZXhwIjoxNTk5NTE4NDU3fQ.kUVx3fVrtibeuXMXkJAJ8MwVWabSpzCwkaMYPu7VAR7MrhYEzf2oa4hx9HG_Yqg0QoPOb25IefKrXlgvscIZe88OwuvxXYZml3PI3-TwL5b9Xgvuu7PXJkaUsubo7rBCtacfzPsJCov8XDr3VrVbYv43tFUcS-hTd-BDdUMcCHB1oiM8TP2SD1HtcyPiXfu6jpOfV287tPjz1q866m87o2oOZzEk9MpGu7opYsnp-TOnKE8QVkTLqCxj_4ecsccHI53q9guSLxUbhlnsnaTnsNsNUVJ1CZ55hXj6pCVZ8hDhivoiiJGPmZqhv4Xl7LtSj1yK7XtXPt56ymBV26hd5w";

		xlog("XXXX - doing big JWT auth \n");
                if (jwt_authorize($var(token),$var(out_token),$var(out_sip)))
                        xlog("XXXX - jwt success\n");
		else
                        xlog("XXXX - jwt failure\n");
		$log_level=3;
		


#	$var(json1) = "{\"test\":\"vlad\",\"attrs\":{\"attrs\" : { \"cc\" : 10, \"headers\" : { \"key\" : \"val\", \"test\" : \"v1\"}}}}";
#        $var(json2) = "{\"test\":\"vlad2\",\"m\":\"v\",\"attrs\":{\"attrs\" : { \"headers\" : { \"test\" : \"v2\"}}}}";
	$var(json1) = "{\"password\": \"00!alex1!00\",\"ds_set\": \"1\",\"pstn_ds_set\": \"55\",\"ds_alg\": \"0\",\"accountid\": \"7594\",\"rtpproxy_enabled\": \"1\",\"max_contacts\": \"10\",\"attrs\": {\"attrs\": {\"cc\": 10,\"headers\": {\"v_u1\": \"1\",\"v_u2\": \"2\",\"UserID\": 455,\"UserName\": \"test\",\"pstn_911\": \"8329008615\",\"pstn_flag\": \"3\"},\"UserName\": \"455\",\"caller_name\": \"alex1alex1\",\"pstn_ds_set\": 55,\"rmq_headers\": {\"test\": \"Vlad\",\"2ndHdr\": \"2ndVal\"	},\"sms_caller_id\": \"12128765432\"},\"extensions\": [\"101\", \"1\"]	}}";
	$var(json2) = "{\"attrs\": {\"attrs\": {\"app_data\": {\"app_name\": \"CallGroup\",\"app_target\": \"alex2\",\"app_target_id\": 28,\"app_target_type\": \"exten\"},\"headers\": {\"UserID\": 441}}},\"iat\": 1616239022,\"name\": \"John Doe\",	\"sub\": \"1234567890\",\"tag\": \"vlad\"}";

	json_merge("$var(json1)","$var(json2)",$var(out_json));
	xlog("XXXX - merged = $var(out_json) \n");




	force_rport();

	if (!mf_process_maxfwd_header(10)) {
		sl_send_reply(483,"Too Many Hops");
		exit;
	}

	## NAT detection
	if (nat_uac_test(7) || $proto == "wss") {
		setflag(SRC_NAT);
		if ( !is_method("REGISTER") )
			fix_nated_contact();
	}

	if ($proto == "wss")
		setflag(SRC_WS);

	if (has_totag()) {
		# sequential request withing a dialog should
		# take the path determined by record-routing
	    	 if ($DLG_status==NULL && !match_dialog() ) {
                    xlog(" cannot match request to a dialog $ci \n");
             }

		xlog("XXX - received $rm for call $ci - $mb \n");
		if (loose_route() || match_dialog() || topology_hiding_match()) {
			xlog("XXX - matched $rm for call $ci \n");
			if (!validate_dialog())
				fix_route_dialog();

			if (is_dlg_flag_set(1)) {
				xlog("XXXX - we do have rtp for $rm - $ci \n");
			}
			# handle RTPP proxy insertion for re-INVITEs
			if ((is_method("INVITE") || is_method("UPDATE")) && is_dlg_flag_set(1)) {
				xlog("XXXX - forcing rtp for $rm - $ci \n");
				route(insert_rtpproxy);
			}

			if (is_method("BYE")) {
				$json(dlg_ctx) := $DLG_ctx_json;
				xlog("XXX - BYE for $ci - $DLG_ctx_json ; actual json : $json(dlg_ctx) \n");
				$avp(rtpproxy_set) = $(dlg_val(rtpproxy_set){s.int});
				rtpproxy_unforce($avp(rtpproxy_set));
			}
			# route it out to whatever destination was set by loose_route()
			# in $du (destination URI).
			xlog("SCRIPT:DBG: sequential $rm sent to $ru/$du $ci \n");
###test header in refer

			if (is_method("REFER") && is_present_hf("Refer-To")) {
				$var(refer_hdr) = $hdr(Refer-To);
				$var(target_refer) = $(rt{uri.headers}{s.unescape.param});

				$var(target_callid) = $(var(target_refer){param.value,Replaces});
				$var(target_ftag) = $(var(target_refer){param.value,from-tag}); 
				$var(target_ttag) = $(var(target_refer){param.value,to-tag}); 

				xlog("REFER XXXX - found target call $var(target_callid) \n");

				$avp(dlg_val_names) = "freeswitch_box";

				if ($var(target_callid) != NULL) {
					if (get_dialog_json($var(target_callid),$avp(out_json))) {
						$json(refer_dlg) := $avp(out_json);
						
						$var(ip) = $(json(refer_dlg/values/freeswitch_box){uri.host});
						$var(reg) = "/c7594.ratetel.com/" + $var(ip) + "/g";

						xlog("REFER XXXX - found box $var(ip) for target call $var(target_callid) - built regex $var(reg) \n");

						remove_hf("Refer-To");
						append_hf("Refer-To: $(var(refer_hdr){re.subst,$var(reg)}) \r\n");
					}
				} else {
					# blind transfer 
					$avp(auth_username) = $(hdr(Referred-By){nameaddr.uri}{uri.user});
					$avp(auth_domain) = $(hdr(Referred-By){nameaddr.uri}{uri.host});
					if (cache_fetch("local","auth_$avp(auth_username)",$avp(auth_info))) {
						xlog("XXXX - REFER to found auth info for auth_$avp(auth_username)@$avp(auth_domain) \n");

						$json(auth_info) := $avp(auth_info);		
						$avp(pstn_ds_set) = $json(auth_info/pstn_ds_set);
						$avp(accountid) = $json(auth_info/accountid);
						$avp(subs_attrs) = $json(auth_info/attrs);

						if ( $rt=~"^sip:\+1[1-9][0-9]{9}@" || $rt=~"^sip:1[1-9][0-9]{9}@" || $rt=~"^sip:[1-9][0-9]{9}@" || $rt=~"^sip:011[1-9][0-9]{7}*@" ||   $rt=~"^sip:[2-9]11@") {
							xlog("XXX - REFER to pstn to $rt from $hdr(Referred-By) \n");
	
							remove_hf("Referred-By");
							$var(new_body) = "<sip:" + $avp(auth_username) + "@" + $avp(auth_domain) + "?";
							$var(new_body) = $var(new_body) + "X_CUSTOMER=" + $avp(accountid) + ";";
							$var(new_body) = $var(new_body) + "X_PSTN_DS_SET=" + $avp(pstn_ds_set);

							$json(subs_attrs) := $avp(subs_attrs);

							$json(headers_info) := $json(subs_attrs/attrs/headers);

							if ($json(headers_info) != NULL) {
								for ($var(k) in $(json(headers_info.keys)[*])) {

									$var(new_body) = $var(new_body) + ";X_" + $(var(k){s.toupper}{re.subst,/-/_/g}) + "=" + $json(headers_info/$var(k));
								} 
							}

							$var(old_ru) = $ru;
							$rU = $(rt{uri.user});
							if ($rt=~"^sip:\+1[1-9][0-9]{9}@")
								strip(1);
							else if ($rt=~"^sip:[1-9][0-9]{9}@")
								prefix("1");

							xlog("XXXX - looking up $rU through the A-Z dialplan \n");
							$avp(partition) = "a_z_dialplan";
							if (do_routing(0,"C",,$avp(rule_attrs),,,$avp(partition))) {
								$json(pstn_data) := $avp(rule_attrs);

								if ($json(pstn_data) != NULL) {
									for ($var(k) in $(json(pstn_data.keys)[*])) {
										$var(new_body) = $var(new_body) + ";X_" + $(var(k){s.toupper}{re.subst,/-/_/g}) + "=" + $json(pstn_data/$var(k));
									} 
								}
							}

							$ru = $var(old_ru);

							$var(new_body) = $var(new_body) + ">";

							append_hf("Referred-By: $var(new_body) \r\n");
						} else {
							xlog("XXX - REFER to extension to $rt from $hdr(Referred-By) \n");

							remove_hf("Referred-By");
							$var(new_body) = "<sip:" + $avp(auth_username) + "@" + $avp(auth_domain) + "?";
							$var(new_body) = $var(new_body) + "X_CUSTOMER=" + $avp(accountid);
					
							$var(orig_ru) = $ru;
							$rU = $(rt{uri.user});

							$avp(partition) = "account_" + $avp(accountid);
							if (do_routing(1,"LC",,$avp(rule_attrs),,,$avp(partition))) {
								$json(rule_attrs) := $avp(rule_attrs);
								$json(app_data) := $json(rule_attrs/app_data);

								if ($json(app_data) != NULL) {
									for ($var(k) in $(json(app_data.keys)[*])) {
										$var(new_body) = $var(new_body) + ";X_" + $(var(k){s.toupper}{re.subst,/-/_/g}) + "=" + $json(app_data/$var(k));
									}	
								}
							}

							$ru = $var(orig_ru);
							$var(new_body) = $var(new_body) + ">";
							append_hf("Referred-By: $var(new_body) \r\n");
						}
					}
				}
			}

			if (is_method("INVITE")  ) {route(update_dialog_vars);}
			route(relay);
		} else {
			if (is_method("SUBSCRIBE") && is_myself("$rd") && $hdr(Event)=="dialog") {
				# in-dialog subscribe requests for BLF
				route(handle_local_presence, 0);
				exit;
			}
			if ( is_method("ACK") ) {
				if ( t_check_trans() ) {
					# non loose-route, but stateful ACK; must be an ACK after
					# a 487 or e.g. 404 from upstream server
					       	if( $dlg_val(socket_name) == "INTERNAL"){
                                	force_send_socket(udp:MY_PRIV_IP:5060);
                                		xlog("L_ERR",
                                    		"ACK  INTERNAL force_send_socket udp:MY_PRIV_IP:5060 \n [$fu/$tu/$ru/$ci] \n " );
                              } else {
					        t_anycast_replicate();
					        exit;
				}
                    	   ## 	route(update_dialog_vars);

                    t_relay();
					exit;
				} else {
					# ACK without matching transaction ->
					# ignore and discard
						xlog("L_ERR",	"incoming $rm without matching transaction  $dlg_val(socket_name)  \n [$fu/$tu/$ru/$ci] \n " );
					exit;
				}
			}
			sl_send_reply(404,"Not here");
		}
		exit;
	}

	# CANCEL processing
	if (is_method("CANCEL")) {
		route(update_dialog_vars);
		if (t_check_trans()) {
			xlog("XXXXX - here - relaying cancel for call $ci \n");
			t_relay();
		}

		exit;
	}

	t_check_trans();
	if (is_method("CANCEL")) {
		xlog("XXXXX - CANCEL got here - reject it for $ci \n");
		send_reply(481,"Call/Transaction Does Not Exist");
		exit;
	}

	# preloaded route checking
	if (loose_route()) {
		xlog("L_ERR",
		"Attempt to route with preloaded Route's [$fu/$tu/$ru/$ci]");
		if (!is_method("ACK"))
			sl_send_reply(403,"Preload Route denied");
		exit;
	}

	# record routing
	record_route();

	$avp(rtpproxy_set) = "0";

	# for INVITEs , create dialog with timeout
	if ( is_method("INVITE") ) {
		if ($rU==NULL) {
			# request with no Username in RURI
			send_reply(484,"Address Incomplete");
			exit;
		}
    #   Move to update_dialog_vars ROUTE
	#	if ( !create_dialog("B") ) {
	#		send_reply("500","Internal Server Error");
	#		exit;
	#	}

	#route(update_dialog_vars);
	}

	$json(a_tags) := "[]";

	# who is the sender ?
	# From PSTN Call
	if( ds_is_in_list("$si", $sp,2) && is_method("INVITE") ) {
           		xlog("SCRIPT:BLF:DBG: Call From PSTN To: $ru \n");
           		route(from_pstn);
	# From PSTN Call
	# TO PSTN Call
	} else if ( (ds_is_in_list("$si", $sp,1) || ds_is_in_list("$si", $sp,3) || ds_is_in_list("$si", $sp,4)  )&&  ( $ru=~"^sip:\+1[1-9][0-9]{9}@" || $ru=~"^sip:1[1-9][0-9]{9}@" || $ru=~"^sip:[1-9][0-9]{9}@" ||   $ru=~"^sip:011[1-9][0-9]{7}*@" ||   $ru=~"^sip:[2-9]11@")    ) {
		xlog("SCRIPT:BLF:DBG: Call TO PSTN To: $ru \n");
        route(to_pstn);
	# TO PSTN Call
	} else if (cluster_check_addr(1,"$si","sip")) {
		xlog("XXXX - cluster SIP from $si - $ru $du $ci \n");	

		# from within the cluster
		if (is_present_hf("X-DU")) {
			$du = $hdr(X-DU);
			remove_hf("X-DU");
		}	

#         	force_send_socket(udp:ANYCAST_IP:5060);
         	force_send_socket(udp:MY_IP:5060);
		create_dialog();
		$dlg_val(create_ts) = $Ts;
		$dlg_val(callerid) = $fU;
		if ($fn != NULL && $fn != "") {
			if ($fn =~ "^\"")		
				$var(fn) = $(fn{s.select,1,"});
			else
				$var(fn) = $fn;
			$dlg_val(calleridname) = $(var(fn){s.escape.common});
		} else
			$dlg_val(calleridname) = $fU;

		if ($ai != NULL && $ai != "")
			$dlg_val(pai) = $ai;
		if ($re != NULL && $re != "")
			$dlg_val(rpid) = $re;

		setbflag(DST_NAT);
		$avp(force_rtpp) = 1;
		route(insert_rtpproxy);

		route(relay);
	} else if ( ds_is_in_list("$si", $sp) ) {
              		# caller is the asterisk server
              		if (is_method("INVITE")) {
              			#$avp(blf_callee) = "sip:"+$rU+"@"+$rd;
              			#xlog("SCRIPT:BLF:DBG: -update_dialog_vars---fU $fu publishing callee $avp(blf_callee) state\n");
              			#dialoginfo_set("B");
              			route(from_server);
			} else if (is_method("OPTIONS")) {
				send_reply(200,"OK");
				exit;
			}
	} else if (is_method("PUBLISH") && is_myself("$si")) {
		route(handle_local_presence);
	} else {
		# probably caller is an subscriber
		if (is_method("REGISTER"))
			route(handle_register);
		if (is_method("SUBSCRIBE"))
			route(handle_presence);
		if (is_method("INVITE")) {
			$avp(blf_caller) = "sip:"+$fU+"@"+$fd;
			xlog("SCRIPT:BLF:DBG: publishing caller $avp(blf_caller) state\n");
			route(from_user);
		} else if (is_method("OPTIONS")) {
			# only from registered users
			if (is_registered("location","$fu")) {
				send_reply(200,"OK");
				exit;
			} else {
				exit;
			}
		}
	}

	# methods witch are not recognized
	send_reply(403,"Not Supported");
	exit;
}

route[handle_sip_auth] {
	if ($param(1) == 0) {
		# REGISTER auth
		$avp(auth_username) = $tU;
		$avp(auth_domain) = $td;
	
	} else { 
		# OTHER auth 
		$avp(auth_username) = $fU;
		$avp(auth_domain) = $fd;
	}

	if (cache_fetch("local","auth_$avp(auth_username)",$avp(auth_info))) {
		$var(cached) = 1;

		$json(auth_info) := $avp(auth_info);		
		xlog("KKKK - auth worked from cache : auth_$avp(auth_username)@$avp(auth_domain) points to $avp(auth_info) \n");
	
		$avp(auth_password) = $json(auth_info/password);
		$avp(password) = $avp(auth_password);
		$avp(ds_set) = $json(auth_info/ds_set);
		$avp(pstn_ds_set) = $json(auth_info/pstn_ds_set);
		$avp(ds_alg) = $json(auth_info/ds_alg);
		$avp(accountid) = $json(auth_info/accountid);
		$avp(rtpproxy_enabled) = $json(auth_info/rtpproxy_enabled);
		$avp(max_contacts) = $json(auth_info/max_contacts);
		$avp(subs_attrs) = $json(auth_info/attrs);
		$avp(pstn_dialplan) = $json(auth_info/pstn_dialplan);

		if ($param(1) == 0) {
			$var(rc) = pv_www_authorize("");
		} else {
			$var(rc) = pv_proxy_authorize("");
		}
	} else {
		$var(cached) = 0;

		if ($param(1) == 0) {
			$var(rc) = www_authorize("", "subscriberView");
		} else {
			$var(rc) = proxy_authorize("", "subscriberView");
		}
	}

	$avp(valid_subscriber) = 1;
	if ( $var(rc)<0 ) {

		if (is_domain_local($td,$avp(domain_attrs))) {
			$json(domain_attrs) := $avp(domain_attrs);
			if ($param(1) == 0)
				$avp(ratelimit_profile) = $json(domain_attrs/failed_reg_ratelimit_profile);
			else
				$avp(ratelimit_profile) = $json(domain_attrs/failed_call_ratelimit_profile);
			if ($avp(ratelimit_profile) != NULL)
				route(check_ratelimit_profile);
		}

		switch ( $var(rc) ) {
			case -5:  # generic error
				xlog("SCRIPT:AUTH:DBG: auth ($rm from $si) failed due generic error (user $fu)\n");
				break;
			case -4:  # no credentials
				xlog("SCRIPT:AUTH:DBG: auth ($rm from $si) without credentials (user $fu)\n");
				break;
			case -3:  # stale nonce
				xlog("SCRIPT:AUTH:DBG: auth ($rm from $si) faile due stale nonce (user $fu)\n");
				break;
			case -2:  # invalid passwd
				xlog("SCRIPT:AUTH:DBG: auth ($rm from $si) failed due invalid password (user $fu)\n");
				break;
			case -1:  # no such user
				xlog("SCRIPT:AUTH:DBG: auth ($rm from $si) failed for inexisting user (user $fu)\n");
				$avp(valid_subscriber) = 0;
				break;
		}

		if ($avp(valid_subscriber) == 1)
			avp_db_query("select count(*) from opensips.subscriberView where username='$avp(auth_username)'","$avp(valid_subscriber)"); 

		if ($avp(valid_subscriber) == 0) {
			xlog("SECURITY:ALERT : AUTH tried for non-existing subscriber $avp(auth_username) from $si - staying silent \n");

        		$json(security_alert) := "{}";
			$json(security_alert/ip) = $si;

			$var(new_body) = $json(security_alert);
			$avp(hdr_name) = "EventType";
			$avp(hdr_value) = "NO_SUBSCRIBER" ;

			if (!rabbitmq_publish("rabbitmq_ip1", "RMQ_EXCH_SECURITY", $var(new_body), "application/json", $avp(hdr_name), $avp(hdr_value))){
				xlog("Failed to push security alert though first rmq \n");
				if (!rabbitmq_publish("rabbitmq_ip2", "RMQ_EXCH_SECURITY", $var(new_body), "application/json", $avp(hdr_name), $avp(hdr_value))){
					xlog("Failed to push security alert though second rmq \n");
				}
			}	

			exit;
		}

		cache_add("local","fails_$avp(auth_username)",1,60,$avp(total_fails));
		if ($avp(total_fails) > 10 && $si != "209.163.136.215" && $si != "209.163.136.216") {
			xlog("SECURITY:ALERT : More than 10 auth fails in 1 minute from $si \n");

        		$json(security_alert) := "{}";
			$json(security_alert/ip) = $si;

			$var(new_body) = $json(security_alert);
			$avp(hdr_name) = "EventType";
			$avp(hdr_value) = "AUTH_FAILS" ;

			if (!rabbitmq_publish("rabbitmq_ip1", "RMQ_EXCH_SECURITY", $var(new_body), "application/json", $avp(hdr_name), $avp(hdr_value))){
				xlog("Failed to push security alert though first rmq \n");
				if (!rabbitmq_publish("rabbitmq_ip2", "RMQ_EXCH_SECURITY", $var(new_body), "application/json", $avp(hdr_name), $avp(hdr_value))){
					xlog("Failed to push security alert though second rmq \n");
				}
			}	
			exit;
		}
	
		if ($param(1) == 0)
			www_challenge("", 0);
		else
			proxy_challenge("", 0);

		exit;
	}

	if ($var(cached) == 0) {
		$json(auth_info) := "{}";	

		$json(auth_info/password) = $avp(password);
		$json(auth_info/ds_set) = $avp(ds_set);
		$json(auth_info/pstn_ds_set) = $avp(pstn_ds_set);
		$json(auth_info/ds_alg) = $avp(ds_alg);
		$json(auth_info/accountid) = $avp(accountid);
		$json(auth_info/rtpproxy_enabled) = $avp(rtpproxy_enabled);
		$json(auth_info/max_contacts) = $avp(max_contacts);
		$json(auth_info/attrs) = $avp(subs_attrs);
		$json(auth_info/pstn_dialplan) = $avp(pstn_dialplan);
	
		xlog("KKKK - auth worked from DB : auth_$avp(auth_username)@$avp(auth_domain) points to $json(auth_info) \n");
		cache_store("local","auth_$avp(auth_username)","$json(auth_info)",7200);
	}

	if ($param(1) == 0) {
		if ($au==$tU)
			$var(rc) = 1;
		else
			$var(rc) = -1;
	} else {
		if ($au==$fU)
			$var(rc) = 1;
		else
			$var(rc) = -1;
	}
		
	if ($var(rc) < 0) {
		xlog("SCRIPT:SECURITY:ALERT: user $fu calling with $au / $ar\n");
		send_reply(403,"Forbidden auth ID");
		exit;
	}
			
	$json(auth_attrs) := $avp(subs_attrs);
	if ($json(auth_attrs/app_data) != NULL) {
		$avp(app_data) = $json(auth_attrs/app_data);
	}
}

route[handle_jwt_auth] {
	$var(jwt_token) = $(param(2){s.select,1, }{s.trim});
	xlog("XXXX trying to do JWT auth for $param(2) ; token = [$var(jwt_token)]\n");

	$log_level=6;
	if (jwt_authorize("$var(jwt_token)",$avp(decoded_jwt),$avp(out_username))) {
		$log_level=3;
		xlog("Validated AUTH token : $avp(decoded_jwt) for SIP username $avp(out_username) ; extra info = $avp(profile_info) \n");
	
#		if ($param(1) == 0) {
#			if ($tU != $avp(out_username)) {
#				send_reply(403,"Forbidden SIP ID");
#				exit;
#			}
#		} else {
#			if ($fU != $avp(out_username)) {
#				send_reply(403,"Forbidden SIP ID");
#				exit;
#			}
#		}
		    $var(jwtHeader)=$(avp(decoded_jwt){s.select,0,.});

	            $var(jwtHeaderSize)=$(var(jwtHeader){s.len})+1;
		    $json(jwt_body) := $(avp(decoded_jwt){s.substr,$var(jwtHeaderSize),0});
	  		  xlog("XXXX - $var(jwtHeader) \n");
                          xlog("XXXX - $var(jwtHeaderSize) \n");
			  xlog("XXXX -$(avp(decoded_jwt){s.substr,$var(jwtHeaderSize),0})  \n");

                $json(jwt_body) := $(avp(decoded_jwt){s.substr,$var(jwtHeaderSize),0}) ;
		 if ($json(jwt_body) != NULL && $json(jwt_body/email) != NULL) {
	         	 xlog("XXXX - OAuth email $json(jwt_body/email) \n"); 
			 append_hf("X-OAUTH: $json(jwt_body/email) \r\n");
			 $avp(email)= $json(jwt_body/email);
			 $avp(emailName)=$(avp(email){s.select,0,@});
			$avp(emailUri)="sip:" + $avp(emailName) +"@" + $fd;
#			uac_replace_from($avp(emailName),"");
#			uac_replace_from("$avp(emailName)","sip:$avp(emailName)@$fd"); 
		}
#		$json(jwt_body) := $(avp(decoded_jwt){s.select,1,.});
		xlog("XXXX - jwt body = $json(jwt_body) \n");
		if ($json(jwt_body) != NULL && $json(jwt_body/attrs/attrs/app_data) != NULL) {
			xlog("XXXX - jwt body got app data \n");
			$json(app_data) := $json(jwt_body/attrs/attrs/app_data);
			$json(new_attrs) := "{}";
			for ($var(k) in $(json(app_data.keys)[*])) {
				$var(new_k) = "app_" + $var(k);
				$json(new_attrs/$var(new_k)) = $json(app_data/$var(k));
			}

			$json(jwt_body/attrs/attrs/app_data) := $json(new_attrs);

			xlog("XXXX - built new attrs $json(new_attrs) \n");
			$avp(app_data) = $json(new_attrs);
		}
	} else {
		$log_level=3;
		if (is_domain_local($td,$avp(domain_attrs))) {
			$json(domain_attrs) := $avp(domain_attrs);
			$avp(ratelimit_profile) = $json(domain_attrs/ratelimit_profile);
			if ($avp(ratelimit_profile) != NULL)
				route(check_ratelimit_profile);
		}
		return (-1);
	}

	remove_hf_glob("Authorization*");
	if ($json(jwt_body) != NULL) {
		xlog("XXXX - need to merge $json(jwt_body) with $avp(profile_info) \n");
		if (json_merge("$avp(profile_info)","$json(jwt_body)",$var(new_info))) {
			$avp(profile_info) = $var(new_info);
			xlog("XXX  - merge info = $var(new_info) \n");
		}
	}

	# we've authenticated using JWT
	# password;ds_set;pstn_ds_set;ds_alg;accountid;rtpproxy_enabled;max_contacts;$avp(subs_attrs)=attrs
	xlog("XXXX - profile attrs $avp(profile_info) \n");

	$json(jwt_info) := $avp(profile_info);
	$avp(password) = $json(jwt_info/password);
	$avp(ds_set) = $json(jwt_info/ds_set);
	$avp(pstn_ds_set) = $json(jwt_info/pstn_ds_set);
	$avp(ds_alg) = $json(jwt_info/ds_alg);
	$avp(accountid) = $json(jwt_info/accountid);
	$avp(rtpproxy_enabled) = $json(jwt_info/rtpproxy_enabled);
	$avp(max_contacts) = $json(jwt_info/max_contacts);
	if ($json(jwt_info/attrs/attrs/app_data) != NULL) {
		$avp(app_data) = $json(jwt_info/attrs/attrs/app_data);
		xlog("Forced app data = $avp(app_data) \n");
	}

	xlog("XXX - setting attrs as $json(jwt_info/attrs) \n");
	$avp(subs_attrs) = $json(jwt_info/attrs);
	
	return (1);
}

route[handle_subscriber_auth] 
{
	$var(idx) = 0;
	$var(jwt_auth) = 0;

	while ($(hdr(Authorization)[$var(idx)]) != NULL && $var(jwt_auth) == 0) {
		xlog("XXXX - auth hdr is $(hdr(Authorization)[$var(idx)]) \n");
		if ($(hdr(Authorization)[$var(idx)]) =~ "Bearer") {
			if (route(handle_jwt_auth,$param(1),$(hdr(Authorization)[$var(idx)]))) {
				if ($json(jwt_info/app_data) != NULL) {
					$avp(app_data) = $json(jwt_info/app_data);
				}
				$var(jwt_auth) = 1;
			} else
				$var(idx) = $var(idx) + 1;		
		} else {
			$var(idx) = $var(idx) + 1;		
		}
	}

	if ($var(jwt_auth) == 0) 
		route(handle_sip_auth,$param(1));	
}

route[handle_register]
{
	$avp(test_hex) = "vlad";
	xlog("SCRIPT:DBG: handling register for $tu $(avp(test_hex){s.encode.hexa}) \n");

#	$var(jwt_token) = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJ0YWciOiJ2bGFkIn0.1MSrLtjUNwuLEiTHa6gA2wAnmNp3UD_pekm1VKJbZwk";
#	if (jwt_authorize("$var(jwt_token)",$avp(decoded_jwt),$avp(out_username))) {
#		xlog("Validated token : $avp(decoded_jwt) for SIP username $avp(out_username) ; extra info = $avp(profile_info) \n");
#	}


	# check for invalid contacts
	$var(i) = 0;
	while( $(ct[$var(i)])!=NULL ) {
		if ($(ct[$var(i)]) != "*") {
			$var(host) = $(ct[$var(i)]{nameaddr.uri}{uri.host});
			# if ( $(var(host){ip.isip})==0 ||  ## contact is not an IP
			if ( $var(host)=="ANYCAST_IP" ||  ## IP is myself
				is_domain_local("$var(host)") ||  ## IP is a local domain
				ds_is_in_list("$var(host)",0)    ## IP is a server
			){
				xlog("SCRIPT:SECURITY:ALERT: $tu/$si registraring protected contacts $var(host)\n");
				send_reply(476, "No Server Address in Contacts Allowed" );
				exit;
			}
		}
		$var(i) = $var(i) + 1;
	}

	route(handle_subscriber_auth,0);

	if (is_domain_local($td,$avp(domain_attrs))) {
		$json(domain_attrs) := $avp(domain_attrs);
		$avp(ratelimit_profile) = $json(domain_attrs/ratelimit_profile);
	}

	$json(subs_attrs) := $avp(subs_attrs);
	if ($json(subs_attrs/ratelimit_profile) != NULL) {
		$avp(ratelimit_profile) = $json(subs_attrs/ratelimit_profile);
	}

	route(check_ratelimit_profile);

        #record the device's user-agent and accountid to send in the device_update event
        $var(reg_cid) = $(rd{s.substr,1,0});
        $var(reg_cid) = $(var(reg_cid){s.select,0,.}) + "|" + $si + "|" + $(ua{s.encode.hexa});
        cache_raw_query("redis","SET agent_$fU $var(reg_cid)");
        cache_raw_query("redis","EXPIRE agent_$fU 100");


	if (isflagset(SRC_NAT)){
		xlog("SCRIPT:SECURITY:ALERT: [Voicenter] user $tu registering with $au / $ar and is active nated conation  \n");

     #   setbflag(RM_ONTO_FLAG);


		fix_nated_register();
		setbflag(DST_NAT);
	#	setbflag(SIP_PING_FLAG);
		setbflag(SIPPING_LATENCY_FLAG);

	}

	if (isflagset(SRC_WS)) {
		setbflag(DST_WS);
		setbflag(DST_NAT);
	}	

	setflag(TCP_PERSISTENT);
	setbflag(NO_NEW_CONN);
	$avp(topmost_pn) = $(ct.fields(uri){uri.param,pn-tok});
	if ($avp(topmost_pn) != NULL && $avp(topmost_pn) != "") {
		setbflag(PUSH_DEVICE);
	}

	$json(subs_attrs) := $avp(subs_attrs);
	$json(subs_attrs/rtpproxy_enabled) = $avp(rtpproxy_enabled);
	$json(subs_attrs/accountid) = $avp(accountid);
	$json(subs_attrs/reg_server) = "sip:MY_PRIV_IP:5060";
	$avp(subs_attrs) = $json(subs_attrs);

	xlog("SCRIPT:DBG: saving register for $tu \n");
	if (!save("location","c$avp(max_contacts)"))
		sl_reply_error();

	$avp(topmost_pn) = $(ct.fields(uri){uri.param,pn-tok});
	if ($avp(topmost_pn) != NULL && $avp(topmost_pn) != "") {
		$avp(token_type) = $(ct.fields(uri){uri.param,pn-type});
		# pn token supported registration
		if (is_present_hf("User-Agent")) {
			$var(imei_lookup_s) = "Device_IMEI:";
			$var(imei_lookup_e) = ")";
			$var(idx_start) = $(hdr(User-Agent){s.index,$var(imei_lookup_s),0});
			$var(idx_end) = $(hdr(User-Agent){s.index,$var(imei_lookup_e),$var(idx_start)});

			$var(idx_start) = $var(idx_start) + 12;
			$var(len) = $var(idx_end) - $var(idx_start);
			
			$avp(imei) = $(hdr(User-Agent){s.substr,$var(idx_start),$var(len)});
			
			xlog("XXXX - $fU sent a register with pn-tok = [$avp(topmost_pn)] and IMEI = [$avp(imei)] \n");

			launch(avp_db_query("call sp_upsert_push_token('$fU','$avp(topmost_pn)','$avp(token_type)','$avp(imei)')"));
		}
	}

	exit;
}


route[handle_local_presence]
{
	if(!t_newtran()){
		sl_reply_error();
		exit;
	}

	trace("hep_tracer","T");

	## for the BLF related presence traffic
	## force the same phony domain in the presentity
	if ( $hdr(Event)=="dialog" ) { ## BLF
		$rd = "phony.voicenter.com";
	}

	if (is_method("PUBLISH")) {
		handle_publish();
	} else
	if (is_method("SUBSCRIBE")) {
		handle_subscribe();
	}

	exit;
}


route[handle_presence]
{
	## traffic comes from a subscriber (only SUBSCRIBE requests here)
	xlog("SCRIPT:DBG: presence $rm received from $si:$sp with event <$hdr(Event)>\n");

	## BLF is locally handled
	if ( $hdr(Event)=="dialog" ) { ## BLF
		# first authenticate
		route(handle_subscriber_auth,1);

		if (is_uri_host_local($avp(domain_attrs))) {
			$json(domain_attrs) := $avp(domain_attrs);
			$avp(ratelimit_profile) = $json(domain_attrs/ratelimit_profile);
		}

		$json(subs_attrs) := $avp(subs_attrs);
		if ($json(subs_attrs/ratelimit_profile) != NULL) {
			$avp(ratelimit_profile) = $json(subs_attrs/ratelimit_profile);
		}
		route(check_ratelimit_profile);

		# convert from SPEEDDIAL to SUBSCRIBER
		if ( route(do_speeddial_lookup) )
			$rU = $avp(sd_target);

		# handle BLF locally
		route(handle_local_presence);
		exit;
	} else if ($hdr(Event) == "message-summary") {
		# first authenticate
		route(handle_subscriber_auth,1);

		if (is_uri_host_local($avp(domain_attrs))) {
			$json(domain_attrs) := $avp(domain_attrs);
			$avp(ratelimit_profile) = $json(domain_attrs/ratelimit_profile);
		}

		$json(subs_attrs) := $avp(subs_attrs);
		if ($json(subs_attrs/ratelimit_profile) != NULL) {
			$avp(ratelimit_profile) = $json(subs_attrs/ratelimit_profile);
		}
		route(check_ratelimit_profile);

		route(handle_local_presence);
		exit;
	} 

	## any other Events to be forwarded to Asterisk
	send_reply(406,"Not Acceptable");
	exit;
}

#route[push_init_pstn_rabbitmq_event] {
#	$avp(old_state) = 0;
#	$avp(new_state) = 1;
#	$avp(dlg_json) = $DLG_ctx_json;
#
#	$avp(hdr_name) := NULL;
#	$avp(hdr_name) = "EventType";
#
#	$avp(hdr_name) = "OldState";
#	$avp(hdr_value) = $avp(old_state);
#	$avp(hdr_name) = "NewState";
#	$avp(hdr_value) = $avp(new_state);
#	
#	$var(new_body) = $avp(dlg_json);
#	
#	if (!rabbitmq_publish("rabbitmq_realtime1", "RMQ_EXCH_REALTIME", "$var(new_body)", "application/json", "$avp(hdr_name)", "$avp(hdr_value)")){
#		xlog("L_ERR", "cannot publish event to rabbitmq server event=$var(event) user=$var(event) body=[$var(new_body)]\n");
#		if (!rabbitmq_publish("rabbitmq_realtime2", "RMQ_EXCH_REALTIME", "$var(new_body)", "application/json", "$avp(hdr_name)", "$avp(hdr_value)")){
#			xlog("L_ERR", "cannot publish event to rabbitmq_2 server event=$var(event) user=$var(event) body=[$var(new_body)]\n");
#		}
#	}
#}

route[pstn_rabbitmq_event] {
	#hash_entry - the entry in the dialog table. This is used, along with the hash_id, to uniquely identify the dialog.
	$avp(hash_entry) = $param(hash_entry);
	xlog("E_DLG_STATE_CHANGED (hash_entry) $avp(hash_entry) \n");

	#hash_id - the id in the dialog table. This is used, along with the hash_entry, to uniquely identify the dialog.
	$avp(hash_id) = $param(hash_id);
	xlog("E_DLG_STATE_CHANGED (hash_id) $avp(hash_id) \n");

	#ostate - the old state of the dialog.
	$avp(old_state) = $param(old_state);
	xlog("E_DLG_STATE_CHANGED (ostate) $avp(old_state) \n");

	#nstate - the new state of the dialog.
	$avp(new_state) = $param(new_state);
	xlog("E_DLG_STATE_CHANGED (nstate) $avp(new_state) \n");

	$avp(dlg_json) = $param(dlg_json);
	xlog("E_DLG_STATE_CHANGED (dlg_json) $avp(dlg_json) \n");

	$json(dlg_json) := $avp(dlg_json);
	if ($json(dlg_json/values/from_pstn) == "1" || $json(dlg_json/values/to_pstn) == "1") {
		# only replicate this way for from_pstn or to_pstn calls
		$avp(hdr_name) := NULL;
		$avp(hdr_value) := NULL;

		$avp(hdr_name) = "EventType";
		if ($json(dlg_json/values/from_pstn) == "1")
	                $avp(hdr_value) = "FromPSTNCall";
		else
	                $avp(hdr_value) = "ToPSTNCall";

		$avp(hdr_name) = "OldState";
		$avp(hdr_value) = $avp(old_state);
		$avp(hdr_name) = "NewState";
		$avp(hdr_value) = $avp(new_state);

		if ($json(dlg_json/values/rmq_headers) != NULL && $json(dlg_json/values/rmq_headers) != "") {
			$json(rmq_headers) := $json(dlg_json/values/rmq_headers);

			for ($var(k) in $(json(rmq_headers.keys)[*])) {
				$avp(hdr_name) = $var(k);
				$avp(hdr_value) = $json(rmq_headers/$var(k));
			}
			
		}
		
		$var(new_body) = $avp(dlg_json);
		xlog("XXXX - preparing to send pstn rmq event $(avp(hdr_name)[*]) : $(avp(hdr_value)[*]) \n");
		
		if (!rabbitmq_publish("rabbitmq_realtime1", "RMQ_EXCH_REALTIME", $var(new_body), "application/json", $avp(hdr_name), $avp(hdr_value))){
			xlog("L_ERR", "cannot publish event to rabbitmq server event=$var(event) user=$var(event) body=[$var(new_body)]\n");
			if (!rabbitmq_publish("rabbitmq_realtime2", "RMQ_EXCH_REALTIME", $var(new_body), "application/json", $avp(hdr_name), $avp(hdr_value))){
        	        	xlog("L_ERR", "cannot publish event to rabbitmq_2 server event=$var(event) user=$var(event) body=[$var(new_body)]\n");
        		}
        	}
	}
}

route[rabbitmq_event] {
	$var(user) = $param(user);
	$var(event) = $param(event);
	$var(body) = $param(body);

	xlog("ILYA SCRIPT:DBG: received publish for user $var(user) with event $var(event)\n");
	script_trace ( 1, "rabbitMQ event user=-->$var(user)<-- event=-->$var(event)<--" , "Voicenter" );

	$avp(hdr_name) := NULL;
	$avp(hdr_value) := NULL;

	$avp(hdr_name) = "customer_id";
	$avp(hdr_value) = "17635";

	$avp(cache_event) = "0";

	switch($var(event)) {
		case "dialog":
			$xml(body) = $var(body);

			# {presentity
			$var(new_body) = "{\"Presentity\":\"" + $xml(body/dialog-info.attr/entity) + "\"";
			# ,direction
			$var(new_body) = $var(new_body) + ",\"Direction\":\"" +
				$xml(body/dialog-info/dialog.attr/direction) + "\"";

			# ,identity in <local>
			$var(new_body) = $var(new_body) + ",\"Local\":\"" +
				$xml(body/dialog-info/dialog/local/identity.val) + "\"";

			# ,identity in <remote>
			$var(new_body) = $var(new_body) + ",\"Remote\":\"" +
				$xml(body/dialog-info/dialog/remote/identity.val) + "\"";

			# callid}
			$var(new_body) = $var(new_body) + ",\"CallID\":\"" +
				$xml(body/dialog-info/dialog.attr/call-id) + "\"";

			# State
			$var(new_body) = $var(new_body) + ",\"State\":\"" +
				$xml(body/dialog-info/dialog/state.val) + "\"";


			#OnHold
			#$var(new_body) = $var(new_body) + ",\"OnHold\":"\"" + $dlg_val(IsOnHold) + "\"";

			xlog("---------------------uri.user =  $(xml(body/dialog-info.attr/entity){uri.user}) ---------------------------" );
			$var(publish_device) =  $(xml(body/dialog-info.attr/entity){uri.user}) ;

			# Call List
		  #    xlog("VCLOG before calling the route load_device_calls var(publish_device) IS:-->$var(publish_device)<--");

		    route (load_device_calls, $var(publish_device)  );
		    $var(new_body) = $var(new_body) + ",\"call_list\":" +
		       $var(json_body_calls);

			xlog("VCLOG Before checking if state=terminated or not, var(new_body) IS:-->$var(new_body)<--");
			if ($xml(body/dialog-info/dialog/state.val) == "terminated") {
				$var(rt_result) = route (remove_call_vars,$xml(body/dialog-info/dialog.attr/call-id),$(xml(body/dialog-info/dialog/local/identity.val){uri.user}));
				$var(new_body) = $var(new_body) + ",\"EventType\":\"hangup_call\""  ;
				   $avp(hdr_name) = "EventType";
                  $avp(hdr_value) = "ActiveCall" ;
            	  $avp(hdr_name) = "EventState";
                 $avp(hdr_value) = $xml(body/dialog-info/dialog/state.val) ;

			} else {
				$var(rt_result) = route (get_single_call,$xml(body/dialog-info/dialog.attr/call-id),$(xml(body/dialog-info/dialog/local/identity.val){uri.user}));
				$var(new_body) = $var(new_body) + ",\"EventType\":\"active_call\""  ;
				$avp(hdr_name) = "EventType";
                $avp(hdr_value) = "ActiveCall" ;
            	  $avp(hdr_name) = "EventState";
                 $avp(hdr_value) = $xml(body/dialog-info/dialog/state.val) ;
             }

			$json(call_list) := $var(json_body_calls);
			$json(topmost_dlg) := $json(call_list[0]);
			$var(dlg_state) = $json(topmost_dlg/state);

			$var(rmq_headers) = $(json(topmost_dlg/values/rmq_headers){s.decode.hexa});
                        xlog("PRESENTITY here - $json(topmost_dlg) with headers $var(rmq_headers)  - all dlgs = $json(call_list) with state = $var(dlg_state) \n");


			if ($var(rmq_headers) == NULL) {
				$json(2nd_dlg) := $json(call_list[1]);
				$var(rmq_headers) = $(json(2nd_dlg/values/rmq_headers){s.decode.hexa});
			}

			xlog("XXXX - PRESENTITY - found vars to $var(rmq_headers) \n");

			if ($var(rmq_headers) != NULL && $var(rmq_headers) != "") {
				$json(rmq_headers) := $var(rmq_headers);

				for ($var(k) in $(json(rmq_headers.keys)[*])) {
					$avp(hdr_name) = $var(k);
					$avp(hdr_value) = $json(rmq_headers/$var(k));
				}
			
			}

			if($xml(body/dialog-info/dialog/state.val) == "early"){
				$avp(cache_event) = "180";
			}

			xlog("VCLOG After checking if state=terminated or not, var(new_body) IS:-->$var(new_body)<--");
			xlog("VCLOG After checking if state=terminated or not, var(rt_result) IS:--> $var(rt_result)<--");

			$var(new_body) = $var(new_body) + ",\"SipUsername\":\"" + $var(publish_device) + "\"";
			$var(new_body) = $var(new_body) + ",\"Timestamp\":\"" + $Ts + "\"";
			#$var(new_body) = $var(new_body) + ",\"CallData\":" + $var(single_call_json);
			#$var(new_body) = $var(new_body) + ",\"OnHold\":\"" + $dlg_val(IsOnHold) + "\"";

			#----------  Closing the object , Dont we have json module for that ---------------------
			  $var(new_body) = $var(new_body) + "}" ;
			#----------------------------------------------------------------------------------------

			xlog("VCLOG After closing var(new_body) IS:-->$var(new_body)<--");

			# fetch the values from the dialog
			$dlg_val("rabbitmq_event") ="xxxxxxxxxxxxxxxxxxxxxxxx";
			get_dialog_vals($avp(hdr_name), $avp(hdr_value), $var(callid));

			$avp(hdr_name) = "DLG_STATE";
			$avp(hdr_value) = $var(dlg_state);
			break;
		default:
			xlog("SCRIPT:WARN: unhandled event type $var(event) for user $var(user)\n");
			xlog("VCLOG var(new_body) iss -->$var(new_body)<--   \n");
				$var(new_body) = $var(body);
	}


	# all good: send the publish to rabbitmq

	xlog("ILYA:: cache_event is $avp(cache_event)");
	xlog("ILYA:: SCRIPT:DBG: publishing rabbitmq: $var(new_body)");
	if (!rabbitmq_publish("rabbitmq_realtime1", "RMQ_EXCH_REALTIME", $var(new_body), "application/json", $avp(hdr_name), $avp(hdr_value))){
		xlog("L_ERR", "cannot publish event to rabbitmq server event=$var(event) user=$var(event) body=[$var(new_body)]\n");
		if (!rabbitmq_publish("rabbitmq_realtime2", "RMQ_EXCH_REALTIME", $var(new_body), "application/json", $avp(hdr_name), $avp(hdr_value))){
                	xlog("L_ERR", "cannot publish event to rabbitmq_2 server event=$var(event) user=$var(event) body=[$var(new_body)]\n");
        	}
        }


	script_trace();
}


# these routes simply catch the event
event_route[E_PRESENCE_PUBLISH] {
	route(rabbitmq_event);
}

event_route[E_PRESENCE_EXPOSED] {
	route(rabbitmq_event);
}


route[do_speeddial_lookup]
{
	xlog("SCRIPT:DBG: doing speeddial lookup for account $avp(accountid) to $rU\n");

	# KKVLAD here - regular speeddial lookup
        
	$avp(partition) = "account_" + $avp(accountid);
	if (!do_routing(1,"LC",,$avp(rule_attrs),,,$avp(partition))) {
		xlog("SCRIPT:DBG: no record found for DID $rU in partition $avp(partition) $ci\n");
		$avp(speeddial_fail) = 1;
		return (1);
		#send_reply("404","Not found23");
		#exit;
	}

	xlog("KKKK - $avp(rule_attrs) for $ci \n");

	$avp(speeddial_fail) = 0;

	$json(rule_attrs) := $avp(rule_attrs);
	$avp(sd_target) = $json(rule_attrs/username);
	$avp(full_username) = $json(rule_attrs/username);
	$avp(sd_setid) = $json(rule_attrs/setid);
	$avp(sd_alg) = $json(rule_attrs/alg);
	if ($avp(sd_target) == NULL || $avp(sd_target) == '') {
		xlog("SCRIPT:DBG: do_speeddial_lookup sd_target is empty must be a Regex lets use the rU-> $rU \n");
		$avp(sd_target)= $rU;
	}

	$json(app_data) := $json(rule_attrs/app_data);

	if ($json(app_data) != NULL) {
		xlog("XXX - found app data $json(app_data) for $ci \n");

		for ($var(k) in $(json(app_data.keys)[*])) {
			if ($avp(app_call) != NULL && $avp(app_call) == 0)
				$var(h_k) = "X-"+$(var(k){s.toupper}{re.subst,/_/-/g});
			else
				$var(h_k) = "X-T-"+$(var(k){s.toupper}{re.subst,/_/-/g});

			$var(v) = $json(app_data/$var(k));

			if ($var(h_k) != "X-APP-TAG") {
				append_hf("$var(h_k) : $var(v)\r\n");

				if ($DLG_status!=NULL ) {
					store_dlg_value("$var(h_k)","$var(v)");
				}
			}
		} 

		#if ($json(app_data/app_name) != NULL) {
                #	append_hf("X-APP-NAME: $json(app_data/app_name) \r\n");
		#}	
		#if ($json(app_data/app_target_id) != NULL) {
                #	append_hf("X-APP-TARGETID: $json(app_data/app_target_id) \r\n");
		#}	
	}

	$json(app_tags) := $json(rule_attrs/app_tags);
	if ($json(app_tags) != NULL) {
		xlog("XXX - found app tags $json(app_tags) for $ci \n");
		for ($var(tag) in $(json(app_tags)[*])) {
			set_dlg_profile("tags","$var(tag)");
			$json(a_tags[]) = $var(tag);
		}
	}
	
#
#	avp_db_query("select target, setid, alg ,accountid from speeddial where (( speeddial='$rU' or ('$rU' REGEXP  CONCAT('^',speeddial.speeddial ,CHAR(36))  and speeddial.pattern_type=2) ) and (  accountid=$avp(accountid) or accountid=0 )) or (speeddial.speeddial  = '$rU'    and speeddial.pattern_type=3 )  order by accountid DESC , priority ",
#		"$avp(sd_target),$avp(sd_setid),$avp(sd_alg),$avp(sd_accountid)");
#	if ($retcode<0 ||  $avp(sd_setid)==NULL) {
#    		xlog("SCRIPT:DBG: do_speeddial_lookup no record found1\n");
#    		return(-1);
#    	}
#    if ( $avp(sd_target)==NULL || $avp(sd_target) = '' ) {
#        		xlog("SCRIPT:DBG: do_speeddial_lookup sd_target is empty must be a Regex lets use the rU-> $rU \n");
#        		 $avp(sd_target)= $rU;
#     }
#
	xlog("SCRIPT:DBG: found target <$avp(sd_target)> via $avp(sd_setid) / $avp(sd_alg)/ $avp(accountid) $ci\n");

	if ($avp(sd_setid)=="<null>")
		$avp(sd_setid)=NULL;
	if ($avp(sd_alg)=="<null>")
		$avp(sd_alg)=NULL;

	return(1);
}


route[from_user]
{
	if (cache_fetch("local","bannedua_0_$ua",$var(banned_ua))) {
		xlog("SECURITY:ALERT : $si sending from banned UA $ua \n");
		exit;
	}
	
	if (!pike_check_req() && $si != "209.163.136.215" && $si != "209.163.136.216") {
		xlog("SECURITY:ALERT : Flood attempt - more than 10 request in 1 minute from $si \n");

        	$json(security_alert) := "{}";
		$json(security_alert/ip) = $si;

		$var(new_body) = $json(security_alert);
		$avp(hdr_name) = "EventType";
		$avp(hdr_value) = "PIKE_FLOOD" ;

	        if (!rabbitmq_publish("rabbitmq_ip1", "RMQ_EXCH_SECURITY", $var(new_body), "application/json", $avp(hdr_name), $avp(hdr_value))){
			xlog("Failed to push security alert though first rmq \n");
			if (!rabbitmq_publish("rabbitmq_ip2", "RMQ_EXCH_SECURITY", $var(new_body), "application/json", $avp(hdr_name), $avp(hdr_value))){
				xlog("Failed to push security alert though second rmq \n");
			}
		}	
	}

	 $avp(os_uuid) = $UUID;
	xlog("ZZZZ - just generated $avp(os_uuid) for $ci \n");

    $var(device) = $fU ;
    $avp(device) = $fU ;
 	#route(update_dialog_vars);

	xlog("SCRIPT:DBG: $rm from customer ($fu) to $ru $ci \n");

#	if (!is_from_local()) {
#		route (remove_call_vars,$ci,$dlg_val(device));
#		send_reply("403","Forbidden Domain");
#		exit;
#	}

	# authenticate if from local subscriber
	# authenticate all initial non-REGISTER request that pretend to be
	# generated by local subscriber (domain from FROM URI is local)
	route(handle_subscriber_auth,1);

	route(post_subscriber_auth);
	exit;	
}

route[post_subscriber_auth] {
	create_dialog();
	$dlg_val(create_ts) = $Ts;
	$dlg_val(callerid) = $fU;
	if ($fn != NULL && $fn != "") {
		if ($fn =~ "^\"")		
			$var(fn) = $(fn{s.select,1,"});
		else
			$var(fn) = $fn;
		$dlg_val(calleridname) = $(var(fn){s.escape.common});
	} else
		$dlg_val(calleridname) = $fU;
	if ($ai != NULL && $ai != "")
		$dlg_val(pai) = $ai;
	if ($re != NULL && $re != "")
		$dlg_val(rpid) = $re;
	trace("hep_tracer","D");
	$json(subs_attrs) := $avp(subs_attrs);
	xlog("XXX - subs attrs post = $json(subs_attrs) \n");
	$var(cc_limit) = $json(subs_attrs/attrs/cc);

	$json(a_tags[]) = "u" + $fU;
	set_dlg_profile("tags","u$fU");
	set_dlg_profile("tags","a$avp(accountid)");
	$json(a_tags[]) = "a" + $avp(accountid) ;

	if ($var(cc_limit) > 0) {

		set_dlg_profile("subscriber",$fU);
		get_profile_size("subscriber",$fU,$var(size));		
		if ($var(size) > $var(cc_limit)) {
			xlog("XXX - user $fU has reached the $var(cc_limit) concurrent call limit - rejecting \n");
			send_reply(503,"CC Limit Reached");
			exit;
		} else {
			xlog("XXX - user $fU has $var(size) ongoing calls of $var(cc_limit) total limit \n");
		}
	}

        xlog("SCRIPT:BLF:DBG: -update_dialog_vars--- A-side fU $avp(blf_caller) publishing callee $avp(blf_callee) state\n");

	dialoginfo_set("A");
	set_dlg_sharing_tag("MY_CLUSTER_NODE_ID");

	consume_credentials();
	# caller authenticated

	if (!is_uri_host_local($avp(domain_attrs))) {
		route (remove_call_vars,$ci,$dlg_val(device));
		send_reply(404,"Domain not found");
		exit;
	}

	$json(domain_attrs) := $avp(domain_attrs);
	$avp(ratelimit_profile) = $json(domain_attrs/ratelimit_profile);

	if ($json(subs_attrs/ratelimit_profile) != NULL)
		$avp(ratelimit_profile) = $json(subs_attrs/ratelimit_profile);
	route(check_ratelimit_profile);

        $avp(partition) = "account_" + $avp(accountid);
        xlog("XXX - looking up $rU in $avp(partition) \n");

        remove_hf("X-APP");
        #replace * with 122
        $var(replace_stars) = $rU;
        $rU = $(var(replace_stars){re.subst,/\*/1222/g});
        xlog("SCRIPT:DBG: new user is $rU #######################");


	if ($avp(app_data) != NULL) {
		$json(app_data) := $avp(app_data);
                ##$avp(app_call) = 1;

		if ($json(app_data) != NULL) {
			xlog("XXX - found app data $json(app_data) for $ci \n");

			for ($var(k) in $(json(app_data.keys)[*])) {
				$var(h_k) = "X-"+$(var(k){s.toupper}{re.subst,/_/-/g});
				$var(v) = $json(app_data/$var(k));

				
				if ($var(h_k) != "X-APP-TAG") {	
					append_hf("$var(h_k) : $var(v)\r\n");
					if ($DLG_status!=NULL ) {
						store_dlg_value("$var(h_k)","$var(v)");
					}
				}
			} 

			if ($json(app_data) != NULL && ( $json(app_data/app_name) == "pickup" || $json(app_data/app_name) == "whisper")) {
				$var(app_tag) = $json(app_data/app_tag);
			
				if ($json(app_data/app_name) == "pickup")
					$var(search_query) = '[{"tags":' + $var(app_tag) + ',"min_state": 0,"max_state":3}]';
				else
					$var(search_query) = '[{"tags":' + $var(app_tag) + ',"min_state": 3,"max_state":5}]';

				xlog("XXXX - $oU is $json(app_data/app_name) targeting tag $var(app_tag) \n");

				xlog("XXXX - running1 /root/search_tags.py '$var(search_query)' \n");

				exec("/root/search_tags.py '$var(search_query)'",,$avp(dlg_jsons));
		
				xlog("XXXX - got back $avp(dlg_jsons) \n");

				$json(search_tags) := $var(app_tag);
				$var(first_tag) = $json(search_tags[0]);

				$json(ret_dlgs) := $avp(dlg_jsons);
				if ($json(ret_dlgs) != NULL) {

					$avp(recent_uuid) := NULL;
					$avp(recent_location) := NULL;
					$var(min_start) = 2147483646;
					
					for ($var(ret_arr) in $(json(ret_dlgs)[*])) {
						$json(ret_arr) := $var(ret_arr);
						for ($var(q_rpl) in $(json(ret_arr)[*])) {
							xlog("BBBBB - got dlg rpl $var(q_rpl) \n");

							$json(dlg_info) := $var(q_rpl);
							$var(dlg_state) = $(json(dlg_info/state){s.int});
							$var(dlg_start) = $(json(dlg_info/values/create_ts){s.int});

							if ($json(app_data/app_name) == "pickup" && $var(first_tag) =~ "^u") {
								$var(from_uri_tag) = "u" + $(json(dlg_info/from_uri){uri.user});

								xlog("XXXXX - pickup targetting user $var(first_tag) - from generated tag = $var(from_uri_tag) \n");
								
								if ($var(first_tag) != $var(from_uri_tag)){
									$avp(recent_uuid) = $json(dlg_info/values/UID);
									$avp(recent_location) = $json(dlg_info/values/asterisk_box);
									
									$var(min_start) = $var(dlg_start);
								} else
									xlog("XXXX - skipping pickup for call originated by $var(first_tag) \n");
							} else {
								$avp(recent_uuid) = $json(dlg_info/values/UID);
								$avp(recent_location) = $json(dlg_info/values/asterisk_box);
								
								$var(min_start) = $var(dlg_start);
							}
						}
					}

					if ($avp(recent_uuid) != NULL) {
						append_hf("X-APP-UUID: $avp(recent_uuid) \r\n");
						$du = $avp(recent_location);
						$avp(asterisk_picked) = 1;

						$avp(app_call_pickup) = 1;
					}
				} 
			}

			#if ($json(app_data/app_name) != NULL) {
			#	append_hf("X-APP-NAME: $json(app_data/app_name) \r\n");
			#}	
			#if ($json(app_data/app_target_id) != NULL) {
			#	append_hf("X-APP-TARGETID: $json(app_data/app_target_id) \r\n");
			#}	
		}

        } else if (do_routing(2,"C",,$avp(rule_attrs),,,$avp(partition))) {
                $avp(app_call) = 1;
                $var(len) = $(avp(acc_rule_prefix){s.len});
                $rU = $(rU{s.substr,$var(len),0});

                $json(app_attrs) := $avp(rule_attrs);
		$json(app_data) := $json(app_attrs/app_data);

		xlog("XXXX - Dialing $rU Matched app dialing $avp(acc_rule_prefix) -> $json(app_attrs) for account $avp(accountid) , $json(app_data) , -> $json(app_data/app_name) \n");

		$json(app_tags) := $json(app_attrs/app_tags);
		if ($json(app_tags) != NULL) {
			xlog("XXX - found app tags $json(app_tags) for $ci \n");
			for ($var(tag) in $(json(app_tags)[*])) {
				set_dlg_profile("tags","$var(tag)");
				$json(a_tags[]) = $var(tag);
			}
		}

		if ($json(app_data) != NULL) {
			xlog("XXX - found app data $json(app_data) for $ci \n");

			for ($var(k) in $(json(app_data.keys)[*])) {
				$var(h_k) = "X-"+$(var(k){s.toupper}{re.subst,/_/-/g});
				$var(v) = $json(app_data/$var(k));

				
				if ($var(h_k) != "X-APP-TAG") {	
					append_hf("$var(h_k) : $var(v)\r\n");
					if ($DLG_status!=NULL ) {
						store_dlg_value("$var(h_k)","$var(v)");
					}
				}
			} 

			if ($json(app_data) != NULL && ( $json(app_data/app_name) == "pickup" || $json(app_data/app_name) == "whisper")) {
				$var(app_tag) = $json(app_data/app_tag);
			
				if ($json(app_data/app_name) == "pickup")
					$var(search_query) = '[{"tags":' + $var(app_tag) + ',"min_state": 0,"max_state":3}]';
				else
					$var(search_query) = '[{"tags":' + $var(app_tag) + ',"min_state": 3,"max_state":5}]';

				xlog("XXXX - $oU is $json(app_data/app_name) targeting tag $var(app_tag) \n");

				xlog("XXXX - running1 /root/search_tags.py '$var(search_query)' \n");

				exec("/root/search_tags.py '$var(search_query)'",,$avp(dlg_jsons));
		
				xlog("XXXX - got back $avp(dlg_jsons) \n");

				$json(search_tags) := $var(app_tag);
				$var(first_tag) = $json(search_tags[0]);

				$json(ret_dlgs) := $avp(dlg_jsons);
				if ($json(ret_dlgs) != NULL) {

					$avp(recent_uuid) := NULL;
					$avp(recent_location) := NULL;
					$var(min_start) = 2147483646;
					
					for ($var(ret_arr) in $(json(ret_dlgs)[*])) {
						$json(ret_arr) := $var(ret_arr);
						for ($var(q_rpl) in $(json(ret_arr)[*])) {
							xlog("BBBBB - got dlg rpl $var(q_rpl) \n");

							$json(dlg_info) := $var(q_rpl);
							$var(dlg_state) = $(json(dlg_info/state){s.int});
							$var(dlg_start) = $(json(dlg_info/values/create_ts){s.int});

							if ($json(app_data/app_name) == "pickup" && $var(first_tag) =~ "^u") {
								$var(from_uri_tag) = "u" + $(json(dlg_info/from_uri){uri.user});

								xlog("XXXXX - pickup targetting user $var(first_tag) - from generated tag = $var(from_uri_tag) \n");
								
								if ($var(first_tag) != $var(from_uri_tag)){
									$avp(recent_uuid) = $json(dlg_info/values/UID);
									$avp(recent_location) = $json(dlg_info/values/asterisk_box);
									
									$var(min_start) = $var(dlg_start);
								} else
									xlog("XXXX - skipping pickup for call originated by $var(first_tag) \n");
							} else {
								$avp(recent_uuid) = $json(dlg_info/values/UID);
								$avp(recent_location) = $json(dlg_info/values/asterisk_box);
								
								$var(min_start) = $var(dlg_start);
							}
						}
					}

					if ($avp(recent_uuid) != NULL) {
						append_hf("X-APP-UUID: $avp(recent_uuid) \r\n");
						$du = $avp(recent_location);
						$avp(asterisk_picked) = 1;

						$avp(app_call_pickup) = 1;
					}
				} 
			}

			#if ($json(app_data/app_name) != NULL) {
			#	append_hf("X-APP-NAME: $json(app_data/app_name) \r\n");
			#}	
			#if ($json(app_data/app_target_id) != NULL) {
			#	append_hf("X-APP-TARGETID: $json(app_data/app_target_id) \r\n");
			#}	
		}
        } else {
		$avp(app_call) = 0;
		$avp(app_call_pickup) = 0;
	}


	# $avp(ds_set) holds the users's dispatcher set
	# $avp(ds_alg) holds the users's dispatcher algorithm
#	if ($fU == "alex1") {
#		xlog("XXXX - forcing ds set to 4 \n");
#		$avp(ds_set) = 4;
#	} else {
#		xlog("XXXX - forcing ds set to 1 \n");
#		$avp(ds_set) = 1;
#	}
	$avp(ds_alg) = 4;
     $dlg_val(socket_name) = "EXTERNAL";
	if ( $avp(app_data) == NULL && route(do_speeddial_lookup) ) {
		if ($avp(sd_setid)!=NULL)
			$avp(ds_set):=$avp(sd_setid);
		if ($avp(sd_alg)!=NULL)
			$avp(ds_alg):=$avp(sd_alg);

		set_dlg_profile("tags","e$rU");
		$json(a_tags[]) = "e" + $rU ;
    }

	#if ($rU =~ "^[0-9]{10}$")
	#	prefix("1");
	#if ($rU =~ "^011")
	#	strip(3);

	if ($avp(speeddial_fail) == 1) {
		xlog("XXXX - looking up $rU through the A-Z dialplan \n");
		$avp(partition) = "a_z_dialplan";
		if (do_routing(0,"C",,$avp(rule_attrs),,,$avp(partition))) {
			$json(pstn_data) := $avp(rule_attrs);

			if ($json(pstn_data) != NULL) {
				for ($var(k) in $(json(pstn_data.keys)[*])) {
					$var(h_k) = "X-"+$(var(k){s.toupper}{re.subst,/_/-/g});
					$var(v) = $json(pstn_data/$var(k));

				
					append_hf("$var(h_k) : $var(v)\r\n");
      					if ($DLG_status!=NULL ) {
						store_dlg_value("$var(h_k)","$var(v)");
					}
				} 
			}
		} else {
			# just let FS decide, send to it
		#	if ($avp(app_call) == 0) {
		#		send_reply("503","Unavailable");
		#		exit;
		#	}
		}
	}

	if ($avp(app_call_pickup) == 1 && $json(app_data/app_name) == "queue") {
		$var(app_tag) = $json(app_data/app_tag);
		$json(app_data/app_name) = "QueuePickup";

		$var(search_query) = '[{"tags":' + $var(app_tag) + ',"min_state": 3,"max_state":5}]';

		xlog("XXXX - running3 /root/search_tags.py '$var(search_query)' \n");

		exec("/root/search_tags.py '$var(search_query)'",,$avp(dlg_jsons));

		xlog("XXXX - got back $avp(dlg_jsons) \n");

		$json(search_tags) := $var(app_tag);
		$var(first_tag) = $json(search_tags[0]);

		$json(ret_dlgs) := $avp(dlg_jsons);
		if ($json(ret_dlgs) != NULL) {
			$avp(recent_uuid) := NULL;
			$avp(recent_location) := NULL;
			$var(min_start) = 2147483646;
			
			for ($var(ret_arr) in $(json(ret_dlgs)[*])) {
				$json(ret_arr) := $var(ret_arr);
				for ($var(q_rpl) in $(json(ret_arr)[*])) {
					xlog("BBBBB - got dlg rpl $var(q_rpl) \n");

					$json(dlg_info) := $var(q_rpl);
					$var(dlg_state) = $(json(dlg_info/state){s.int});
					$var(dlg_start) = $(json(dlg_info/values/create_ts){s.int});

					if ($json(app_data/app_name) == "pickup" && $var(first_tag) =~ "^u") {
						$var(from_uri_tag) = "u" + $(json(dlg_info/from_uri){uri.user});

						xlog("XXXXX - pickup targetting user $var(first_tag) - from generated tag = $var(from_uri_tag) \n");
						
						if ($var(first_tag) != $var(from_uri_tag)){
							$avp(recent_uuid) = $json(dlg_info/values/UID);
							$avp(recent_location) = $json(dlg_info/values/asterisk_box);
							
							$var(min_start) = $var(dlg_start);
						} else
							xlog("XXXX - skipping pickup for call originated by $var(first_tag) \n");
					} else {
						$avp(recent_uuid) = $json(dlg_info/values/UID);
						$avp(recent_location) = $json(dlg_info/values/asterisk_box);
						
						$var(min_start) = $var(dlg_start);
					}
				}
			}

			if ($avp(recent_uuid) != NULL) {
				if ($avp(app_call_pickup) == 1)
					append_hf("X-T-APP-UUID: $avp(recent_uuid) \r\n");
				else {
					append_hf("X-APP-UUID: $avp(recent_uuid) \r\n");
					if ($avp(asterisk_picked) == 0) {
						$du = $avp(recent_location);
						$avp(asterisk_picked) = 1;
					}
				}
			}
		}

	} else if ($json(app_data) != NULL && ( $json(app_data/app_name) == "pickup" || $json(app_data/app_name) == "whisper")) {
		$var(app_tag) = $json(app_data/app_tag);
		xlog("XXXX - $rU is $json(app_data/app_name) targeting tag $var(app_tag) \n");

		if ($json(app_data/app_name) == "pickup")
			$var(search_query) = '[{"tags":' + $var(app_tag) + ',"min_state": 0,"max_state":3}]';
		else
			$var(search_query) = '[{"tags":' + $var(app_tag) + ',"min_state": 3,"max_state":5}]';

		xlog("XXXX - running2 /root/search_tags.py '$var(search_query)' \n");

		exec("/root/search_tags.py '$var(search_query)'",,$avp(dlg_jsons));

		xlog("XXXX - got back $avp(dlg_jsons) \n");

		$json(search_tags) := $var(app_tag);
		$var(first_tag) = $json(search_tags[0]);

		$json(ret_dlgs) := $avp(dlg_jsons);
		if ($json(ret_dlgs) != NULL) {

			$avp(recent_uuid) := NULL;
			$avp(recent_location) := NULL;
			$var(min_start) = 2147483646;
			
			for ($var(ret_arr) in $(json(ret_dlgs)[*])) {
				$json(ret_arr) := $var(ret_arr);
				for ($var(q_rpl) in $(json(ret_arr)[*])) {
					xlog("BBBBB - got dlg rpl $var(q_rpl) \n");

					$json(dlg_info) := $var(q_rpl);
					$var(dlg_state) = $(json(dlg_info/state){s.int});
					$var(dlg_start) = $(json(dlg_info/values/create_ts){s.int});

					if ($json(app_data/app_name) == "pickup" && $var(first_tag) =~ "^u") {
						$var(from_uri_tag) = "u" + $(json(dlg_info/from_uri){uri.user});

						xlog("XXXXX - pickup targetting user $var(first_tag) - from generated tag = $var(from_uri_tag) \n");
						
						if ($var(first_tag) != $var(from_uri_tag)){
							$avp(recent_uuid) = $json(dlg_info/values/UID);
							$avp(recent_location) = $json(dlg_info/values/asterisk_box);
							
							$var(min_start) = $var(dlg_start);
						} else
							xlog("XXXX - skipping pickup for call originated by $var(first_tag) \n");
					} else {
						$avp(recent_uuid) = $json(dlg_info/values/UID);
						$avp(recent_location) = $json(dlg_info/values/asterisk_box);
						
						$var(min_start) = $var(dlg_start);
					}
				}
			}

			if ($avp(recent_uuid) != NULL) {
				if ($avp(app_call_pickup) == 1)
					append_hf("X-T-APP-UUID: $avp(recent_uuid) \r\n");
				else {
					append_hf("X-APP-UUID: $avp(recent_uuid) \r\n");
					if ($avp(asterisk_picked) == 0) {
						$du = $avp(recent_location);
						$avp(asterisk_picked) = 1;
					}
				}
			}
		} 
	}

	if ($avp(sd_target) != NULL)
		$rU = $avp(sd_target);

	if ($avp(app_call) == 1) {
		append_hf("X-APP-TARGET: $avp(full_username)\r\n");
      		if ($DLG_status!=NULL ) {
			store_dlg_value("X-APP-TARGET","$avp(full_username)");
		}
	}

    	xlog("SCRIPT:DBG: checking speeddial for $fU with accountid $avp(accountid)\n");

	$json(rule_attrs) := $avp(rule_attrs);
	$avp(sd_accountid) = $json(rule_attrs/accountid);
	$avp(sd_set) = $json(rule_attrs/setid);
	$avp(sd_alg) = $json(rule_attrs/alg);
			$json(app_data) := $json(rule_attrs/app_data);
	if ($json(app_data) != NULL) {
		xlog("XXX - found app data $json(app_data) for $ci \n");
	}

	# KKVLAD here - pick top most extension in array
	$json(subs_attrs) := $avp(subs_attrs);
	$avp(sd_user) = $json(subs_attrs/extensions[0]);
	$avp(caller_name) = $json(subs_attrs/attrs/caller_name);

	$json(headers_info) := $json(subs_attrs/attrs/headers);
	xlog("ZZZ - headers info $json(headers_info) for $ci \n");

	if ($json(headers_info) != NULL) {
		for ($var(k) in $(json(headers_info.keys)[*])) {
			$var(h_k) = "X-"+$(var(k){s.toupper}{re.subst,/_/-/g});
			$var(v) = $json(headers_info/$var(k));

			append_hf("$var(h_k) : $var(v)\r\n");
      			if ($DLG_status!=NULL ) {
				store_dlg_value("$var(h_k)","$var(v)");
			}
		} 
	}

	$json(subscriber_tags) := $json(subs_attrs/attrs/tags);
	if ($json(subscriber_tags) != NULL) {
		xlog("XXX - found app tags $json(subscriber_tags) for $ci \n");
		for ($var(tag) in $(json(subscriber_tags)[*])) {
			set_dlg_profile("tags","$var(tag)");
			$json(a_tags[]) = $var(tag); 
		}
	}

	$dlg_val(rmq_headers) = $(json(subs_attrs/attrs/rmq_headers){s.encode.hexa});
	xlog("XXXX - set rmq headers to $dlg_val(rmq_headers) from $(json(subs_attrs/attrs/rmq_headers)) \n");

	xlog("ZZZ we fetched ext $avp(sd_user) \n");

	if ($var(jwt_auth) == 1) {
			$avp(cdr_from_ruri) = "sip:"+$avp(emailName)+"@"+$fd;
			uac_replace_from("$avp(emailName)","$avp(cdr_from_ruri)"); 
	} else {
		if ($avp(sd_user) == NULL) {
				xlog("SCRIPT:DBG: no speeddial found for   target $fU $ci \n");
				$avp(cdr_from_ruri) = $fu;
			if (dp_translate($avp(pstn_dialplan),$fU,$var(new_fu))) {
				xlog("XXX - translated $fU to $var(new_fu) for call $ci \n");
				uac_replace_from("$var(new_fu)","sip:$var(new_fu)@$fd");
			}
		} else {
			$avp(cdr_from_ruri) = "sip:" + $avp(sd_user) + "@" + $fd;
			if ($avp(caller_name) != NULL) {
				dp_translate($avp(pstn_dialplan),$avp(sd_user),$avp(sd_user)); 
				uac_replace_from("$avp(caller_name)","sip:$avp(sd_user)@$fd");
			} else {
				dp_translate($avp(pstn_dialplan),$avp(sd_user),$avp(sd_user)); 
				uac_replace_from("$avp(sd_user)","sip:$avp(sd_user)@$fd");
			}
		}
	}

#    	if ($avp(accountid) == NULL)
#    		$avp(accountid) = 0;
#
#        	avp_db_query("select speeddial from speeddial where accountid=$avp(accountid) and target='$fU'", "$avp(sd_user)");
#        	if ($retcode<0 ||  $avp(sd_user)==NULL) {
#        		xlog("SCRIPT:DBG: no speeddial found for   target $fU $ci ---select speeddial from speeddial where accountid=$avp(accountid) and target='$fU'< \n");
#			$avp(cdr_from_ruri) = $fu;
#        	}else{
#
#			$avp(cdr_from_ruri) = "sip:" + $avp(sd_user) + "@" + $fd;
#        		uac_replace_from("sip:$avp(sd_user)@$fd");
#        	}
#

	xlog("XXX - checking if any translations for $avp(pstn_dialplan) \n");
	if (dp_translate($avp(pstn_dialplan),$rU,$var(new_ru))) {
		xlog("XXX - translated $rU to $var(new_ru) for call $ci \n");
		$rU = $var(new_ru);
	}

#    if($avp(ds_set)== 1){
          route(asterisk_officering_route);
#     }

    if($avp(ds_set)== 3){
          route(conference_officering_route);
     }


	if ( $ru=~"^sip:\+1[1-9][0-9]{9}@" || $ru=~"^sip:1[1-9][0-9]{9}@" || $ru=~"^sip:[1-9][0-9]{9}@" ||   $ru=~"^sip:011[1-9][0-9]{7}*@" ||   $ru=~"^sip:[2-9]11@") {
		append_hf("X-PSTN-SET: $avp(pstn_ds_set)\r\n");
      		if ($DLG_status!=NULL ) {
			store_dlg_value("X-PSTN-SET","$avp(pstn_ds_set)");
		}
	}

	append_hf("X-APP-TAG: $json_compact(a_tags) \r\n");

	route(to_server);
	exit;
}

route[asterisk_officering_route]
{
	xlog("asterisk_officering_route --->  for call $ci from $si\n");

	 force_send_socket(udp:MY_PRIV_IP:5060);

	  $dlg_val(socket_name) = "INTERNAL";
 remove_hf("X-CUSTOMER");
 if (!is_present_hf("X-Twilio-AccountSid"))
   append_hf("X-CUSTOMER: $avp(accountid)\r\n");
 $avp(os_uuid) = $UUID;
 xlog("ZZZZ - just generated $avp(os_uuid) for $ci \n");
 append_hf("X-GUID: $avp(os_uuid)\r\n");
      if ($DLG_status!=NULL ) {
		store_dlg_value("X-CUSTOMER","$avp(accountid)");
		store_dlg_value("X-GUID","$avp(os_uuid)");
	}
#route(insert_rtpproxy);
}
route[conference_officering_route]
{
	xlog("conference_officering_route --->  for call $ci from $si\n");

	 force_send_socket(udp:MY_PRIV_IP:5060);
     $dlg_val(socket_name) = "INTERNAL";
      remove_hf("X-CONF-D");
     append_hf("X-CONF-D: $avp(accountid)\r\n");
     remove_hf("X-CONF-E");
     append_hf("X-CONF-E: $rU\r\n");
      if ($DLG_status!=NULL ) {
		store_dlg_value("X-CONF-D","$avp(accountid)");
		store_dlg_value("X-CONF-E","$rU");
	}
#route(insert_rtpproxy);
}


route[freeswitch_headers]
{
	xlog("SCRIPT:FS-XML-URL:DBG:  route freeswitch_headers  for call $ci from $si\n");
    remove_hf("X-FS-XML-URL");
#    append_hf("X-FS-XML-URL: 188.120.135.182:8003 \r\n");
 append_hf("X-FS-XML-URL: 185.138.168.167:3003 \r\n");
      if ($DLG_status!=NULL ) {
		store_dlg_value("X-FS-XML-URL","185.138.168.167:3003");
	}

route(insert_rtpproxy);
    return ;

}

route[insert_rtpengine] {
	if (has_totag()) {
		# fetch it
		$avp(rtpproxy_set) = $(dlg_val(rtpproxy_set){s.int});
		if ($avp(rtpproxy_set) != NULL)
			$avp(rtpproxy_enabled) = 1;
		else
			$avp(rtpproxy_enabled) = 0;
	} else {
		$var(rtpproxy_enabled) = $avp(rtpproxy_enabled);
		$avp(rtpproxy_enabled) = $(avp(rtpproxy_enabled){s.int});

		# store it
		if ($avp(force_rtpp) == 1) {
			$avp(rtpproxy_enabled) = 1;
		}

		if ($avp(rtpproxy_enabled) > 0) {
			$avp(rtpproxy_set) = $var(rtpproxy_enabled);

			$dlg_val(rtpproxy_set) = $avp(rtpproxy_set);
			$avp(rtpproxy_set) = $(avp(rtpproxy_set){s.int});
		}
	}

	$avp(rtpproxy_set) = $(avp(rtpproxy_set){s.int});
	xlog("XXXX - using set $avp(rtpproxy_set) for $ci - enabled = $avp(rtpproxy_enabled) \n");
	rtpengine_use_set($avp(rtpproxy_set));
	$avp(offer_flags) = "address-family=IP4 ";
        if ( has_body("application/sdp") && $avp(rtpproxy_enabled) > 0) {
                if ($si =~ "192.168") {
                        if ($rd =~ "192.168") {
                                if ($dd != NULL && $dd !~ "192.168") {
                                        $avp(offer_flags) = $avp(offer_flags) + "in-iface=internal out-iface=external symmetric";
                                        $avp(rtpengine_mode) = "ie";

                                        # not internal, call goes out
                                        if (!nat_uac_test(8))
                                                $avp(offer_flags) = $avp(offer_flags) + " trust-address";
                                } else {
                                        xlog("ZZZ - internal \n");
                                        # internal to internal
                                        $avp(in2in) = 1;

                                        $avp(offer_flags) = $avp(offer_flags) + "in-iface=internal out-iface=internal symmetric";
                                        $avp(rtpengine_mode) = "ii";

                                        if (!nat_uac_test(8))
                                                $avp(offer_flags) = $avp(offer_flags) + " trust-address";
                                }
                        } else {
                                $avp(offer_flags) = $avp(offer_flags) + "in-iface=internal out-iface=external symmetric";
                                $avp(rtpengine_mode) = "ie";

                                # not internal, call goes out
                                if (!nat_uac_test(8))
                                        $avp(offer_flags) = $avp(offer_flags) + " trust-address";
                        }
                        xlog("XXX - offer from internal forced on grp $avp(rtpproxy_set)\n");
                } else {
                        xlog("XXX - offer from external forced on grp $avp(rtpproxy_set) \n");
                    if ($rd =~ "192.168") {
                                if ($dd != NULL && $dd !~ "192.168") {
                                        # going out
                                        $avp(offer_flags) = $avp(offer_flags) + "in-iface=external out-iface=external symmetric";
                                        $avp(rtpengine_mode) = "ee";
                                } else {
                                        $avp(offer_flags) = $avp(offer_flags) + "in-iface=external out-iface=internal symmetric";
                                        $avp(rtpengine_mode) = "ei";
                                }
                        } else {
                                $avp(offer_flags) = $avp(offer_flags) + "in-iface=external out-iface=external symmetric";
                                        $avp(rtpengine_mode) = "ee";
                        }

                        if (!nat_uac_test(8))
                                $avp(offer_flags) = $avp(offer_flags) + " trust-address";
                }

                if (isflagset(SRC_WS) && isbflagset(DST_WS))
                        $avp(offer_flags) = $avp(offer_flags) + " ICE=force-relay DTLS=passive SDES-off";
                else if (isflagset(SRC_WS) && !isbflagset(DST_WS))
                        $avp(offer_flags) = $avp(offer_flags) + " RTP/AVP replace-session-connection replace-origin ICE=remove SDES-off";
                else if (!isflagset(SRC_WS) && isbflagset(DST_WS))
                        $avp(offer_flags) = $avp(offer_flags) + " UDP/TLS/RTP/SAVPF ICE=force SDES-off";
                else if (!isflagset(SRC_WS) && !isbflagset(DST_WS))
                        $avp(offer_flags) = $avp(offer_flags) + " RTP/AVP replace-session-connection replace-origin ICE=remove SDES-off";

                rtpengine_offer("$avp(offer_flags)");

                if ($avp(rtpproxy_set) == 2)
                        rtpengine_start_recording();

                set_dlg_flag(1); /*RTPP used*/
                xlog("SCRIPT:RTPP:DBG:  RTPengine used is done  for call $ci from $si\n");

                t_on_reply("reply_handling");
        }
}

route[insert_rtpproxy]
{
	$avp(rtpproxy_enabled) = 1;
	$avp(force_rtpp) = 1;
	xlog("SCRIPT:RTPP:DBG:socket_name:$dlg_val(socket_name) $rm enabling rtpproxy for call $ci from $si - $avp(rtpproxy_enabled) - $avp(force_rtpp) \n");
	route(insert_rtpengine);
	return;

	if (has_totag()) {
		# fetch it
		$avp(rtpproxy_set) = $(dlg_val(rtpproxy_set){s.int});
		if ($avp(rtpproxy_set) != NULL)
			$avp(rtpproxy_enabled) = 1;
		else
			$avp(rtpproxy_enabled) = 0;
	} else {
		$var(rtpproxy_enabled) = $avp(rtpproxy_enabled);
		$avp(rtpproxy_enabled) = $(avp(rtpproxy_enabled){s.int});

		# store it
		if ($avp(force_rtpp) == 1) {
			$avp(rtpproxy_enabled) = 1;
		}

		if ($avp(rtpproxy_enabled) > 0) {
			$avp(rtpproxy_set) = $var(rtpproxy_enabled);

			$dlg_val(rtpproxy_set) = $avp(rtpproxy_set);
			$avp(rtpproxy_set) = $(avp(rtpproxy_set){s.int});
		}
	}

	if ( has_body("application/sdp") && $avp(rtpproxy_enabled) > 0) {
                if ($si =~ "192.168") {
                        if ($rd =~ "192.168") {
                                if ($dd != NULL && $dd !~ "192.168") {
					# not internal, call goes out
                                        if (nat_uac_test(8))
                                                rtpproxy_offer("coies",,$avp(rtpproxy_set));
                                        else
                                                rtpproxy_offer("coiers",,$avp(rtpproxy_set));
                                } else {
                                        xlog("ZZZ - internal \n");
					# internal to internal
                                        $avp(in2in) = 1;

                                        if (nat_uac_test(8))
                                                rtpproxy_offer("coiis",,$avp(rtpproxy_set));
                                        else
                                                rtpproxy_offer("coiirs",,$avp(rtpproxy_set));
                                }
                        } else {
                                if (nat_uac_test(8))
                                        rtpproxy_offer("coies",,$avp(rtpproxy_set));
                                else
                                        rtpproxy_offer("coiers",,$avp(rtpproxy_set));
                        }
                        xlog("XXX - offer from internal forced on grp $avp(rtpproxy_set)\n");
                } else {
                        if (nat_uac_test(8))
                                rtpproxy_offer("coeis",,$avp(rtpproxy_set));
                        else
                                rtpproxy_offer("coeirs",,$avp(rtpproxy_set));
                        xlog("XXX - offer from external forced on grp $avp(rtpproxy_set) \n");
                }


		if ($avp(rtpproxy_set) == 2)
			rtpproxy_start_recording($avp(rtpproxy_set));

		set_dlg_flag(1); /*RTPP used*/
		xlog("SCRIPT:RTPP:DBG:  RTPP used is done  for call $ci from $si\n");
	}

	t_on_reply("reply_handling");
}


route[to_pstn]
{
	# answer to all OPTIONS initial requests
	if (is_method("OPTIONS")) {
		send_reply(200,"OK");
		exit;
	}
     if ($DLG_status==NULL ) {
                if ( !create_dialog("B") ) {
                     xlog("DLG ---  CALL WITH OUT DIALOG INFO Huston we have a problem .....$ci\n");
                }
		$dlg_val(create_ts) = $Ts;
		$dlg_val(callerid) = $fU;
		if ($fn != NULL && $fn != "") {
			if ($fn =~ "^\"")		
				$var(fn) = $(fn{s.select,1,"});
			else
				$var(fn) = $fn;
			$dlg_val(calleridname) = $(var(fn){s.escape.common});
		} else
			$dlg_val(calleridname) = $fU;
		if ($ai != NULL && $ai != "")
			$dlg_val(pai) = $ai;
		if ($re != NULL && $re != "")
			$dlg_val(rpid) = $re;
		trace("hep_tracer","D");
		if (!has_totag())
			set_dlg_sharing_tag("MY_CLUSTER_NODE_ID");
      } else {
                xlog(" in-dialog valid request - $DLG_dir !\n");
      }

     $dlg_val(to_pstn) = "1";
     append_hf("X-HCID: c$hdr(X-CUSTOMER)\r\n");
     $avp(os_uuid) = $UUID;
     xlog("ZZZZ - just generated $avp(os_uuid) for $ci \n");
     append_hf("X-GUID: $avp(os_uuid)\r\n");
      if ($DLG_status!=NULL ) {
		store_dlg_value("X-HCID","c$hdr(X-CUSTOMER)");
		store_dlg_value("X-GUID","$avp(os_uuid)");
	}
#         force_send_socket(udp:ANYCAST_IP:5060);
         force_send_socket(udp:MY_IP:5060);
         $dlg_val(socket_name) = "EXTERNAL";
         $avp(ds_set) = 2;

	 if (is_present_hf("X-PSTN-SET"))
		$avp(ds_set) = $(hdr(X-PSTN-SET){s.int});
	 else
         	$avp(ds_set) = 2;

         $avp(ds_alg) = 4;
         $avp(rtpproxy_enabled)=1;
         t_on_reply("reply_handling");

	$avp(cdr_from_ruri) = $fu;

  	#$avp(hdr_value) := NULL;
	#  $avp(hdr_value) = "ToPSTNCall";
	#  route(push_init_pstn_rabbitmq_event);

         route(to_server);

}
route[from_pstn]{
 	$avp(os_uuid) = $UUID;
	xlog("ZZZZ - just generated $avp(os_uuid) for $ci \n");

      if ($DLG_status==NULL ) {
                if ( !create_dialog("B") ) {
                     xlog("DLG ---  CALL WITH OUT DIALOG INFO Huston we have a problem .....$ci\n");
                }
		$dlg_val(create_ts) = $Ts;
		$dlg_val(callerid) = $fU;
		if ($fn != NULL && $fn != "") {
			if ($fn =~ "^\"")		
				$var(fn) = $(fn{s.select,1,"});
			else
				$var(fn) = $fn;
			$dlg_val(calleridname) = $(var(fn){s.escape.common});
		} else
			$dlg_val(calleridname) = $fU;
		if ($ai != NULL && $ai != "")
			$dlg_val(pai) = $ai;
		if ($re != NULL && $re != "")
			$dlg_val(rpid) = $re;
		trace("hep_tracer","D");
		if (!has_totag())
			set_dlg_sharing_tag("MY_CLUSTER_NODE_ID");
      } else {
                xlog(" in-dialog valid request - $DLG_dir !\n");
      }
	$dlg_val(from_pstn) = "1";
	 force_send_socket(udp:MY_PRIV_IP:5060);
     $dlg_val(socket_name) = "INTERNAL";
    $avp(ds_set) = 1;
     $avp(ds_alg) = 4;
     t_on_reply("reply_handling");
     $avp(rtpproxy_enabled)=1;
     $avp(serviceKey)="from_pstn";




	# KKVLAD here - match pstn did
	if ($rU =~ "^\+")
		strip(1);

	$avp(partition) = "pstn_dids";
	if (!do_routing(0,"LC",,$avp(rule_attrs),,,$avp(partition))) {
		xlog("SCRIPT:DBG: no record found for DID $rU $ci\n");

		if (is_present_hf("X-Twilio-AccountSid")) {
			$avp(subs_attrs) := NULL;
		        avp_db_query("select FN_GetSimIDData('$hdr(X-Twilio-AccountSid)') as subs_attrs","$avp(subs_attrs)");
		        if ($avp(subs_attrs) != NULL) {
				xlog("XXX - fetched twillion $hdr(X-Twilio-AccountSid) sim with attrs $avp(subs_attrs) \n");

				$json(subs_attrs) := $avp(subs_attrs);
				$avp(ds_alg) = $json(subs_attrs/ds_alg);	
				$avp(accountid) = $json(subs_attrs/attrs/headers/customer);	
				$avp(rtpproxy_enabled) = $json(subs_attrs/rtpproxy_enabled);
				$avp(pstn_ds_set) = $json(subs_attrs/pstn_ds_set);

				route(post_subscriber_auth);
				exit;
			}
		}

		send_reply(404,"Not found");
		exit;
	}

	$json(rule_attrs) := $avp(rule_attrs);
	$avp(sd_accountid) = $json(rule_attrs/accountid);
	$avp(ds_set) = $json(rule_attrs/setid);
	$avp(ds_alg) = $json(rule_attrs/alg);
			$json(app_data) := $json(rule_attrs/app_data);
	if ($json(app_data) != NULL) {
		xlog("XXX - found app data $json(app_data) for $ci \n");

		for ($var(k) in $(json(app_data.keys)[*])) {
			$var(h_k) = "X-"+$(var(k){s.toupper}{re.subst,/_/-/g});
			$var(v) = $json(app_data/$var(k));

			append_hf("$var(h_k) : $var(v)\r\n");

      			if ($DLG_status!=NULL ) {
				store_dlg_value("$var(h_k)","$var(v)");
			}
		} 

		#if ($json(app_data/app_name) != NULL) {
		#	append_hf("X-APP-NAME: $json(app_data/app_name) \r\n");
		#}	
		#if ($json(app_data/app_target_id) != NULL) {
		#	append_hf("X-APP-TARGETID: $json(app_data/app_target_id) \r\n");
		#}	
	}

#    	if ($avp(accountid) != NULL)
#    		$avp(accountid) = 0;
#
#        	avp_db_query("select  setid, alg ,accountid  from speeddial where speeddial.speeddial  = '$rU' and speeddial.pattern_type=3 ", "$avp(sd_set),$avp(sd_alg),$avp(sd_accountid)");
#        	if ($retcode<0 ||  $avp(sd_set)==NULL) {
#        		xlog("SCRIPT:DBG: no record found for speeddial $fU $ci  select  setid, alg ,accountid  from speeddial where speeddial.speeddial  = '$rU' and speeddial.pattern_type=3  \n");
#        		send_reply("404","Not found2");
#        		exit;
#        	}
        	 $avp(accountid)=$avp(sd_accountid);
            append_hf("X-CUSTOMER: $avp(accountid)\r\n");
     	    $avp(os_uuid) = $UUID;
	    xlog("ZZZZ - just generated $avp(os_uuid) for $ci \n");
	    append_hf("X-GUID: $avp(os_uuid)\r\n");

      	if ($DLG_status!=NULL ) {
		store_dlg_value("X-CUSTOMER","$avp(accountid)");
		store_dlg_value("X-GUID","$avp(os_uuid)");
	}

  $avp(cdr_from_ruri) = $fu;

#  $avp(hdr_value) := NULL;
#  $avp(hdr_value) = "FromPSTNCall";
#  route(push_init_pstn_rabbitmq_event);
  route(to_server);


}

route[from_server]
{
 if (is_present_hf("X-GUID"))
	 $avp(os_uuid) = $hdr(X-GUID);

    $var(device) = $tU ;
    $avp(device) = $tU ;	
    if($(hdr(X-VMC))){
    	$avp(ds_set) = 4;
    	$avp(ds_alg) = 4;
       route(to_server);
    }
    if($(hdr(X-CONF-UID))){
    	$avp(ds_set) = 3;
    	$avp(ds_alg) = 4;
       route(to_server);
    }
    if($(hdr(X-CONF-D))){
    	$avp(ds_set) = 5;
    	$avp(ds_alg) = 4;
       route(to_server);
    }


   if ($DLG_status==NULL ) {
                if ( !create_dialog("B") ) {
                     xlog("DLG ---  CALL WITH OUT DIALOG INFO Huston we have a problem .....$ci\n");
                }
		$dlg_val(create_ts) = $Ts;
		$dlg_val(callerid) = $fU;
		if ($fn != NULL && $fn != "") {
			if ($fn =~ "^\"")		
				$var(fn) = $(fn{s.select,1,"});
			else
				$var(fn) = $fn;
			$dlg_val(calleridname) = $(var(fn){s.escape.common});
		} else
			$dlg_val(calleridname) = $fU;
		if ($ai != NULL && $ai != "")
			$dlg_val(pai) = $ai;
		if ($re != NULL && $re != "")
			$dlg_val(rpid) = $re;
		trace("hep_tracer","D");
		if (!has_totag())
			set_dlg_sharing_tag("MY_CLUSTER_NODE_ID");
      } else {
                xlog(" in-dialog valid request - $DLG_dir !\n");
      }

	$dlg_val(freeswitch_box) = "sip:" + $si + ":" + $sp;

	if (is_present_hf("X-UUID")) {
		$dlg_val(UID) = $hdr(X-UUID);
	}
	if (is_present_hf("X-DIALOG")) {
		$json(dlg_vals) := $hdr(X-DIALOG);

		for ($var(k) in $(json(dlg_vals.keys)[*])) {
			$var(v) = $json(dlg_vals/$var(k));
			store_dlg_value("$var(k)","$var(v)");
		}
	}
	
	if (is_present_hf("X-APP-TAG")) {
		$json(dlg_tags) := $hdr(X-APP-TAG);
		for ($var(v) in $(json(dlg_tags)[*]))
			set_dlg_profile("tags","$var(v)");
	}
	
	$dlg_val(device) = $tU;
	if ($dlg_val(LinkedCallid) == NULL && $hdr(X-CID) != NULL) {
		$dlg_val(LinkedCallid) = $hdr(X-CID);
	}

	if (!is_in_profile("device"))
		set_dlg_profile("device","$dlg_val(device)");
		

	xlog("MY11111111 update_dialog_vars   $tU $rm $ci from: $fU");
	route(update_dialog_vars);


	xlog("SCRIPT:DBG: $rm from server $si for $ru $ci\n");
	

#	if (!avp_db_query("select subscriber.rtpproxy_enabled ,subscriber.username    from subscriber inner JOIN speeddial on    speeddial.accountid = subscriber.accountid and    speeddial.target = subscriber.username  where speeddial.speeddial='$rU' and subscriber.domain='$rd'","$avp(rtpproxy_enabled),$avp(target_user)") || $avp(rtpproxy_enabled)==NULL) {
#		xlog("SCRIPT:FROM_SERVER:DBG: user $ru not found in subscriber table\n");
#		send_reply("404","Not found1");
#		exit;
#	}

#	$var(orig_ru) = $ru;
#	$rU = $fU;
#
#	if (!lookup("location","m")) {
#       		uac_replace_from("$fU","sip:$fU@$rd");
#		
#		xlog("ZZZ - no lookup :( - $ci \n");
#	} else {
#		xlog("ZZZ - lkookup $avp(extensions) for $ci \n");
#		$json(extensions) := $avp(extensions);
#		$avp(top_ext) = $json(extensions/extensions[0]);
#		
#		# cleanup lookup branches
#		$var(i) = 0;
#		while ($(branch(uri)[$var(i)]) != null) {
#			remove_branch($var(i));
#		}
#
#
#       		uac_replace_from("$avp(top_ext)","sip:$avp(top_ext)@$od");
#		xlog("ZZZ - lookup $fU to $avp(top_ext)  - $ci \n");
#	}
#
#	$ru = $var(orig_ru);

	$avp(cdr_to_ruri) = $ru;
	$avp(cdr_from_ruri) = $fu;
	$avp(cdr_domain) = $rd;

	$avp(dlg_correlation) = $avp(cdr_to_ruri);
	$dlg_val(dlg_correlation) = $avp(dlg_correlation);
        

	# KKVLAD here - direct extension dialing
	if ($rU =~ "^\+")
		strip(1);

	if ($avp(accountid) == NULL || $avp(accountid) == 0) {
                if (is_present_hf("X-CUSTOMER"))
                        $avp(accountid) = $hdr(X-CUSTOMER);
	}

	$avp(partition) = "account_" + $avp(accountid);

	$var(orig_ru) = $ru;
	$rU = $fU;

	if (!do_routing(1,"LC",,$avp(rule_attrs),,,$avp(partition))) {
		xlog("SCRIPT:DBG: no record found for DID $rU in partition $avp(partition) $ci\n");
		$avp(caller_username) = $fU;
	} else {
		$json(rule_attrs) := $avp(rule_attrs);
		#$avp(blf_caller) = "sip:" + $json(rule_attrs/username) + "@" + $od;

		$avp(caller_username) = $json(rule_attrs/username);
	}

	$ru = $var(orig_ru);

	
	$var(twillio_call) = 0;
	if ($rU =~ "[0-9]*" && do_routing(1,"LC",,$avp(rule_attrs),,,$avp(partition))) {
		xlog("XXXX - extension dialing from FS for $ci \n");
		$json(rule_attrs) := $avp(rule_attrs);
		$rU = $json(rule_attrs/username);

		$avp(callee_username) = $json(rule_attrs/username);
	} else if ($rU =~ "^SIM_TW*") {
		remove_hf_glob("X-*");
		strip(7);
		append_hf("X-SIMID: $rU\r\n");
		#$rU = "+18322467771";

		#$rd = "tortsims.sip.twilio.com";
		$rd = "rtclient.sip.twilio.com";
		#$rd = "myrtsim.sip.us1.twilio.com";
		uac_replace_to("","$ru");
#		if ($fn != NULL)
			#uac_replace_from("$fn","sip:$fU@ANYCAST_IP:5060");
#		else
			#uac_replace_from("","sip:$fU@ANYCAST_IP:5060");

		#topology_hiding();

		$var(twillio_call) = 1;
	} else {
		xlog("XXXX - regular dialing from FS for $ci \n");
		$avp(callee_username) = $rU;
	}

	set_dlg_profile("caller","$avp(caller_username)");
	set_dlg_profile("callee","$avp(callee_username)");

	# route call to user
	xlog("SCRIPT:DBG: sending call to local subscriber $ru\n");
	# do lookup with method filtering

	if ($(rd{ip.isip}) == 1) {
		$rd = "c" + $avp(accountid)+".ratetel.com";
	}

	$avp(blf_caller) = "sip:"+$avp(caller_username)+"@"+$rd;
	$avp(blf_callee) = "sip:"+$rU+"@"+$rd;
        xlog("SCRIPT:BLF:DBG: -update_dialog_vars--- B-side fU $avp(blf_caller) publishing callee $avp(blf_callee) state\n");
        dialoginfo_set("B");

	if ($var(twillio_call) == 1) {
		avp_db_query("select FN_GetSimIDData('$rU') as subs_attrs","$avp(subs_attrs)");
		$json(subs_attrs) := $avp(subs_attrs);
		$avp(rtpproxy_enabled) = $json(subs_attrs/rtpproxy_enabled);

#         	force_send_socket(udp:ANYCAST_IP:5060);
         	force_send_socket(udp:MY_IP:5060);
		#force_send_socket(udp:209.163.136.201:5060);
		t_newtran();

		route(insert_rtpproxy);
		t_on_reply("reply_handling");
		route(do_accounting);
		route(relay);

		exit;
	}

	t_newtran();

	## handle the NATed users (as target)
	xlog("XXX - setting reply handling $ci \n");
	#t_on_reply("reply_handling");

	t_on_branch("lookup_branching");

	avp_db_query("select FN_GetSubscriberPushTokens('$rU') as token_info","$avp(token_info)");
	$var(start) = 0;
	xlog("KKKK - checking tokens for $rU - fetched $avp(token_info) \n");

	if ($avp(token_info) != NULL) {
		$json(token_json) = NULL;
		$json(token_json) := $avp(token_info);	
		if ($json(token_json) != NULL) {
			for ($var(token) in $(json(token_json/TokenList)[*])) {
				xlog("KKKK - $rU has associated token $var(token) \n");
				
				if ($var(start) == 0) {
					t_wait_for_new_branches();
					$var(start) = 1;	
				}

				$avp(token) = $var(token);
			}
		}
	}

	if ($avp(token) != NULL) {
		remove("location", "$ru","","","PUSH_DEVICE");
		$avp(filter) = "aor="+$rU+"@"+$rd;
		xlog("XXXX - setting event_routing with filter $avp(filter) \n");

		$var(lookup_rc) = lookup("location","m");
		notify_on_event("E_UL_CONTACT_INSERT",$avp(filter), "fork_call", 30);

		$var(i) = 0;		
		while ($(avp(token)[$var(i)]) != NULL) {
			#curl --request POST   --url https://fcm.googleapis.com/fcm/send   --header 'Authorization: key=AAAA4tABBC4:APA91bEiqan73l0CsULKUc1BsIa9-Q5GJHSNhVIWPKI9sVd26Dc6JZ58diWQ_7K2WblGS0FYVuBSMSrgmIj_n9Pwbn9kg1qgqdhNwTbtTqfQyvDlPB335PnsmQDCM7aJ0S0OzLA77NpA'   --header 'Content-Type: application/json'   --header 'Postman-Token: da992324-854b-4b07-81bc-072f42c02ffc'   --header 'cache-control: no-cache'   --data '{ "data": {"title": "notification","body": "Wake Up"},"android":{"ttl":"0", "priority":"high"},"to" : "fJv_SCxoAsE:APA91bHSLU64mqwZpK0OPyaMr-SZKo1XtLvJZytbspIw8kXLnHTGrURi73bNxwW0WFApQwVhtJRSxOoZo0ImffcLTRnQqKOvvzxvhnPSTJxt5gDHxiwwxj40dEtAg98WqvK91jvGOQ4Y"}'			
			rest_append_hf("Authorization: key=AAAA4tABBC4:APA91bEiqan73l0CsULKUc1BsIa9-Q5GJHSNhVIWPKI9sVd26Dc6JZ58diWQ_7K2WblGS0FYVuBSMSrgmIj_n9Pwbn9kg1qgqdhNwTbtTqfQyvDlPB335PnsmQDCM7aJ0S0OzLA77NpA");
			rest_append_hf("Postman-Token: da992324-854b-4b07-81bc-072f42c02ffc");
			rest_append_hf("cache-control: no-cache");

			$avp(rest_body) = '{ "data": {"title": "notification","body": "Wake Up"},"android":{"ttl":"0", "priority":"high"},"to" : "' + $(avp(token)[$var(i)]) + '"}';
			
			xlog("KKKKK - doing push for $rU token $var(token) \n");
			#launch(rest_get("https://fcm.googleapis.com/fcm/send","$avp(rest_body)"));
			$var(rc) = rest_post("https://fcm.googleapis.com/fcm/send","$avp(rest_body)","application/json",$avp(recv_body));
	
			xlog("KKKKK - fetched $avp(recv_body) \n");

			$var(i) = $var(i) + 1;
		}

		$dlg_val(socket_name) = "EXTERNAL";

		# sl_send_reply("180", "Ringing");
		# exit & wait for registrations

		$avp(push_notification) = 1;
		if ($var(lookup_rc) > 0) {
			$var(attr) = $avp(subs_attrs);
			route(do_accounting);
			route(relay);
		}

		exit;
	} else {
		if (!lookup("location","m")) {
			xlog("XXXX - failed to find $ru - $ci \n");
			send_reply(480,"Temporarily Unavailable");
			exit;
		}else{
			$json(subs_attrs) := $avp(subs_attrs);
			$avp(rtpproxy_enabled) = $json(subs_attrs/rtpproxy_enabled);

			$dlg_val(socket_name) = "EXTERNAL";
		}
	}

	if (isbflagset(DST_NAT)) {
		xlog("XXXX - subscriber $ru is NATED for $ci \n");
	}

	route(do_accounting);
	route(relay);

	exit;
}

branch_route[lookup_branching] {
	if (is_present_hf("X-Alert-Info")) 
		append_hf("Alert-Info: $hdr(X-Alert-Info)\r\n"); 
	remove_hf_glob("X-*");

	if ($avp(push_notification) == 1) {
		$json(entry_attrs) := $var(attr);
		$avp(rtpproxy_enabled) = $json(entry_attrs/rtpproxy_enabled);
	} else {
		$var(idx) = $T_branch_idx;
		$json(entry_attrs) := $(avp(subs_attrs)[$var(idx)]);
		$avp(rtpproxy_enabled) = $json(entry_attrs/rtpproxy_enabled);
	}

	route(insert_rtpproxy);
	
	xlog("XXX - found $json(entry_attrs) for branch going to $ru $du - idx = $T_branch_idx\n");
	if ($json(entry_attrs/reg_server) != NULL && $json(entry_attrs/reg_server) != "sip:MY_PRIV_IP:5060") {
		# other node in our cluster, try to send it there
		remove_hf("X-DU");

		if ($du != NULL)
			append_hf("X-DU: $du\r\n");

		$du = $json(entry_attrs/reg_server);		

	 	force_send_socket(udp:MY_PRIV_IP:5060);
	} else {
		xlog("XXX - forcing public anycast \n");
		if (isbflagset(DST_WS)) {
			#force_send_socket(wss:ANYCAST_IP:8888); 
			xlog("KKK - forcing wss for $ci \n");
			force_send_socket(wss:MY_IP:8888);
		} else {
			if (is_present_hf("X-INTERFACE"))
				$fs = $hdr(X-INTERFACE);
			#else
			#	force_send_socket(udp:ANYCAST_IP:5060);
			#force_send_socket(udp:MY_IP:5060);
		}
	}
}

route[do_accounting] {
	xlog("XXXX - arming accounting for $ci \n");

	$dlg_val(device) = $var(device);
	if (!is_in_profile("device"))
		set_dlg_profile("device","$dlg_val(device)");
        $avp(device) = $var(device);

	$acc_extra(from_uri) = $avp(cdr_from_ruri);
	$acc_extra(to_uri) = $avp(cdr_to_ruri);
	$acc_extra(domain) = $avp(cdr_domain) ;
	$acc_extra(ruri) = $ct;
	$acc_extra(src_ip) = $si;
	$acc_extra(src_port) = $sp;
	$acc_extra(rpi) = $re;
        $acc_extra(contact) = $ct;
	if ($avp(accountid) == NULL || $avp(accountid) == 0) {
		if (is_present_hf("X-CUSTOMER"))
			$acc_extra(accountid) = $hdr(X-CUSTOMER);
	} else
		$acc_extra(accountid) = $avp(accountid);
	if ($avp(os_uuid) == NULL && is_present_hf("X-GUID"))
		$acc_extra(guid) = $hdr(X-GUID);
	else
		$acc_extra(guid) = $avp(os_uuid);

	xlog("ZZZZ - do accounting - $acc_extra(guid) for $ci \n");

	$acc_extra(direction) = $dlg_val(socket_name);
	$acc_extra(rtpproxy_setid) = $dlg_val(rtpproxy_set);
	$acc_extra(original_callid) = $hdr(X-CID);
	$acc_extra(uid) = $hdr(X-UID);
	do_accounting("evi","cdr|missed");
	route(update_dialog_vars);
}

route[to_server]
{
#	append_hf("X-INTERFACE: $pr:$Ri:$Rp\r\n");
	xlog("SCRIPT:DBG: sending $rm to servers via $avp(ds_set) $ci\n");

	$avp(cdr_to_ruri) = $ru;

	#if ( $avp(rtpproxy_enabled)>0 )
		route(insert_rtpproxy);

	if ($avp(asterisk_picked) == NULL)
		$avp(asterisk_picked) = 0;

	# TODO - change this ?
	$avp(dlg_correlation) = $avp(cdr_from_ruri);

	if ($avp(ds_set) == 1) {
		# going to Asterisk, check ongoing calls
		# if we need to pick a specific one

		xlog("XXX - looking if another call for $avp(dlg_correlation) exists - current dlg is $DLG_ctx_json $ci \n");
		if ($avp(app_call) != NULL && $avp(app_call) == 1) {
			$avp(dlg_correlation) = $avp(cdr_to_ruri);
			$avp(dlg_correlation_val) = "cdr_to_ruri";
		} else {
			$avp(dlg_correlation) = $avp(cdr_from_ruri);
			$avp(dlg_correlation_val) = "cdr_from_ruri";
		}

		xlog("XXX - looking if another call with $avp(dlg_correlation_val) -> $avp(dlg_correlation) exists ; $avp(cdr_to_ruri) - current dlg is $DLG_ctx_json $ci ; \n");

		if ( get_dialog_info("asterisk_box",$avp(asterisk_box),$avp(dlg_correlation_val),$avp(dlg_correlation),$avp(dlgs_no)) ) {
			$avp(asterisk_picked) = 1;
			$du = $avp(asterisk_box);

			xlog("XXX - $avp(dlgs_no) calls for $avp(dlg_correlation) exists on box $avp(asterisk_box) - sending $ci there \n");
		}

		if ( get_dialogs_by_profile("callee",$rU,$avp(dlg_jsons),$avp(dlg_no)) ) {
			xlog("XXXX - matched $avp(dlg_no) correlated calls with $rU as destination \n");
			$var(i) = 0;
			while ( $(avp(dlg_jsons)[$var(i)])!=NULL ) {
				xlog("XXXX - dlg raw info $(avp(dlg_jsons)[$var(i)]) \n");
				$json(dlg_info) := $(avp(dlg_jsons)[$var(i)]); 
				# fetch any info for the above call and process it
				xlog("XXXX - correlated dlg : $json(dlg_info) - $json(dlg_info/values/UID)  \n");
				if ($avp(app_call) == 1 && $json(dlg_info/values/UID) != NULL) {
					append_hf("X-APP-UID-$var(i)-$json(dlg_info/values/socket_name): $json(dlg_info/values/UID)\r\n");
      					if ($DLG_status!=NULL ) {
						store_dlg_value("X-APP-UID-$var(i)-$json(dlg_info/values/socket_name)","$json(dlg_info/values/UID)");
					}
				}
				$var(i) = $var(i) + 1;
			}

			$var(stop) = $var(i);
		} else
			$var(stop) = 0;

		if ( get_dialogs_by_profile("caller",$rU,$avp(dlg_jsons),$avp(dlg_no)) ) {
			xlog("XXXX - matched $avp(dlg_no) correlated calls with $rU as source \n");
			$var(i) = 0;
			while ( $(avp(dlg_jsons)[$var(i)])!=NULL ) {
				xlog("XXXX - dlg raw info $(avp(dlg_jsons)[$var(i)]) \n");
				$json(dlg_info) := $(avp(dlg_jsons)[$var(i)]); 
				# fetch any info for the above call and process it
				xlog("XXXX - correlated dlg : $json(dlg_info) - $json(dlg_info/values/UID)  \n");
				if ($avp(app_call) == 1 && $json(dlg_info/values/UID) != NULL) {
					$var(idx) = $var(stop) + $var(i);
					append_hf("X-APP-UID-$var(idx)-$json(dlg_info/values/socket_name): $json(dlg_info/values/UID)\r\n");

      					if ($DLG_status!=NULL ) {
						store_dlg_value("X-APP-UID-$var(idx)-$json(dlg_info/values/socket_name)","$json(dlg_info/values/UID)");
					}
				}
				$var(i) = $var(i) + 1;
			}
		}
	}

	if ($avp(asterisk_picked) == 1) {
		if (ds_get_script_attrs($avp(ds_set),$du,$avp(fetched_attrs))) {
			xlog("YYYY - fetched attrs $avp(fetched_attrs) for $du  \n");
		}	

		$json(picked_json) := $avp(fetched_attrs);

		$var(sess_count) = $(json(picked_json/sess_count){s.int});	
		$var(sess_max) = $(json(picked_json/sess_max){s.int});	

		if ($var(sess_count) >= $var(sess_max)) {
			xlog("Target FS $du is already at limit of $var(sess_max) \n");
			$avp(asterisk_picked) = 0;
			$du = NULL;
		}
	}

	if ($avp(asterisk_picked) == 0) {

		xlog("YYYY - before we had $avp(ds_script_attrs) \n");
		$avp(ds_set) = $(avp(ds_set){s.int});

		# we do dispatcher for all methods
		if (!ds_select_dst($avp(ds_set),$avp(ds_alg), "f")) {
			xlog("XXX -failed to route through $avp(ds_set) \n");
			send_reply(503,"Route Unavailable");
			exit;
		}

		$var(ds_idx) = 0;
		while ($(avp(ds_script_attrs)[$var(ds_idx)]) != NULL) {
			$json(ds_attr) := $(avp(ds_attrs)[$var(ds_idx)]);
			$json(ds_info) := $(avp(ds_script_attrs)[$var(ds_idx)]);

			$avp(ds_idle) = $json(ds_info/idle_cpu);
			$avp(ds_ping) = $json(ds_info/ping);

			$avp(max_ping) = $json(ds_attr/max_ping);
			$avp(min_idle) = $json(ds_attr/min_idle);

			$var(update) = 0;

			if (math_compare($avp(ds_ping),$avp(max_ping),$avp(ds_comp_ret))) {
				if ($avp(ds_comp_ret) > 0) {
					xlog("XXXX - $(avp(ds_dst_failover)[$var(ds_idx)]) has ping $avp(ds_ping) over $avp(max_ping) \n");
					avp_delete("$(avp(ds_script_attrs)[$var(ds_idx)])");
					avp_delete("$(avp(ds_attrs)[$var(ds_idx)])");
					avp_delete("$(avp(ds_dst_failover)[$var(ds_idx)])");
					avp_delete("$(avp(ds_sock_failover)[$var(ds_idx)])");

					$var(update) = 1;
				}
			}

			if (math_compare($avp(ds_idle),$avp(min_idle),$avp(ds_comp_ret))) {
				if ($avp(ds_comp_ret) < 0) {
					xlog("XXXX - $(avp(ds_dst_failover)[$var(ds_idx)]) has idle $avp(ds_idle) under min $avp(min_idle) \n");
					avp_delete("$(avp(ds_script_attrs)[$var(ds_idx)])");
					avp_delete("$(avp(ds_attrs)[$var(ds_idx)])");
					avp_delete("$(avp(ds_dst_failover)[$var(ds_idx)])");
					avp_delete("$(avp(ds_sock_failover)[$var(ds_idx)])");

					$var(update) = 1;
				}
			}

			$var(ds_idx) = $var(ds_idx) + 1 - $var(update);
		}

		if ($avp(ds_dst_failover) == NULL) {
			xlog("XXX -failed to route through $avp(ds_set) - all destinations failed filter conditions \n");
			send_reply(503,"Route Unavailable");
			exit;
		}

		set_count($avp(ds_dst_failover), $avp(total_dests));
		if ($avp(total_dests) == 1) {
			xlog("XXXX - got a single destination $avp(ds_dst_failover) , routing to it \n");	
			$du = $avp(ds_dst_failover);	
		} else {
			$var(i) = 0;
			$var(i_stop_loop) = $avp(total_dests) -1;

			xlog("XXX - before avps are $(avp(ds_attrs)[*]) and $(avp(ds_script_attrs)[*]) and $(avp(ds_dst_failover)[*]) \n");

			while ($var(i) < $var(i_stop_loop)) {
				xlog("XXX - we at i $var(i) with stop $var(i_stop_loop) \n");

				$var(j) = 0;
				$var(j_stop_loop) = $avp(total_dests) - $var(i) - 1;
				while ($var(j) < $var(j_stop_loop)) {
					xlog("XXX - we at j $var(j) with stop $var(j_stop_loop) \n");

					$var(next) = $var(j) + 1;
					$json(j_ds_info) := $(avp(ds_script_attrs)[$var(j)]);
					$json(j1_ds_info) := $(avp(ds_script_attrs)[$var(next)]);

					$json(j_ds_att) := $(avp(ds_attrs)[$var(j)]);
					$json(j1_ds_att) := $(avp(ds_attrs)[$var(next)]);
					
					if (math_compare("$json(j_ds_info/idle_cpu) * $json(j_ds_att/load_factor)","$json(j1_ds_info/idle_cpu) * $json(j1_ds_att/load_factor)",$avp(ds_comp_ret))) {
						if ($avp(ds_comp_ret) < 0) {
							# uh oh, gotta swap
							xlog("XXX - gotta swap $var(j) with $var(next) \n");

							$var(swap_aux) = $(avp(ds_script_attrs)[$var(j)]);
							$(avp(ds_script_attrs)[$var(j)]) = $(avp(ds_script_attrs)[$var(next)]);
							$(avp(ds_script_attrs)[$var(next)]) = $var(swap_aux);

							$var(swap_aux) = $(avp(ds_attrs)[$var(j)]);
							$(avp(ds_attrs)[$var(j)]) = $(avp(ds_attrs)[$var(next)]);
							$(avp(ds_attrs)[$var(next)]) = $var(swap_aux);

							$var(swap_aux) = $(avp(ds_dst_failover)[$var(j)]);
							$(avp(ds_dst_failover)[$var(j)]) = $(avp(ds_dst_failover)[$var(next)]);
							$(avp(ds_dst_failover)[$var(next)]) = $var(swap_aux);

							$var(swap_aux) = $(avp(ds_sock_failover)[$var(j)]);
							$(avp(ds_sock_failover)[$var(j)]) = $(avp(ds_sock_failover)[$var(next)]);
							$(avp(ds_sock_failover)[$var(next)]) = $var(swap_aux);
						}
					}

					$var(j) = $var(j) +1;	
				}
				$var(i) = $var(i) + 1;
			}

			xlog("XXX - after avps are $(avp(ds_attrs)[*]) and $(avp(ds_script_attrs)[*]) and $(avp(ds_dst_failover)[*]) \n");
		
			xlog("XXX - least loaded is $avp(ds_dst_failover) \n");

			$du = $avp(ds_dst_failover);
		}

		$avp(ds_cnt_failover) := $avp(total_dests);

		$dlg_val(freeswitch_box) = $du;
		$dlg_val(dlg_correlation) = $avp(dlg_correlation);

                $dlg_val(cdr_from_ruri) = $avp(cdr_from_ruri);
                $dlg_val(cdr_to_ruri) = $avp(cdr_to_ruri);

	}

	t_on_failure("ds_failure");

	# dst URI points to the new destination
	xlog("SCRIPT:DBG: attempting server $ru $du $ci\n");


	$avp(cdr_domain) = $rd;

	route(do_accounting);

	route(relay);
	exit;
}


route[relay] {
	xlog("SCRIPT:DBG: sending $rm :out to $ru/$du $ci - $fs \n");
	if (!t_relay()) {
		route (remove_call_vars,$ci,$dlg_val(device));
		send_reply(500,"Internal Error");
	};
	exit;
}

route[push_server_load] {
	$json(attrs) := "{}";

	if ($avp(micro_ping) != NULL)
		$json(attrs/ping) = $avp(micro_ping);
	if ($avp(sess_max) != NULL)
		$json(attrs/sess_max)= $avp(sess_max);
	if ($avp(sess_count) != NULL)
		$json(attrs/sess_count)= $avp(sess_count);
	if ($avp(idle_cpu) != NULL)
		$json(attrs/idle_cpu)= $avp(idle_cpu);
	
	$var(rc) = ds_push_script_attrs($json(attrs),$avp(ip),$avp(port));

	if ($var(rc) < 0) {
		xlog("XXXX - failed to update attrs for $si : $sp \n"); 
	} else {
		xlog("XXX - pushed $json(attrs) for $avp(ip) : $avp(port) \n");
	}

	$avp(ret) = "{\"success\:\"" + $var(rc) + "\"}";
}


onreply_route[local_options_reply] {

     	get_timestamp($avp(rpl_sec), $avp(rpl_usec));
	ts_usec_delta($avp(rpl_sec),$avp(rpl_usec),$avp(sec),$avp(usec),$avp(micro_ping));

	xlog("BBBBB - got reply for ping $ci from $si - ping is $avp(micro_ping) us ; active calls $hdr(X-SESS-COUNT) , max calls $hdr(X-SESS-MAX) , idle CPU $hdr(X-IDLE-CPU) \n");
	if (is_present_hf("X-SESS-COUNT")) {
		# Load enabled FS

		if (is_present_hf("X-SESS-COUNT"))
			$avp(sess_count) = $hdr(X-SESS-COUNT);
		if (is_present_hf("X-SESS-MAX"))
			$avp(sess_max)= $hdr(X-SESS-MAX);
		if (is_present_hf("X-IDLE-CPU"))
			$avp(idle_cpu)= $hdr(X-IDLE-CPU);

		$avp(ip) = $si;
		$avp(port) = $sp;

		route(push_server_load);

	}
}

onreply_route[reply_handling]
{

	xlog("ILYA:: OpenSIPS received a reply from $si with $rs pending event:  $mi \n");
	xlog("ILYA:: HGET event_$si EVENT_$rs");
	cache_raw_query("redis","HGET event_$si EVENT_$rs","$avp(event_data)");
  	xlog("ILYA:: remove_call_vars route after HGET   (avp(event_data){s.decode.hexa}) iS:-->    $(avp(event_data){s.decode.hexa})<-- \n");
	$var(single_event_json)=$(avp(event_data){s.decode.hexa});

	xlog("XXX - here $dlg_val(socket_name) for $ci \n");
			xlog("reply_handling::socket_name: $dlg_val(socket_name) of $ci \n");

	if (isbflagset(DST_NAT)) {
		xlog("XXXX - $rs reply for $ci nated \n");
	} else {
		xlog("XXXX - $rs reply for $ci not nated \n");
	}
#	if (isbflagset(DST_NAT)) {
		# fix the private contacts (with the public IP of the NAT)
		# for all replies coming from behind a NAT
		if (!cluster_check_addr(1,$si,"sip"))
			fix_nated_contact();
			xlog("reply_handling:: fix_nated_contact of $rU \n");
#	}

	if ((isbflagset(DST_NAT) || is_dlg_flag_set(1)) && $avp(rtpproxy_enabled) > 0) {
		if ( has_body("application/sdp") ) {
		xlog("reply_handling:: has_body rtpproxy_answer is done on  $dlg_val(socket_name) $ci  \n");

		rtpengine_use_set($avp(rtpproxy_set));
		$avp(answer_flags) = "address-family=IP4 ";


                if ($avp(rtpengine_mode) == "ie") {
                        $avp(answer_flags) = $avp(answer_flags) + "in-iface=external out-iface=internal symmetric";
                } else if ($avp(rtpengine_mode) == "ii") {
                        $avp(answer_flags) = $avp(answer_flags) + "in-iface=internal out-iface=internal symmetric";
                } else if ($avp(rtpengine_mode) == "ei") {
                        $avp(answer_flags) = $avp(answer_flags) + "in-iface=internal out-iface=external symmetric";
                } else {
                        $avp(answer_flags) = $avp(answer_flags) + "in-iface=external out-iface=external symmetric";
                }
                if (!nat_uac_test(8))
                        $avp(answer_flags) = $avp(answer_flags) + " trust-address";

		if (isflagset(SRC_WS) && isbflagset(DST_WS))
			$avp(answer_flags) = $avp(answer_flags) + " ICE=force-relay DTLS=passive SDES-off replace-origin";
		else if (isflagset(SRC_WS) && !isbflagset(DST_WS))
			$avp(answer_flags) = $avp(answer_flags) + " UDP/TLS/RTP/SAVPF ICE=force SDES-off replace-origin";
		else if (!isflagset(SRC_WS) && isbflagset(DST_WS))
			$avp(answer_flags) = $avp(answer_flags) + " RTP/AVP replace-session-connection replace-origin ICE=remove SDES-off";
		else if (!isflagset(SRC_WS) && !isbflagset(DST_WS))
			$avp(answer_flags) = $avp(answer_flags) + " RTP/AVP replace-session-connection replace-origin ICE=remove SDES-off";
		rtpengine_answer("$avp(answer_flags)");


#		$avp(rtpproxy_set) = $(dlg_val(rtpproxy_set){s.int});
#
#                if ($si =~ "192.168") {
#                        if ($avp(in2in) == 1) {
#                                if (nat_uac_test(8))
#                                        rtpproxy_answer("coiis",,$avp(rtpproxy_set));
#                                else
#                                        rtpproxy_answer("coiirs",,$avp(rtpproxy_set));
#                        } else {
#                                if (nat_uac_test(8))
#                                        rtpproxy_answer("coies",,$avp(rtpproxy_set));
#                                else
#                                        rtpproxy_answer("coiers",,$avp(rtpproxy_set));
#                        }
#                        xlog("XXX - answer from internal forced on grp $avp(rtpproxy_set) \n");
#                } else {
#                        if (nat_uac_test(8))
#                                rtpproxy_answer("coeis",,$avp(rtpproxy_set));
#                        else
#                                rtpproxy_answer("coeirs",,$avp(rtpproxy_set));
#                        xlog("XXX - answer from external forced on grp $avp(rtpproxy_set) \n");
#                }


	
		}
	}
	if ($dlg_val(socket_name) = "EXTERNAL" ) {
        	 #force_send_socket(udp:ANYCAST_IP:5060);
        	 force_send_socket(udp:MY_IP:5060);

     }

	if (is_present_hf("X-UUID")) {
		$dlg_val(UID) = $hdr(X-UUID);
	}

}

failure_route[ds_failure] {
	if (t_was_cancelled()) {
		t_reply(487,"Request cancelled");
		exit;
	}

	/* server failure ? */
	if ( t_check_status("56[0-9][0-9]") ||
	(t_check_status("408") && t_local_replied("all")) ) {
		xlog("SCRIPT:DS:DBG: destination $avp(ds_dst_failover) failed, disabling\n");
		ds_mark_dst("p");

		if ($avp(asterisk_picked) == 1) {
			if (!ds_select_dst($avp(ds_set),$avp(ds_alg), "f")) {
				send_reply(503,"Route Unavailable");
				exit;
			}

			$dlg_val(asterisk_box) = $du;
			t_on_failure("ds_failure");
			$avp(cdr_domain) = $rd;

			route(relay);
			exit;
		} else {
			if ( ds_next_dst() ) {
				xlog("SCRIPT:DS:DBG: new destination $du selected\n");
				$dlg_val(asterisk_box) = $du;
				$avp(cdr_domain) = $rd;
				t_on_failure("ds_failure");
				t_relay();
			} else {
				xlog("SCRIPT:DS:DBG: no other Asterisk servers available\n");
				t_reply(404,"No Server found");
			}
		}
	}
}

## takes as param a SIP URI A and SIP URI B
##    URI A -> accountid + URI B -> speeddial
route[do_revert_speeddial_lookup]
{
	xlog("SCRIPT:DBG: doing revert speeddial lookup for uri B=$param(2) from URI A=$param(1) perspective\n");
	$avp(accountid) := NULL;
	$avp(sd_user) := NULL;

	$var(orig_ru) = $ru;
	$ru = $param(1);

	# KKVLAD - lookup active subscriber for param1
	if (!lookup("location","m")) {
		# FIXME - is this ok here, or do we still need the subscriber query
		xlog("SCRIPT:DBG: $param(1) is not an active subscriber\n");
		$ru = $var(orig_ru);
		return (-1);
	}

	$json(subs_attrs) := $avp(subs_attrs);
	$avp(accountid) = $json(subs_attrs/accountid);
	
	# cleanup lookup branches
	$var(i) = 0;
	while ($(branch(uri)[$var(i)]) != NULL) {
		remove_branch($var(i));
	}

	$var(orig_du) = $du;

	$du = NULL;
	$ru = $var(orig_ru);

	xlog("XXXV - fetched account $avp(accountid) \n");

	$ru = $param(2);
	if (!lookup("location","m")) {
		# FIXME - is this ok here, or do we still need the subscriber query
		xlog("SCRIPT:DBG: $param(2) is not an active subscriber\n");
		$ru = $var(orig_ru);
		return (-1);
	}

	$json(subs_attrs) := $avp(subs_attrs);
	$avp(sd_user) = $json(subs_attrs/extensions[0]);
	
	# cleanup lookup branches
	$var(i) = 0;
	while ($(branch(uri)[$var(i)]) != NULL) {
		remove_branch($var(i));
	}

	$du = $var(orig_du);
	$ru = $var(orig_ru);
	


#	avp_db_query("select accountid from subscriber where username='$(param(1){uri.user})' and domain='$(param(1){uri.host})'", "$avp(accountid)");
#	if ($retcode<0 ||  $avp(accountidt)==NULL) {
#		xlog("SCRIPT:DBG: $param(1) is not a subscriber\n");
#		return(-1);
#	}
#
#	xlog("SCRIPT:DBG: checking speeddial for $param(2) with accountid $avp(accountid)\n");
#	avp_db_query("select speeddial from speeddial where accountid=$avp(accountid) and target='$(param(2){uri.user})'", "$avp(sd_user)");
#	if ($retcode<0 ||  $avp(sd_user)==NULL) {
#		xlog("SCRIPT:DBG: no record found\n");
#		return(-1);
#	}

	xlog("SCRIPT:DBG: found SD <$avp(sd_user)> for $param(2) / $avp(accountid)\n");

	return(1);
}




local_route {

	## NOTIFY request for BLF ?
	if (is_method("NOTIFY") && $hdr(Event)=="dialog" ) {

		xlog("XXXX - generated notify event for callid $ci going to $ru - $du - $mb \n");

		$xml(body) = $rb;
		$var(sd_user) = $fU;

		## extract, convert and fix the entity from "dialog-info" (presentity)
		$var(uri) = $xml(body/dialog-info.attr/entity);
		xlog("SCRIPT:NOTIFY:DBG: entity uri is $var(uri)\n");
		if ( $var(sd_user)!=$(var(uri){uri.user}) ) {
			# compute the new SIP URI (with the sd)
			$var(uri) = "sip:"+$var(sd_user)+"@"+$fd;
			# do the replacement in the body
			xlog("SCRIPT:NOTIFY:DBG: replacing entity uri with $var(uri)\n");
			$xml(body/dialog-info.attr/entity) = $var(uri);
			setflag(REBUILD);
		}

		if ( $xml(body/dialog-info/dialog.attr/call-id)!=NULL ) {

			## extract "direction"
			$var(direction) = $xml(body/dialog-info/dialog.attr/direction);
			xlog("SCRIPT:NOTIFY:DBG: direction is $var(direction)\n");

			## extract the identity in <local>
			$var(local_uri) = $xml(body/dialog-info/dialog/local/identity.val);
			xlog("SCRIPT:NOTIFY:DBG: local identity is $var(local_uri)\n");

			## extract the identity in <remote>
			$var(remote_uri) = $xml(body/dialog-info/dialog/remote/identity.val);
			xlog("SCRIPT:NOTIFY:DBG: remote identity is $var(remote_uri)\n");

			if ($var(direction)=="recipient") {

				# dialog identity is in local (local->SD)
				# if extracted value is the same with new value, avoid the change
				##if ( $var(sd_user)==$(var(local_uri){uri.user}) ) {
				##	$var(local_uri)="";
				##} else {
				##	$var(local_uri) = "sip:"+$var(sd_user)+"@"+$(var(local_uri){uri.host});
					$var(local_uri) = "sip:"+$var(sd_user)+"@"+$fd;
				##}
				# try to do revers lookup on the other call leg, we might be able to translate to a SD
				if ( route(do_revert_speeddial_lookup, $tu, $var(remote_uri)) ) {
					# $avp(sd_user) holds the SD username, so compute the new SIP URI (with the sd)
					$var(remote_uri) = "sip:"+$avp(sd_user)+"@"+$fd;
				} else {
					$var(remote_uri) = "sip:"+$(var(remote_uri){uri.user})+"@"+$fd;
				}

			} else {

				# dialog identity is in remote (remote->SD)
				# if extracted value is the same with new value, avoid the change
				##if ( $var(sd_user)==$(var(remote_uri){uri.user}) ) {
				##	$var(remote_uri)="";
				##} else {
				##	$var(remote_uri) = "sip:"+$var(sd_user)+"@"+$(var(remote_uri){uri.host});
					$var(remote_uri) = "sip:"+$var(sd_user)+"@"+$fd;
				##}
				# try to do revers lookup on the other call leg, we might be able to translate to a SD
				if ( route(do_revert_speeddial_lookup, $tu, $var(locak_uri)) ) {
					# $avp(sd_user) holds the SD username, so compute the new SIP URI (with the sd)
					$var(local_uri) = "sip:"+$avp(sd_user)+"@"+$fd;
				} else {
					$var(local_uri) = "sip:"+$(var(local_uri){uri.user})+"@"+$fd;
				}

			}

			## apply changes over the body

			if ($var(local_uri)!="") {
				# do the replacement in the body
				xlog("SCRIPT:NOTIFY:DBG: replacing local identity with $var(local_uri)\n");
				$xml(body/dialog-info/dialog/local/identity.val) = $var(local_uri);
				$xml(body/dialog-info/dialog/local/target.attr/uri) = $var(local_uri);
				setflag(REBUILD);
			}

			if ($var(remote_uri)!="") {
				# do the replacement in the body
				xlog("SCRIPT:NOTIFY:DBG: replacing remote identity with $var(remote_uri)\n");
				$xml(body/dialog-info/dialog/remote/identity.val) = $var(remote_uri);
				$xml(body/dialog-info/dialog/remote/target.attr/uri) = $var(remote_uri);
				setflag(REBUILD);
			}
		}

		if (isflagset(REBUILD)) {
			remove_body_part();
			add_body_part("$xml(body)","application/dialog-info+xml");
		}

	}



	#########################################################################
	################ OPTIONS Dealy Magering  Start ##########################
	#########################################################################

   if(is_method("OPTIONS")) {
     	get_timestamp($avp(sec), $avp(usec));
        append_hf("P-hint: foreign request\r\n");

	t_on_reply("local_options_reply");

   }



	#########################################################################
	################ OPTIONS Dealy Magering  end  ##########################
	#########################################################################
}

###########################################################################

#event_route[E_UL_LATENCY_UPDATE] {
#	route(CONTACT_HANDLE, "device_latency_update");
#}

event_route[E_UL_CONTACT_INSERT] {
	xlog("XXX - insert \n");
    $var(latency) = NULL;
    $var(latency) = $param(latency);
    $var(expires) = $param(expires);
    $var(received) = $param(received);
    $var(subs_attrs) = $param(attr);

    xlog("XXXX - fetched attrs $var(subs_attrs) \n");

    if ($var(received) == NULL || $var(received) == "") {
	$var(received) = $param(uri);
    }
	$var(address) = $param(uri);
	$var(aor) = $param(aor);

	route(CONTACT_HANDLE, "device_insert");
}
event_route[E_UL_CONTACT_UPDATE] {
	xlog("XXX - update \n");
    $var(latency) = NULL;
    $var(latency) = $param(latency);
    $var(expires) = $param(expires);
    $var(received) = $param(received);
    $var(subs_attrs) = $param(attr);

    xlog("XXXX - fetched attrs $var(subs_attrs) \n");

    if ($var(received) == NULL || $var(received) == "") {
	$var(received) = $param(uri);
    }
	$var(address) = $param(uri);
	$var(aor) = $param(aor);

	route(CONTACT_HANDLE, "device_update");
}

event_route[E_UL_CONTACT_DELETE] {
    $var(latency) = NULL;
    $var(latency) = $param(latency);
    $var(expires) = $param(expires);
    $var(received) = $param(received);
    $var(subs_attrs) = $param(attr);

    xlog("XXXX - fetched attrs $var(subs_attrs) \n");

    if ($var(received) == NULL || $var(received) == "") {
	$var(received) = $param(uri);
    }
	$var(address) = $param(uri);
	$var(aor) = $param(aor);

	route(CONTACT_HANDLE, "device_delete");
}

route[CONTACT_HANDLE] {
	$var(uri_host) = $(var(received){uri.host});
	$var(uri_port) = $(var(received){uri.port});
	if ($var(uri_port) == NULL || $var(uri_port) == "")
		$var(uri_port) = 5060;

	if($var(latency)==NULL){
		$var(latency)="-1";
	}
        if ($var(expires) == NULL) {
                $var(expires)="-1";
        }

    xlog("E_UL_CONTACT event $param(1) : $var(address) has $var(latency) microsec latency with AOR $var(aor) from host $var(uri_host) : $var(uri_port) \n");
    route(publish_rabbit_device_event, $var(address), $var(latency), $param(1),$var(expires),$var(uri_host),$var(uri_port) );
}


########################################################################





route[update_dialog_vars_tests]{

#	xlog(" update_dialog_vars /n");
	$var(hdr-name) ="from";
	xlog("\n\n\n\n\n\n\n\n\n\n\n\n");
	xlog("Heder Var test -hdr(from)- 1-->$hdr(from))<---\n");
	xlog("Heder Var test -hdr(var(hdr-name))- 1-->$hdr($var(hdr-name))<---\n");
	$var(x)= $hdr($var(hdr-name));
	xlog(" 2 -->$var(x)<---\n");
	xlog(" 3 -->$hdr(var(hdr-name))<--- \n");
	xlog("\n\n\n\n\n\n\n\n\n\n\n\n");
	cache_fetch("local","dialog_vars_name",$var(dialog_vars_name));
	cache_fetch("local","dialog_vars_value",$var(dialog_vars_value));
	cache_fetch("local","dialog_vars_header_name",$var(dialog_vars_header_name));
    $var(i_var) = 0;
    #  while($var(i_var) < 10){
 	while ( $(var(dialog_vars_name){s.select,$var(i_var),;}) != "") {
		$var(dialog_var_name)=$(var(dialog_vars_name){s.select,$var(i_var),;});
		$var(dialog_var_value)=$(var(dialog_vars_name){s.select,$var(i_var),;});
		$var(dialog_var_header_name)=$(var(dialog_vars_name){s.select,$var(i_var),;});
		xlog ("-------------------dialog_vars_name--------------:$(var(dialog_vars_name){s.select,$var(i_var),;})\n");
		xlog ("-------------------diaglog_vars_value--------------:$(var(dialog_vars_value){s.select,$var(i_var),;})\n");
		xlog ("-------------------diaglog_vars_header_name--------------:$(var(dialog_vars_header_name){s.select,$var(i_var),;})\n");
            ################################### headers logic ############################
		xlog ("-------------------------------------------------------------------------------------------\n");
 		xlog ("-------------------------------------------------------------------------------------------\n");
    	xlog ("-------------------------------------------------------------------------------------------\n");
 		xlog ("-------------------------------------------------------------------------------------------\n");
		#  xlog ("------------------------$--(hdr(name)[N])-----------------------$(hdr(name)[N])--------------------\n");

		#If ($(hdr($var(dialog_var_name))[1]) != "") {
		#	$var(dialog_var_name)="";
		#}

        $var(i_var) = $var(i_var) + 1;
	}
}


route[update_dialog_vars] {
	if($avp(update_dialog_vars)==1)
		return;

	if ($DLG_status == NULL) {
		if (has_totag()) {
			xlog("DLG ---  SEQUENTIAL WITH OUT DIALOG INFO Huston we have a problem .....$ci\n");
			return;
		} else if (is_method("INVITE")) {
			if ( !create_dialog("B") ) {
				xlog("DLG ---  CALL WITH OUT DIALOG INFO Huston we have a problem .....$ci\n");
				return;
			}
			$dlg_val(create_ts) = $Ts;
			$dlg_val(callerid) = $fU;
			if ($fn != NULL && $fn != "") {
				if ($fn =~ "^\"")		
					$var(fn) = $(fn{s.select,1,"});
				else
					$var(fn) = $fn;
				$dlg_val(calleridname) = $(var(fn){s.escape.common});
			} else
				$dlg_val(calleridname) = $fU;
			if ($ai != NULL && $ai != "")
				$dlg_val(pai) = $ai;
			if ($re != NULL && $re != "")
				$dlg_val(rpid) = $re;
			if (!has_totag())
				set_dlg_sharing_tag("MY_CLUSTER_NODE_ID");
		}
	}

	$avp(update_dialog_vars)=1;

	if ($dlg_val(accountid) == NULL) {
		if ($avp(accountid) == NULL || $avp(accountid) == 0) {
                	if (is_present_hf("X-CUSTOMER"))
                        	$dlg_val(accountid) = $hdr(X-CUSTOMER);
        	} else
                	$dlg_val(accountid) = $avp(accountid);
	}

	if ($dlg_val(guid) == NULL) {
	        if ($avp(os_uuid) == NULL && is_present_hf("X-GUID"))
	        	$dlg_val(guid) = $hdr(X-GUID);
        	else
                	$dlg_val(guid) = $avp(os_uuid);
	}

	if ($dlg_val(device) == NULL){
    		if ($var(device) != NULL ){
    			$dlg_val(device) = $var(device) ;
			if (!is_in_profile("device"))
				set_dlg_profile("device","$dlg_val(device)");
			$avp(device) = $var(device) ;
    		}
	}

	if(is_audio_on_hold()){
		xlog("SCRIPT:DBG: is_audio_on_hold  $rm sent to $ru/$du\n");
		$dlg_val(IsOnHold) = "True";
	}else{
		$dlg_val(IsOnHold) = "False";
	}

	if ($dlg_val(UID) == NULL && $hdr(X-UID) != NULL) {
		$dlg_val(UID) = $hdr(X-UID);
	}
	
	if ($dlg_val(LinkedCallid) == NULL && $hdr(X-CID) != NULL) {
		$dlg_val(LinkedCallid) = $hdr(X-CID);
	}
}


route [update_dialog_IVRUniqueID] {


    $var(ParamHeaderValue) = $param(1);
    $var(ParamDefaultValue) = $param(2);


    if ($var(ParamHeaderValue) != "" ) {
            $json(call_json/IVRUniqueID) =$var(ParamHeaderValue);
            cache_raw_query("redis","HSET var_$ci IVRUniqueID $var(ParamHeaderValue)");
        } else {
                if  ($json(call_json/IVRUniqueID) != "")  {
                    store_dlg_value("IVRUniqueID", "$json(call_json/IVRUniqueID)" );
                } else {
                    if ($var(ParamDefaultValue) != "")   {
                        cache_raw_query("redis","HSET var_$ci IVRUniqueID $var(ParamDefaultValue)");
                        store_dlg_value("IVRUniqueID","$var(ParamDefaultValue)" );
                        $json(call_json/IVRUniqueID) = $var(ParamDefaultValue);
                    }
                }
        }
}

route[update_dialog_single_var] {
#route(update_dialog_single_var,ParamName,ParamHeaderValue,ParamDefaultValue)
    $var(ParamName) = $param(1);
    $var(ParamHeaderValue) = $param(2);
    $var(ParamDefaultValue) = $param(3);
     xlog ("\n\n  ----------- var(ParamName): $var(ParamName) -------------------------\n");
     xlog ("\n\n  ----------- var(ParamHeaderValue): $var(ParamHeaderValue) -------------------------\n");
     xlog ("\n\n  ----------- var(ParamDefaultValue): $var(ParamDefaultValue) -------------------------\n");
return(1);
}

event_route[E_DLG_STATE_CHANGED]{
	xlog("\n\n\n  E_DLG_STATE_CHANGED Fire !!!!!!!!!!!!!!!!!!!!!!!!!!");

	route(pstn_rabbitmq_event);
}


route [load_device_calls] {
    $var(DeviceToLoad) = $param(1);
 #   script_trace ( 1, "Load calls for $var(DeviceToLoad)" , "Voicenter" );


	$var(json_body_calls_data) = "[";

	if ( get_dialogs_by_val("device",$var(DeviceToLoad),$avp(dlg_jsons),$avp(dlg_no)) ) {
		xlog("XXXX - matched $avp(dlg_no) correlated calls for device $var(DeviceToLoad) \n");
		$var(i) = 0;
		while ( $(avp(dlg_jsons)[$var(i)])!=NULL ) {
			xlog("XXXX - dlg raw info $(avp(dlg_jsons)[$var(i)]) \n");
			$json(dlg_info) := $(avp(dlg_jsons)[$var(i)]); 
			# fetch any info for the above call and process it
			xlog("XXXX - correlated dlg : $json(dlg_info) \n");
			xlog("XXX asterisk box is $json(dlg_info/values/asterisk_box) \n"); 

			if ($var(i) != 0)
				$var(json_body_calls_data) = $var(json_body_calls_data) + ",";
			$var(json_body_calls_data) = $var(json_body_calls_data) + $(avp(dlg_jsons)[$var(i)]);

			$var(i) = $var(i) + 1;


		}
	}

	if ($DLG_ctx_json != NULL) {
		xlog("XXXXXX - yo yo - dialog ctx is good $DLG_ctx_json \n");
		$var(json_body_calls_data) = $var(json_body_calls_data) + ","+$DLG_ctx_json+"]";
	} else {
		$var(json_body_calls_data) = $var(json_body_calls_data) + "]";
		xlog("XXXXXX - uh oh - dialog ctx is null :( - we got $var(json_body_calls_data) so far \n");
	}


	xlog("ZZZZZ - rabbit info is $var(json_body_calls_data) \n");
	$var(json_body_calls) = $var(json_body_calls_data);

##script_trace();

}


route [publish_rabbit_device_event] {
    $var(address) = $param(1);
    $var(latency) = $param(2);
    $var(event_type) = $param(3);
    $var(expires) =  $param(4);
    $var(host) =  $param(5);
    $var(port) =  $param(6);
    $var(DeviceToLoad) = $(var(address){uri.user});
    $avp(DeviceToLoad) = $var(DeviceToLoad);
    $avp(hdr_name) = "EventType";
    $avp(hdr_value) = $var(event_type) ;
    $var(json_device_event) = "{";
    cache_raw_query("redis","GET agent_$var(DeviceToLoad)", "$avp(uagent)");

	# Extension Data
        $var(json_device_event) = "{ \"Profile\": \"" + $var(DeviceToLoad) + "\"";
	$var(json_device_event) = $var(json_device_event) + ",\"EventType\":\"" + $var(event_type)+"\"";
        $var(json_device_event) = $var(json_device_event) + ",\"Address\":\"" + $var(address)+"\"";
    $var(json_device_event) = $var(json_device_event) + ",\"Latency\":" + $var(latency);
    $var(json_device_event) = $var(json_device_event) + ",\"Expires\":" + $var(expires);
    $var(json_device_event) = $var(json_device_event) + ",\"Timestamp\":" + $Ts;
        $var(json_device_event) = $var(json_device_event) + ",\"SipUsername\":\"" + $var(DeviceToLoad)+"\"";
        $var(json_device_event) = $var(json_device_event) + ",\"UserAgent\":\"" + $(avp(uagent){s.select,2,|})+"\"";
        $var(json_device_event) = $var(json_device_event) + ",\"Accountid\":\"" + $(avp(uagent){s.select,0,|})+"\"";
        $var(json_device_event) = $var(json_device_event) + ",\"ReceivedHost\":\"" + $var(host)+"\"";
        $var(json_device_event) = $var(json_device_event) + ",\"ReceivedPort\":" + $var(port);
        $var(json_device_event) = $var(json_device_event) + ",\"Opensips_node_id\": MY_CLUSTER_NODE_ID";
        $var(json_device_event) = $var(json_device_event) + ",\"AOR\":\"" + $var(aor)+"\"";

	# Call List
	route (load_device_calls, $var(DeviceToLoad)  );
    $var(json_device_event) = $var(json_device_event) + ",\"CallList\":" + $var(json_body_calls);
	$var(json_device_event) = $var(json_device_event) + "}";

	$json(subs_attrs) := $var(subs_attrs);
	xlog("XXXX - attrs here = $json(subs_attrs) \n");

	if ($json(subs_attrs/attrs/rmq_headers) != NULL && $json(subs_attrs/attrs/rmq_headers) != "") {
		$json(rmq_headers) := $json(subs_attrs/attrs/rmq_headers);

		for ($var(k) in $(json(rmq_headers.keys)[*])) {
			$avp(hdr_name) = $var(k);
			$avp(hdr_value) = $json(rmq_headers/$var(k));
		}

		xlog("XXXX - added extra rmq headers $(avp(hdr_name)[*]) : $(avp(hdr_value)[*]) \n");

	}

	# all good: send the publish to rabbitmq
	xlog("SCRIPT:DBG: publishing rabbitmq: $var(json_device_event)");
	if (!rabbitmq_publish("rabbitmq_location1", "RMQ_EXCH_LOCATION", $var(json_device_event), "application/json", $avp(hdr_name), $avp(hdr_value))){
		xlog("L_ERR", "cannot publish event to rabbitmq server event=$var(event) user=$var(event) body=[$var(json_device_event)]\n");
		if (!rabbitmq_publish("rabbitmq_location2", "RMQ_EXCH_LOCATION", $var(json_device_event), "application/json", $avp(hdr_name), $avp(hdr_value))){
                	xlog("L_ERR", "cannot publish event to rabbitmq_2 server event=$var(event) user=$var(event) body=[$var(json_device_event)]\n");
        	}
	}
}

route [remove_call_vars] {
	$var(callid) = $param(1);
	$var(device) = $param(2);
   	script_trace ( 1, "call to remove vars for callid-->$var(callid)<-- of device --> $var(device)<-- " , "Voicenter" );
   	xlog("VCLOG HGET calls_$var(device) $var(callid) \n");
   	cache_raw_query("redis","HGET calls_$dlg_val(device) $var(callid)","$avp(remove_call_result)");
  	xlog("remove_call_vars route after HGET   (avp(remove_call_result){s.decode.hexa}) iS:-->    $(avp(remove_call_result){s.decode.hexa})<-- \n");
	$var(single_call_json)=$(avp(remove_call_result){s.decode.hexa});
    xlog("MY111HDEL calls_ var $var(device) $var(callid) - avp $avp(device)  dlg $dlg_val(device) ---  $ci --$rm-- " );
    
	if($var(device) == NULL) {
	$var(device) = $avp(device);
	}
	cache_raw_query("redis","HDEL calls_$var(device) $var(callid) ");
	xlog("DEL var_$var(callid)" );
    cache_raw_query("redis","DEL var_$var(callid)");

	xlog("remove_call_vars route finished   var(single_call_json) iS:-->$var(single_call_json)<-- \n");


	script_trace();
	return(1);
}
route [remove_call_vars_old] {
    $var(callid) = $param(1);
    $var(device) = $param(2);
    script_trace ( 1, "call to get vars for callid-->$var(callid)<-- of device --> $var(device)<-- " , "Voicenter" );
		xlog("HGET calls_$var(device) -->$var(callid) \n");
		cache_raw_query("redis","HGET calls_$dlg_val(device) $var(callid)","$avp(call_result)");
		$var(single_call_json)= $(avp(call_result){s.decode.hexa}) ;
		xlog("HGET calls_$var(device) -->$var(callid)<-- with2 -->$avp(call_result)<-- \n");
	xlog("get_single_call route finished \n");

script_trace();
return(1);
}

route [get_single_call] {
    $var(callid) = $param(1);
    $var(device) = $param(2);
    script_trace ( 1, "call to get vars for callid-->$var(callid)<-- of device --> $var(device)<-- " , "Voicenter" );
	xlog("HGET calls_$var(device) -->$var(callid) \n");
	cache_raw_query("redis","HGET calls_$dlg_val(device) $var(callid)","$avp(call_result)");
	$var(single_call_json)= $(avp(call_result){s.decode.hexa}) ;
	xlog("HGET calls_$var(device) -->$var(callid)<-- with2 -->$avp(call_result)<-- \n");
	xlog("get_single_call route finished \n");
	
	$json(call_json) := $var(single_call_json);
	
	
	# Update call data with data from redis:
	cache_raw_query("redis","HGETALL var_$var(callid)","$avp(result)");
	$var(it) = 0;
	while ($(avp(result)[$var(it)]) != NULL) {
		xlog("ILYA:: HHHH $(avp(result)[$var(it)])   $(avp(result)[$var(it2)]) ");
		$var(it2) = $var(it)+1; 
		if (   $(avp(result)[$var(it)]) !=  ""  ) {			
			$var(kkey) = $(avp(result)[$var(it)]);
			$json(call_json/$var(kkey)) = $(avp(result)[$var(it2)]);
		}
		$var(it) = $var(it) + 2;
	}
	xlog("ILYA:: THE JSON IS  $json(call_json) ");
	$var(single_call_json) = $json(call_json);

	script_trace();
	return(1);
}

event_route[E_ACC_MISSED_EVENT] {
	xlog("XXXX - E_ACC_MISSED event \n");

        $json(cdr) := "{}";
	$json(cdr/method) =  $param(method) ;
        $json(cdr/from_tag) =  $param(from_tag);
        $json(cdr/to_tag) =  $param(to_tag);
        $json(cdr/callid) =  $param(callid) ;
        $json(cdr/sip_code) =  $param(sip_code);
        $json(cdr/sip_reason) =  $param(sip_reason);
        $json(cdr/time) =  $param(time) ;
        $json(cdr/from_uri) =  $param(from_uri);
        $json(cdr/to_uri) =  $param(to_uri);
        $json(cdr/domain) =  $param(domain) ;
        $json(cdr/ruri) =  $param(ruri) ;
	$json(cdr/src_ip) =  $param(src_ip);
        $json(cdr/src_port) =  $param(src_port);
        $json(cdr/contact) =  $param(contact);
        $json(cdr/rpi) =  $param(rpi);
        $json(cdr/accountid) =  $param(accountid);
        $json(cdr/duration) =  $param(duration);
        $json(cdr/setuptime) =  $param(setuptime);
        $json(cdr/created) =  $param(created) ;
        $json(cdr/direction) =  $param(direction);
	$json(cdr/rtpproxy_setid) = $param(rtpproxy_setid);
	$json(cdr/original_callid) = $param(original_callid);
        $json(cdr/multi_leg_info) =  $param(multi_leg_info) ;
        $json(cdr/multi_leg_bye_info) =  $param(multi_leg_bye_info);
	$json(cdr/uid) =  $param(uid);
	$json(cdr/guid) = $param(guid);

	$var(new_body) =  $json(cdr) ;
	$avp(hdr_name) = "EventType";
	$avp(hdr_value) = "CDR" ;

	xlog("XXXX - built json object $json(cdr) for dialog $DLG_did \n");
	

	$var(rmq_headers) = $(dlg_val(rmq_headers){s.decode.hexa});
	
	xlog("XXXX - rmq headers are $dlg_val(rmq_headers) decoded to  $var(rmq_headers) in dlg $DLG_did \n");

	if ($var(rmq_headers) != NULL && $var(rmq_headers) != "") {
		$json(rmq_headers) := $var(rmq_headers);

		for ($var(k) in $(json(rmq_headers.keys)[*])) {
			$avp(hdr_name) = $var(k);
			$avp(hdr_value) = $json(rmq_headers/$var(k));
		}

	}

        if (!rabbitmq_publish("rabbitmq_cdr1", "RMQ_EXCH_CDR", $var(new_body), "application/json", $avp(hdr_name), $avp(hdr_value))){
               xlog("L_ERR", "cannot publish cdr event to rabbitmq_cdr RMQ_HOST  server event=$var(event) user=$var(event) body=[$var(new_body)]\n");
		if (!rabbitmq_publish("rabbitmq_cdr2", "RMQ_EXCH_CDR", $var(new_body), "application/json", $avp(hdr_name), $avp(hdr_value))){
              	  xlog("L_ERR", "FATAL CDR ERROR cdr event to rabbitmq_cdr2 RMQ_HOST2 Failed  server event=$var(event) user=$var(event) body=[$var(new_body)]\n");
		}

        

			
        }

	xlog("XXX - cdr info :  $var(new_body) \n"); 
}

event_route[E_ACC_CDR] {
	xlog("XXXXX - E_ACC_CDR event \n");

        $json(cdr) := "{}";
	$json(cdr/method) =  $param(method) ;
        $json(cdr/from_tag) =  $param(from_tag);
        $json(cdr/to_tag) =  $param(to_tag);
        $json(cdr/callid) =  $param(callid) ;
        $json(cdr/sip_code) =  $param(sip_code);
        $json(cdr/sip_reason) =  $param(sip_reason);
        $json(cdr/time) =  $param(time) ;
        $json(cdr/from_uri) =  $param(from_uri);
        $json(cdr/to_uri) =  $param(to_uri);
        $json(cdr/domain) =  $param(domain) ;
        $json(cdr/ruri) =  $param(ruri) ;
	$json(cdr/src_ip) =  $param(src_ip);
        $json(cdr/src_port) =  $param(src_port);
        $json(cdr/contact) =  $param(contact);
        $json(cdr/rpi) =  $param(rpi);
        $json(cdr/accountid) =  $param(accountid);
        $json(cdr/duration) =  $param(duration);
        $json(cdr/setuptime) =  $param(setuptime);
        $json(cdr/created) =  $param(created) ;
        $json(cdr/direction) =  $param(direction);
	$json(cdr/rtpproxy_setid) = $param(rtpproxy_setid);
	$json(cdr/original_callid) = $param(original_callid);
        $json(cdr/multi_leg_info) =  $param(multi_leg_info) ;
        $json(cdr/multi_leg_bye_info) =  $param(multi_leg_bye_info);
	$json(cdr/uid) =  $param(uid);
	$json(cdr/guid) = $param(guid);

	$var(new_body) =  $json(cdr) ;
	$avp(hdr_name) = "EventType";
	$avp(hdr_value) = "CDR" ;

	xlog("XXXX - built json object $json(cdr) for dialog $DLG_did \n");
	

	$var(rmq_headers) = $(dlg_val(rmq_headers){s.decode.hexa});
	
	xlog("XXXX - rmq headers are $dlg_val(rmq_headers) decoded to  $var(rmq_headers) in dlg $DLG_did \n");

	if ($var(rmq_headers) != NULL && $var(rmq_headers) != "") {
		$json(rmq_headers) := $var(rmq_headers);

		for ($var(k) in $(json(rmq_headers.keys)[*])) {
			$avp(hdr_name) = $var(k);
			$avp(hdr_value) = $json(rmq_headers/$var(k));
		}

	}

        if (!rabbitmq_publish("rabbitmq_cdr1", "RMQ_EXCH_CDR", $var(new_body), "application/json", $avp(hdr_name), $avp(hdr_value))){
               xlog("L_ERR", "cannot publish cdr event to rabbitmq_cdr RMQ_HOST  server event=$var(event) user=$var(event) body=[$var(new_body)]\n");
		if (!rabbitmq_publish("rabbitmq_cdr2", "RMQ_EXCH_CDR", $var(new_body), "application/json", $avp(hdr_name), $avp(hdr_value))){
              	  xlog("L_ERR", "FATAL CDR ERROR cdr event to rabbitmq_cdr2 RMQ_HOST2 Failed  server event=$var(event) user=$var(event) body=[$var(new_body)]\n");
		}

        

			
        }

	xlog("XXX - cdr info : $var(new_body) \n"); 
}

route[fork_call]  {
	xlog("user $avp(aor) registered a new contact $avp(uri), injecting it in transaction - $avp(received) - $Ri ; flags = $avp(bflags) ; attrs = $avp(attr) \n");
	t_inject_branches("event");
}

route[sp_upsert_sim] {
	$var(rc) = avp_db_query("call sp_upsert_sim ('$avp(sim_id)' ,'$avp(sim_attrs)');");
	$avp(ret) = "{\"success\:\"" + $var(rc) + "\"}";
}

# expecting input as : {'data':[{'key':'$.xxx.xx','value':'yyy'},{'key':'$.xxx.xx3','value':'yyy'}],'subscriber':'alex4'}
route[subscriber_attrs_update] {
	$json(update) := $avp(input);
	$avp(subscriber) = $json(update/subscriber);	
	
#	
#	$avp(query) = "update opensips.subscriber set attrs=JSON_SET(attrs";
#	$json(data) := $json(update/data);
#	for ($var(v) in $(json(data)[*])) {
#		$json(kv) := $var(v);
#		$avp(query) = $avp(query) + ",'" + $json(kv/key) + "','" + $json(kv/val) + "'"; 
#	}
#	
#	$avp(query) = $avp(query) + ") where username='" + $avp(subscriber) + "'"; 
#
#	xlog("XXX - subscriber attrs update built query $avp(query) \n");

	xlog("subscriber_attrs_update running query CALL opensips.SP_UpdateOpenSIPSSubscriberAttr('$avp(input)') \n");
	$var(rc) = avp_db_query("CALL opensips.SP_UpdateOpenSIPSSubscriberAttr('$avp(input)')");
	cache_remove("local","auth_$avp(subscriber)");

	$avp(ret) = '{"success:"' + $var(rc) + '"}';
}

route[load_ratelimit_profile]
{
	if (cache_fetch("local","rlprofile_$avp(ratelimit_profile)",$avp(rl_profile_info))) {
		$json(rl_profile_info) := $avp(rl_profile_info);
		for ($var(profile_entry) in $(json(rl_profile_info)[*])) {
			xlog("XXX- loaded cached entry $var(profile_entry) \n");
			$json(profile_entry) := $var(profile_entry);

			$avp(ratelimit_profile_name) = $json(profile_entry[0]);
			$avp(ratelimit_limit) = $json(profile_entry[1]);
			$avp(ratelimit_algorithm) = $json(profile_entry[2]);
			$avp(ratelimit_profile_attrs) = $json(profile_entry[3]);
		}
	} else {
		avp_db_query("select ratelimit_profile_name,profile_limit,algorithm,ratelimit_profile_attrs from ratelimit_profile where ratelimit_profile_id=$avp(ratelimit_profile)","$avp(ratelimit_profile_name);$avp(ratelimit_limit);$avp(ratelimit_algorithm);$avp(ratelimit_profile_attrs)"); 
                switch ($retcode) {
                        case -1:
                        case -2:
                                return (-1);
                }

		$json(rl_profile_info) := "[]";
		$var(idx) = 0;
		while ($(avp(ratelimit_profile_name)[$var(idx)]) != NULL) {
			$json(profile_entry) := "[]";
			$json(profile_entry[]) = $(avp(ratelimit_profile_name)[$var(idx)]); 
			$json(profile_entry[]) = $(avp(ratelimit_limit)[$var(idx)]); 
			$json(profile_entry[]) = $(avp(ratelimit_algorithm)[$var(idx)]); 
			$json(profile_entry[]) = $(avp(ratelimit_profile_attrs)[$var(idx)]); 

			xlog("XXX - built entry $json(profile_entry) ; adding to $json(rl_profile_info) \n");
			$json(rl_profile_info[]) = $json(profile_entry);

			$var(idx) = $var(idx) + 1;
		}

		xlog("XXX - built ratelimit $json(rl_profile_info) \n");

		cache_store("local","rlprofile_$avp(ratelimit_profile)","$json(rl_profile_info)",3600);
	}

	return (1);
}


route[check_ratelimit_profile]
{
	if (!route(load_ratelimit_profile)) {
		xlog("RATELIMIT: Failed to load profile $avp(ratelimit_profile) \n");
		return;
	}

	$var(idx) = 0;
	$var(stop) = 0;
	while ($(avp(ratelimit_profile_name)[$var(idx)]) != NULL && $var(stop) == 0) {
		$var(rl_profile_name) = $(avp(ratelimit_profile_name)[$var(idx)]);
		$var(rl_profile_name) = $(var(rl_profile_name){s.eval});
		xlog("XXX -checking profile $avp(ratelimit_profile) with $(avp(ratelimit_profile_name)[$var(idx)]) = $var(rl_profile_name) - $(avp(ratelimit_limit)[$var(idx)]) - $(avp(ratelimit_algorithm)[$var(idx)]) \n");
		if (!rl_check($(avp(ratelimit_profile_name)[$var(idx)]),$(avp(ratelimit_limit)[$var(idx)]),$(avp(ratelimit_algorithm)[$var(idx)]))) {
			xlog("RATELIMIT: Exceeded limit in profile $avp(ratelimit_profile) \n"); 
			$var(stop) = 1;

        		$json(ratelimit) := "{}";
			$json(ratelimit/ratelimit_profile) = $avp(ratelimit_profile);
			$json(ratelimit/ratelimit_profile_name) = $var(rl_profile_name);
			$json(ratelimit/ratelimit_profile_attrs) = $(avp(ratelimit_profile_attrs)[$var(idx)]);
			$json(ratelimit/src_ip) = $si;
			$json(ratelimit/domain) = $rd;
			$var(new_body) =  $json(ratelimit) ;
			$avp(hdr_name) = "EventType";
			$avp(hdr_value) = "RATELIMIT" ;

			if (!rabbitmq_publish("rabbitmq_ratelimit1", "RMQ_EXCH_RATELIMIT", $var(new_body), "application/json", $avp(hdr_name), $avp(hdr_value))){
			       xlog("L_ERR", "cannot publish ratelimit event to rabbitmq \n");
			}

		}
        
		$var(idx) = $var(idx) + 1;
	}

}

#route[vlad_route] {
#        xlog("In custom route : $(avp(name1)[*]) ; $(avp(name2)[*]) \n");
#
#        $avp(ret) = "{\"success\":\"true\"}"; 
#}

#timer_route[rabbit, 10] {
#        #$var(new_body)='{"type":"ROUTE","name":"vlad_route","params":{"name1":["val1","val2"],"name2":["val3"]},"get_output":"rabbitmq_realtime2"}';
#        #$var(new_body)='{"type":"ROUTE","name":"vlad_route","params":{"name1":["val1","val2"],"name2":["val3"]}}';

#        $var(new_body)='{"type":"MI","name":"ul_show_contact","params":"location,alex2@c7593.ratetel.com","get_output":"rabbitmq_realtime2"}';

#        $avp(hdr_name)="EventType";
#        $avp(hdr_value)="CUSTOM";

#        if (!rabbitmq_publish("rabbitmq_realtime1", "test", "$var(new_body)", "application/json", "$avp(hdr_name)", "$avp(hdr_value)")) {
#                xlog("XXX - failed :( \n");
#        }

#}
