#! /usr/bin/python

import sys
from xmlrpclib import ServerProxy
from demjson import decode
import json

srv = ServerProxy("http://192.168.201.201:8080/RPC2/")

#QUERY_JSON='[{"tags":["e103"],"min_state": 0, "max_state": 5, "limit": 3},{"tags":["e105"]}]'
#QUERY_JSON='[{"tags":["e103"],"min_state": 0, "max_state": 5, "limit": 3}]'

QUERY_JSON = sys.argv[1]

text_file = open("/tmp/search_tags.log", "w")
text_file.write("Query is %s" % QUERY_JSON)

text_file.close()

val = srv.dlg_search_tags(QUERY_JSON)
python_dict = decode(str(val))

out = ""
for elem in python_dict['RESULT']:
        out = out + str(elem)

json_dict = json.loads(out)

ret = "["
j = 0
for elem in json_dict:
	ret = ret + "["
	i = 0
	for inner_dlg in elem:
		ret = ret + inner_dlg + ","
		i = i+1
		
	if i > 0:
		ret = ret[:-1]

	ret = ret + "],"
	
	j + j + 1
	
ret = ret[:-1]

ret = ret + "]"
print ret
