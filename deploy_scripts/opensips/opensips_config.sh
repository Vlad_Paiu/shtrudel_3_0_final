#!/bin/bash
## Autoconfig for m4 autoconfig
# Options available/Default values
echo -e "Configuration parameters:\n"
_options=( "OSIPS_MPATH::/lib64/opensips/modules/"
"TLS_PATH::/etc/opensips/tls/"
"ANYCAST_IP::185.138.171.254"
"MY_IP::209.163.136.201"
"MY_PRIV_IP::192.168.201.201"
"MY_CLUSTER_IP::192.168.201.201"
"CLUSTER_ID::1"
"MY_CLUSTER_NODE_ID::201"
"DB_HOST::127.0.0.1"
"DB_USER::root"
"DB_PASS::00!*!00"
"DB_NAME::opensips"
"RMQ_EXCH_REALTIME::REALTIMECDR"
"RMQ_EXCH_CDR::CDR"
"RMQ_EXCH_LOCATION::LOCATION"
"RMQ_EXCH_SECURITY::OPENSIPS_IP"
"CDR_RMQ_HOST::192.168.201.88"
"CDR_RMQ_USER::admin"
"CDR_RMQ_PASS::00!*!00"
"CDR_RMQ_PORT::5672"
"RMQ_HOST::192.168.201.184"
"RMQ_USER::admin"
"RMQ_PASS::00!*!00"
"RMQ_PORT::5672"
"RMQ_HOST2::192.168.201.183"
"RMQ_USER2::admin"
"RMQ_PASS2::00!*!00"
"RMQ_PORT2::5672")


# Generate name for template file
ETC_FOLDER=/etc/opensips

_filename=local_$(date +%s).m4.template

touch $ETC_FOLDER/$_filename
echo "divert(-1)" > $ETC_FOLDER/$_filename

_force=0
while getopts ":f" opt; do
  case $opt in
    f) _force=1
    ;;
    \? ) echo "Usage: cmd [-f]"
    ;;
  esac
done

# Ask for some input for each parameter or use default
for _option in "${_options[@]}"; do
    KEY="${_option%%:*}"
    VALUE="${_option##*:}"
    if [ $_force -eq 0 ]; then
        read -p "$KEY[$VALUE]: " _input
    fi
    if [[ -n $_input ]]; then
        VALUE=$_input
    fi
    if [[ $KEY == "DB_HOST" ]]; then
        _DB_HOST=$VALUE
    fi
    if [[ $KEY == "DB_USER" ]]; then
        _DB_USER=$VALUE
    fi
    if [[ $KEY == "DB_PASS" ]]; then
        _DB_PASS=$VALUE
    fi
    if [[ $KEY == "DB_NAME" ]]; then
        _DB_NAME=$VALUE
    fi
    printf "%s => %s.\n" "$KEY" "$VALUE"
    echo "define(\`${KEY}', \`${VALUE}')" >> $ETC_FOLDER/$_filename
done

# File finished
echo -e "divert\n" >> $ETC_FOLDER/$_filename

# Run m4 generation
echo -- $ETC_FOLDER/$_filename
m4 $ETC_FOLDER/$_filename $ETC_FOLDER/opensips_proxy.m4 > $ETC_FOLDER/opensips_proxy.cfg
#echo -- opensips_proxy.cfg
#rm $_filename

mv $ETC_FOLDER/local.m4.template $ETC_FOLDER/local.m4.template.backup
mv $ETC_FOLDER/$_filename $ETC_FOLDER/local.m4.template

# MySQL data source selection
# _o_ prefix is for opensips DB
# _p_ prefix is for Prefix DB

# opensips DB
_o_dbInit="1"

echo "1) local"
echo "2) http"
read -p "Opensips DB init[$_o_dbInit]: " _input

if [[ $_input == "1" || $_input == "2" ]]; then
    _o_dbInit=$_input
fi

if [[ $_o_dbInit == "1" ]]; then
    # Local option selected
    _o_localAddress="/etc/opensips/opensips_db_dump.sql"
    read -p "Local Address[$_o_localAddress] :" _input
    if [[ -n $_input ]]; then
        _o_localAddress=$_input
    fi
    _o_lastDump=$_o_localAddress # $(ls -t $_o_localAddress/*.sql | head -1) #???
else
    # HTTP option selected
    _o_httpAddress="192.168.201.253/latest_dump.sql"
    read -p "HTTP Address[$_o_httpAddress] :" _input
    if [[ -n $_input ]]; then
        _o_httpAddress=$_input
    fi
    wget -O o_dump.sql $_o_httpAddress
    _o_lastDump=$(ls -t o_dump.sql | head -1)
fi

# Prefix DB
_p_dbInit="1"

echo "1) local"
echo "2) http"
read -p "Prefix DB init[$_p_dbInit]: " _input

if [[ $_input == "1" || $_input == "2" ]]; then
    _p_dbInit=$_input
fi

if [[ $_p_dbInit == "1" ]]; then
    # Local option selected
    _p_localAddress="/etc/opensips/Prefix_db_dump.sql"
    read -p "Local Address[$_p_localAddress] :" _input
    if [[ -n $_input ]]; then
        _p_localAddress=$_input
    fi
    _p_lastDump=$_p_localAddress # $(ls -t $_p_localAddress/*.sql | head -1)
else
    # HTTP option selected
    _p_httpAddress="192.168.201.253/latest_dump.sql"
    read -p "HTTP Address[$_p_httpAddress] :" _input
    if [[ -n $_input ]]; then
        _p_httpAddress=$_input
    fi
    wget -O p_dump.sql $_p_httpAddress
    _p_lastDump=$(ls -t p_dump.sql | head -1)
fi

# UPDATE DB user/pass
if [[ "$_DB_HOST" == "127.0.0.1" ]]; then
    _DB_HOST="localhost"
fi

if [[ "$_DB_USER" == "root" ]] && [[ "$_DB_HOST" == "localhost" ]]; then # user root
    echo "root @ local"
    mysql -h localhost -u root mysql -e "UPDATE mysql.user SET plugin='mysql_native_password', authentication_string=PASSWORD('$_DB_PASS') WHERE User='root' AND Host='localhost';"
else
    if [[ "$_DB_HOST" == "localhost" ]]; then
        echo "non root @ local"
        mysql -h localhost -u root mysql -e "CREATE USER '$_DB_USER'@'$_DB_HOST' IDENTIFIED BY '$_DB_PASS';"
        mysql -h localhost -u root mysql -e "GRANT ALL on $_DB_NAME.* to '$_DB_USER'@'$_DB_HOST';"
        mysql -h localhost -u root mysql -e "GRANT ALL on Prefix.* to '$_DB_USER'@'$_DB_HOST';"
        mysql -h localhost -u root mysql -e "UPDATE mysql.user SET Super_Priv='Y' WHERE user='$_DB_USER' AND host='$_DB_HOST';"
        mysql -h localhost -u root mysql -e "UPDATE mysql.user SET plugin='mysql_native_password' WHERE User='$_DB_USER' AND Host='$_DB_HOST';FLUSH PRIVILEGES;"
    fi
fi
service mysql restart
sleep 5

# Put opensips dump to the DB if it exists
if [[ -n $_o_lastDump ]]; then
    echo $_o_lastDump
    mysql -h $_DB_HOST -u $_DB_USER -p$_DB_PASS -e "drop database if exists ${_DB_NAME}"
    mysql -h $_DB_HOST -u $_DB_USER -p$_DB_PASS -e "create database ${_DB_NAME}"
    mysql -h $_DB_HOST -u $_DB_USER -p$_DB_PASS $_DB_NAME < $_o_lastDump
#    mysql -e "grant all privileges on opensips.* to opensips@127.0.0.1 identified by 'opensips_rw'"
#    mysql -e "flush privileges"
else
    echo "No dump file for ${_DB_NAME}"
fi
# Put Prefix dump to the DB if it exists
if [[ -n $_p_lastDump ]]; then
    echo $_p_lastDump
    mysql -h $_DB_HOST -u $_DB_USER -p$_DB_PASS -e "drop database if exists Prefix"
    mysql -h $_DB_HOST -u $_DB_USER -p$_DB_PASS -e "create database Prefix"
    mysql -h $_DB_HOST -u $_DB_USER -p$_DB_PASS Prefix < $_p_lastDump
#    mysql -e "grant all privileges on Prefix.* to opensips@127.0.0.1 identified by 'opensips_rw'"
#    mysql -e "flush privileges"
else
    echo "No dump file for Prefix"
fi