#!/bin/bash

set -x

LOCAL_IP=ETH1IP
UAS_PORT=5090
UAC_PORT=5091
UAS_USERNAME=alex1
UAS_EXTENSION=101
UAS_PASSWORD=00!alex1!00
REGISTRAR=185.138.171.254
TIMEOUT=5
EXEC_TIMEOUT=5

EXTRA="-m 1 -r 1 -nd"

DIR=/usr/local/src/sipp-monitoring
RESULTFILE=$DIR/sipp-monitoring.log
ERRFILE=$DIR/sipp-monitoring.err
SCENARIO_DIR=/usr/local/src/sipp-monitoring
UAC_SCENARIO=$SCENARIO_DIR/uac.xml
UAS_REG_SCENARIO=$SCENARIO_DIR/uas-register.xml
UAS_CALL_SCENARIO=$SCENARIO_DIR/uas-call.xml

# locking file
LOCKFILE=$DIR/sipp-monitoring

SIPP=/usr/local/bin/sipp

[ -x "$SIPP" ] || exit
[ -z "$REGISTRAR" -o -z "$UAS_USERNAME" -o -z "$UAS_PASSWORD" ] && exit

/usr/bin/lockfile-create -r 0 "$LOCKFILE" || exit
export TERM=xterm

function cleanup {
        EXIT=$1
	REASON=$2

        /usr/bin/lockfile-remove "$LOCKFILE"
	echo $REASON > $RESULTFILE
        exit $EXIT
}


function register_uas {
  /bin/echo "Starting UAS Register scenario ..." &>> $DIR/uas-register.log
  timeout $EXEC_TIMEOUT $SIPP \
  	-i $LOCAL_IP \
  	-p $UAS_PORT \
  	-s $UAS_USERNAME \
  	-ap $UAS_PASSWORD \
  	-sf $UAS_REG_SCENARIO \
  	-timeout $TIMEOUT \
  	$REGISTRAR \
  	$EXTRA &>> $DIR/uas-register.log

  RET=$?

  [ "$RET" = "0" ] || cleanup $RET "UAS cannot register"
}

function uas_call {
        /bin/echo "Starting UAS Call scenario ..." &>> $DIR/uas-call.log
        timeout $EXEC_TIMEOUT $SIPP \
                        -i $LOCAL_IP \
                        -p $UAS_PORT \
                        -sf $UAS_CALL_SCENARIO \
                        -timeout $TIMEOUT \
                        $EXTRA &>> $DIR/uas-call.log

        case $? in
        97)
          MSG="${TIMEOUT}s timeout"
          ;;
        *)
          MSG="error $?"
          ;;
        esac

        cleanup $? 0 "$MSG"
}

#==============================================================================

#set -x

register_uas

uas_call &
UAS_PID=$!

/bin/echo "Starting UAC Call scenario ..." &>> $DIR/uac-call.log
timeout $EXEC_TIMEOUT $SIPP \
        -i $LOCAL_IP \
        -p $UAC_PORT \
        -s $UAS_EXTENSION \
        -sf $UAC_SCENARIO \
        -timeout $TIMEOUT \
        $REGISTRAR \
        $EXTRA &>> $DIR/uac-call.log
RET=$?

wait $UAS_PID
RET_WAIT=$?

[ "$RET" = "0" ] || cleanup $RET "UAC cannot call"
[ "$RET_WAIT" = "0" ] || cleanup $RET_WAIT "UAS cannot receive call"

cleanup 0 "OK"


