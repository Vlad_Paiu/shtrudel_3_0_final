LOCAL_IP=217.20.39.99
LOCAL_PORT=5062
USERNAME=9999999
REGISTRAR=sip.orbtalk.co.uk:5060
SCENARIO=uac.xml

/usr/local/sbin/sipp \
	-i $LOCAL_IP \
	-p $LOCAL_PORT \
	-s $USERNAME \
	-sf $SCENARIO \
	-m 1 \
	-r 1 \
	$REGISTRAR
