LOCAL_IP=209.163.136.215
LOCAL_PORT=5060
USERNAME=alex1
PASSWORD=00!alex1!00
REGISTRAR=185.138.171.254
SCENARIO=uas-register.xml
TIMEOUT=10
EXTRA="-m 1 -r 1"

/usr/local/bin/sipp \
	-i $LOCAL_IP \
	-p $LOCAL_PORT \
	-s $USERNAME \
	-ap $PASSWORD \
	-sf $SCENARIO \
	-timeout $TIMEOUT \
	-nd \
	$EXTRA \
	$REGISTRAR
