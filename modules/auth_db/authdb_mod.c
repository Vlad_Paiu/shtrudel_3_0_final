/*
 * Digest Authentication Module
 *
 * Copyright (C) 2001-2003 FhG Fokus
 *
 * This file is part of opensips, a free SIP server.
 *
 * opensips is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version
 *
 * opensips is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * History:
 * --------
 * 2003-02-26: checks and group moved to separate modules (janakj)
 * 2003-03-11: New module interface (janakj)
 * 2003-03-16: flags export parameter added (janakj)
 * 2003-03-19  all mallocs/frees replaced w/ pkg_malloc/pkg_free (andrei)
 * 2003-04-05: default_uri #define used (jiri)
 * 2004-06-06  cleanup: static & auth_db_{init,bind,close.ver} used (andrei)
 * 2005-05-31  general definition of AVPs in credentials now accepted - ID AVP,
 *             STRING AVP, AVP aliases (bogdan)
 * 2006-03-01 pseudo variables support for domain name (bogdan)
 */

#include <stdio.h>
#include <string.h>
#include "../../sr_module.h"
#include "../../db/db.h"
#include "../../dprint.h"
#include "../../cachedb/cachedb.h"
#include "../../cachedb/cachedb_cap.h"
#include "../../error.h"
#include "../../mem/mem.h"
#include "../auth/api.h"
#include "../signaling/signaling.h"
#include "aaa_avps.h"
#include "authorize.h"
#include "checks.h"

/*
 * Version of domain table required by the module,
 * increment this value if you change the table in
 * an backwards incompatible way
 */
#define SUBSCRIBER_TABLE_VERSION 7
#define URI_TABLE_VERSION        2

/*
 * Module destroy function prototype
 */
static void destroy(void);


/*
 * Module child-init function prototype
 */
static int child_init(int rank);


/*
 * Module initialization function prototype
 */
static int mod_init(void);


static int auth_fixup_table(void** param);
static int fixup_check_outvar(void **param);

/** SIGNALING binds */
struct sig_binds sigb;

#define USER_COL "username"
#define USER_COL_LEN (sizeof(USER_COL) - 1)

#define DOMAIN_COL "domain"
#define DOMAIN_COL_LEN (sizeof(DOMAIN_COL) - 1)

#define PASS_COL "ha1"
#define PASS_COL_LEN (sizeof(PASS_COL) - 1)

#define PASS_COL_2 "ha1b"
#define PASS_COL_2_LEN (sizeof(PASS_COL_2) - 1)

#define DEFAULT_CRED_LIST "rpid"

/*
 * Module parameter variables
 */
static str db_url           = {NULL,0};
str user_column             = {USER_COL, USER_COL_LEN};
str domain_column           = {DOMAIN_COL, DOMAIN_COL_LEN};
str pass_column             = {PASS_COL, PASS_COL_LEN};
str pass_column_2           = {PASS_COL_2, PASS_COL_2_LEN};

str uri_user_column         = str_init("username");
str uri_domain_column       = str_init("domain");
str uri_uriuser_column      = str_init("uri_user");

int calc_ha1                = 0;
int use_domain              = 0; /* Use also domain when looking up in table */

db_con_t* auth_db_handle    = 0; /* database connection handle */
db_func_t auth_dbf;
auth_api_t auth_api;

char *credentials_list      = DEFAULT_CRED_LIST;
struct aaa_avp *credentials = 0; /* Parsed list of credentials to load */
int credentials_n           = 0; /* Number of credentials in the list */
int skip_version_check      = 0; /* skips version check for custom db */

str reload_procedure = {0,0};
str delete_procedure = {0,0};

/* cachedb stuff */
str cdb_url = {0,0};
static cachedb_funcs cdbf;
static cachedb_con *cdbc = 0;

mi_response_t *mi_get_auth_info_2(const mi_params_t *params,
								struct mi_handler *async_hdl);
mi_response_t *mi_upsert_auth_info_4(const mi_params_t *params,
								struct mi_handler *async_hdl);

mi_response_t *mi_delete_auth_info_2(const mi_params_t *params,
								struct mi_handler *async_hdl);
#define HLP1 "Username domain"
#define HLP2 "Username domain attrs password"
#define HLP3 "Username domain"

static mi_export_t mi_cmds[] = {
	{ "get_auth_info", HLP1, MI_NAMED_PARAMS_ONLY, 0, {
		{mi_get_auth_info_2, {"username","domain", 0}},
		{EMPTY_MI_RECIPE}}
	},
	{ "upsert_auth_info", HLP2, 0, 0, {
		{mi_upsert_auth_info_4, {"username","domain","attrs","password", 0}},
		{EMPTY_MI_RECIPE}}
	},
	{ "delete_auth_info", HLP3, 0, 0, {
		{mi_delete_auth_info_2, {"username","domain", 0}},
		{EMPTY_MI_RECIPE}}
	},
	{EMPTY_MI_EXPORT}
};

/*
 * Exported functions
 */

static cmd_export_t cmds[] = {
	{"www_authorize", (cmd_function)www_authorize, {
		{CMD_PARAM_STR, 0, 0},
		{CMD_PARAM_STR, auth_fixup_table, 0}, {0,0,0}},
		REQUEST_ROUTE},
	{"proxy_authorize", (cmd_function)proxy_authorize, {
		{CMD_PARAM_STR, 0, 0},
		{CMD_PARAM_STR, auth_fixup_table, 0}, {0,0,0}},
		REQUEST_ROUTE},
	{"db_does_uri_exist", (cmd_function)does_uri_exist, {
		{CMD_PARAM_STR, 0, 0},
		{CMD_PARAM_STR, auth_fixup_table, 0}, {0,0,0}},
		REQUEST_ROUTE|FAILURE_ROUTE|LOCAL_ROUTE},
	{"db_is_to_authorized", (cmd_function)check_to, {
		{CMD_PARAM_STR, auth_fixup_table, 0}, {0,0,0}},
		REQUEST_ROUTE},
	{"db_is_from_authorized", (cmd_function)check_from, {
		{CMD_PARAM_STR, auth_fixup_table, 0}, {0,0,0}},
		REQUEST_ROUTE},
	{"db_get_auth_id", (cmd_function) get_auth_id, {
		{CMD_PARAM_STR, auth_fixup_table, 0},
		{CMD_PARAM_STR, 0, 0},
		{CMD_PARAM_VAR, fixup_check_outvar, 0},
		{CMD_PARAM_VAR, fixup_check_outvar, 0}, {0,0,0}},
		REQUEST_ROUTE|FAILURE_ROUTE|LOCAL_ROUTE},
	{0,0,{{0,0,0}},0}
};

/*
 * Exported parameters
 */
static param_export_t params[] = {
	{"db_url",            STR_PARAM, &db_url.s            },
	{"user_column",       STR_PARAM, &user_column.s       },
	{"domain_column",     STR_PARAM, &domain_column.s     },
	{"password_column",   STR_PARAM, &pass_column.s       },
	{"password_column_2", STR_PARAM, &pass_column_2.s     },
	{"uri_user_column",   STR_PARAM, &uri_user_column.s   },
	{"uri_domain_column", STR_PARAM, &uri_domain_column.s },
	{"uri_uriuser_column",STR_PARAM, &uri_uriuser_column.s},
	{"calculate_ha1",     INT_PARAM, &calc_ha1            },
	{"use_domain",        INT_PARAM, &use_domain          },
	{"load_credentials",  STR_PARAM, &credentials_list    },
	{"skip_version_check",INT_PARAM, &skip_version_check  },
	{ "cachedb_url",      STR_PARAM, &cdb_url.s          },
	{ "reload_procedure", STR_PARAM, &reload_procedure.s },
	{ "delete_procedure", STR_PARAM, &delete_procedure.s },
	{0, 0, 0}
};


static dep_export_t deps = {
	{ /* OpenSIPS module dependencies */
		{ MOD_TYPE_DEFAULT, "auth", DEP_ABORT },
		{ MOD_TYPE_NULL, NULL, 0 },
	},
	{ /* modparam dependencies */
		{ "db_url", get_deps_sqldb_url },
		{ "cachedb_url",get_deps_cachedb_url },
		{ NULL, NULL },
	},
};

/*
 * Module interface
 */
struct module_exports exports = {
	"auth_db",
	MOD_TYPE_DEFAULT,/* class of this module */
	MODULE_VERSION,
	DEFAULT_DLFLAGS, /* dlopen flags */
	0,				 /* load function */
	&deps,           /* OpenSIPS module dependencies */
	cmds,       /* Exported functions */
	0,          /* Exported async functions */
	params,     /* Exported parameters */
	0,          /* exported statistics */
	mi_cmds,    /* exported MI functions */
	0,          /* exported pseudo-variables */
	0,			/* exported transformations */
	0,          /* extra processes */
	0,          /* module pre-initialization function */
	mod_init,   /* module initialization function */
	0,          /* response function */
	destroy,    /* destroy function */
	child_init, /* child initialization function */
	0           /* reload confirm function */
};


static int child_init(int rank)
{
	auth_db_handle = auth_dbf.init(&db_url);
	if (auth_db_handle == 0){
		LM_ERR("unable to connect to the database\n");
		return -1;
	}

	if (cdb_url.s) {
		cdbc = cdbf.init(&cdb_url);
		if (!cdbc) {
			LM_ERR("cannot connect to cachedb_url %.*s\n", cdb_url.len, cdb_url.s);
			return -1;
		}
	}

	return 0;
}


static int mod_init(void)
{
	bind_auth_t bind_auth;

	LM_INFO("initializing...\n");

	init_db_url( db_url , 0 /*cannot be null*/);
	user_column.len = strlen(user_column.s);
	domain_column.len = strlen(domain_column.s);
	pass_column.len = strlen(pass_column.s);
	pass_column_2.len = strlen(pass_column_2.s);

	uri_user_column.len = strlen(uri_user_column.s);
	uri_domain_column.len = strlen(uri_domain_column.s);
	uri_uriuser_column.len = strlen(uri_uriuser_column.s);

	/* Find a database module */
	if (db_bind_mod(&db_url, &auth_dbf) < 0){
		LM_ERR("unable to bind to a database driver\n");
		return -1;
	}

	/* bind to auth module and import the API */
	bind_auth = (bind_auth_t)find_export("bind_auth", 0);
	if (!bind_auth) {
		LM_ERR("unable to find bind_auth function."
			" Check if you load the auth module.\n");
		return -2;
	}

	if (bind_auth(&auth_api) < 0) {
		LM_ERR("unable to bind auth module\n");
		return -3;
	}

	/* load SIGNALING API */
	if(load_sig_api(&sigb)< 0) {
		LM_ERR("can't load signaling functions\n");
		return -1;
	}

	/* process additional list of credentials */
	if (parse_aaa_avps( credentials_list, &credentials, &credentials_n)!=0) {
		LM_ERR("failed to parse credentials\n");
		return -5;
	}

	if (reload_procedure.s)
		reload_procedure.len = strlen(reload_procedure.s);
	if (delete_procedure.s)
		delete_procedure.len = strlen(delete_procedure.s);

	if (cdb_url.s) {
		cdb_url.len = strlen(cdb_url.s);

		if (cachedb_bind_mod(&cdb_url, &cdbf) < 0) {
			LM_ERR("cannot bind functions for cachedb_url %.*s\n",
					cdb_url.len, cdb_url.s);
			return -1;
		}
		if (!CACHEDB_CAPABILITY(&cdbf,
					CACHEDB_CAP_GET|CACHEDB_CAP_ADD|CACHEDB_CAP_SUB)) {
			LM_ERR("not enough capabilities\n");
			return -1;
		}

		cdbc = cdbf.init(&cdb_url);
		if (!cdbc) {
			LM_ERR("cannot connect to cachedb_url %.*s\n", cdb_url.len, cdb_url.s);
			return -1;
		}
	}


	return 0;
}


static void destroy(void)
{
	if (auth_db_handle) {
		auth_dbf.close(auth_db_handle);
		auth_db_handle = 0;
	}
	if (credentials) {
		free_aaa_avp_list(credentials);
		credentials = 0;
		credentials_n = 0;
	}
}

static int auth_fixup_table(void** param)
{
	db_con_t *dbh = NULL;

	dbh = auth_dbf.init(&db_url);
	if (!dbh) {
		LM_ERR("unable to open database connection\n");
		return -1;
	}
	if(skip_version_check == 0 &&
	db_check_table_version(&auth_dbf, dbh, (str*)*param,
	SUBSCRIBER_TABLE_VERSION) < 0) {
		LM_ERR("error during table version check.\n");
		auth_dbf.close(dbh);
		return -1;
	}
	auth_dbf.close(dbh);

	return 0;
}

static int fixup_check_outvar(void **param)
{
	if (((pv_spec_t*)*param)->type != PVT_AVP &&
		((pv_spec_t*)*param)->type != PVT_SCRIPTVAR) {
		LM_ERR("return must be an AVP or SCRIPT VAR!\n");
		return E_SCRIPT;
	}

	return 0;
}

mi_response_t *mi_get_auth_info_2(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	str username,domain;
	char query_buf[1024];
	int n;
	str cdb_key,out;
	mi_response_t *resp;
	mi_item_t *resp_obj;

	if (!cdb_url.s) {
		LM_ERR("No cachedb configured\n");
		return init_mi_error( 400, MI_SSTR("No Cachedb"));
	}

	if (get_mi_string_param(params, "username", &username.s, &username.len) < 0)
		return init_mi_param_error();
	if (get_mi_string_param(params, "domain", &domain.s, &domain.len) < 0)
		return init_mi_param_error();
	
	n = snprintf(query_buf,1024,"auth_%.*s",
	username.len,username.s);
	if (n<0) {
		LM_WARN("Failed to build cdb key \n");
		goto error;
	}

	cdb_key.s = query_buf;
	cdb_key.len = n;

	if (cdbf.get(cdbc, &cdb_key, &out) < 0) {
		LM_DBG("cannot retrieve key\n");
		return init_mi_error( 400, MI_SSTR("Not Found"));
	}

	resp = init_mi_result_object(&resp_obj);
	if (!resp)
		goto error;

	if (add_mi_string(resp_obj, "", 0, out.s,out.len) < 0)
		goto error;

	pkg_free(out.s);
	return resp;

error:
	return init_mi_error( 500, MI_SSTR("Internal Error"));
}

mi_response_t *mi_upsert_auth_info_4(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	str username,domain,password,attrs;
	char query_buf[1024];
	int n;
	str cdb_key,update_query_s;

	if (!cdb_url.s) {
		LM_ERR("No cachedb configured\n");
		return init_mi_error( 400, MI_SSTR("No Cachedb"));
	}

	if (get_mi_string_param(params, "username", &username.s, &username.len) < 0)
		return init_mi_param_error();
	if (get_mi_string_param(params, "domain", &domain.s, &domain.len) < 0)
		return init_mi_param_error();
	if (get_mi_string_param(params, "password", &password.s, &password.len) < 0)
		return init_mi_param_error();
	if (get_mi_string_param(params, "attrs", &attrs.s, &attrs.len) < 0)
		return init_mi_param_error();
	
	n = snprintf(query_buf,1024,"auth_%.*s",
	username.len,username.s);
	if (n<0) {
		LM_WARN("Failed to build cdb key \n");
		goto error;
	}

	cdb_key.s = query_buf;
	cdb_key.len = n;

	if (cdbf.remove(cdbc, &cdb_key) < 0) {
		LM_DBG("cannot remove key\n");
	}

	update_query_s.s = query_buf; 
	update_query_s.len = snprintf(query_buf,1024,"call %.*s('%.*s','%.*s','%.*s','%.*s')",
	reload_procedure.len,reload_procedure.s,
	username.len,username.s,
	domain.len,domain.s,
	attrs.len,attrs.s,
	password.len,password.s);

	if (update_query_s.len <= 0) {
		LM_ERR("DB query building failed\n");
		goto error;
	}

	if ( auth_dbf.raw_query(auth_db_handle, &update_query_s, 0) < 0) {
		LM_ERR("DB query failed %.*s\n",update_query_s.len,update_query_s.s);
		goto error;
	}

	return init_mi_result_ok();
error:
	return init_mi_error( 500, MI_SSTR("Internal Error"));
}

mi_response_t *mi_delete_auth_info_2(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	str username,domain;
	char query_buf[1024];
	int n;
	str cdb_key,update_query_s;

	if (!cdb_url.s) {
		LM_ERR("No cachedb configured\n");
		return init_mi_param_error();
	}

	if (get_mi_string_param(params, "username", &username.s, &username.len) < 0)
		return init_mi_param_error();
	if (get_mi_string_param(params, "domain", &domain.s, &domain.len) < 0)
		return init_mi_param_error();

	n = snprintf(query_buf,1024,"auth_%.*s",
	username.len,username.s);
	if (n<0) {
		LM_WARN("Failed to build cdb key \n");
		goto error;
	}

	cdb_key.s = query_buf;
	cdb_key.len = n;

	if (cdbf.remove(cdbc, &cdb_key) < 0) {
		LM_DBG("cannot remove key\n");
	}

	update_query_s.s = query_buf; 
	update_query_s.len = snprintf(query_buf,1024,"call %.*s('%.*s','%.*s')",
	delete_procedure.len,delete_procedure.s,
	username.len,username.s,
	domain.len,domain.s);

	if (update_query_s.len <= 0) {
		LM_ERR("DB query building failed\n");
		return init_mi_error(500, MI_SSTR("Internal Error"));
	}

	if ( auth_dbf.raw_query(auth_db_handle, &update_query_s, 0) < 0) {
		LM_ERR("DB query failed %.*s\n",update_query_s.len,update_query_s.s);
		return init_mi_error(500, MI_SSTR("Internal Error"));
	}

	return init_mi_result_ok();
error:
	return init_mi_error(500, MI_SSTR("Internal Error"));
}
