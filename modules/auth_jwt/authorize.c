/*
 * Digest Authentication - Database support
 *
 * Copyright (C) 2001-2003 FhG Fokus
 *
 * This file is part of opensips, a free SIP server.
 *
 * opensips is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version
 *
 * opensips is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * history:
 * ---------
 * 2003-02-28 scratchpad compatibility abandoned
 * 2003-01-27 next baby-step to removing ZT - PRESERVE_ZT (jiri)
 * 2004-06-06 updated to the new DB api, added auth_db_{init,bind,close,ver}
 *             (andrei)
 * 2005-05-31 general definition of AVPs in credentials now accepted - ID AVP,
 *            STRING AVP, AVP aliases (bogdan)
 * 2006-03-01 pseudo variables support for domain name (bogdan)
 * 2009-01-25 added prepared statements support in running the DB queries
 *             (bogdan)
 */


#include <string.h>
#include "../../ut.h"
#include "../../str.h"
#include "../../db/db.h"
#include "../../dprint.h"
#include "../../parser/digest/digest.h"
#include "../../parser/hf.h"
#include "../../parser/parser_f.h"
#include "../../usr_avp.h"
#include "../../mod_fix.h"
#include "../../mem/mem.h"
#include "jwt_avps.h"
#include "authjwt_mod.h"

#include "../../cachedb/cachedb.h"
#include "../../cachedb/cachedb_cap.h"

#include <jwt.h>

extern cachedb_funcs cdbf;
extern cachedb_con *cdbc;
extern int cdb_ttl;

#define RAW_QUERY_BUF_LEN	2048
#define DEC_AND_CHECK_LEN(_curr,_size)				\
	 do {							\
		if (_size < 0) {				\
			LM_ERR("Failed to build query \n");	\
			return -1;				\
		}						\
		_curr-=_size; 					\
		if (_curr < 0) { 				\
			LM_ERR("Buffer overflow  \n"); 		\
			return -1; 				\
		}						\
	} while(0)						\

int jwt_authorize(struct sip_msg* _msg, str* jwt_token, 
		pv_spec_t* decoded_jwt, pv_spec_t* auth_user)
{
	char raw_query_s[RAW_QUERY_BUF_LEN], *p, *cout=NULL;
	int n,len, i,j, validated=0;
	str raw_query,secret,username;
	struct jwt_avp *cred;
	char *jwt_token_buf = NULL,*tag_s;
	jwt_t *jwt = NULL,*jwt_dec=NULL;
	str tag;
	db_res_t *res = NULL;
	db_row_t *row;
	db_val_t *val;
	pv_value_t pv_val;
	int_str ivalue;
	str cdb_key, out;
	cJSON* cached=NULL, *aux=NULL, *secrets_array;

	jwt_token_buf = pkg_malloc(jwt_token->len + 1);
	if (!jwt_token_buf) {
		LM_ERR("No more pkg mem \n");
		goto err_out;
	}
	memcpy(jwt_token_buf,jwt_token->s,jwt_token->len);
	jwt_token_buf[jwt_token->len] = 0;

	if (( i = jwt_decode(&jwt, jwt_token_buf, NULL,0)) != 0 || jwt == NULL) {
		LM_ERR("Failed to decode jwt [%s] with out %d \n",jwt_token_buf,i);
		goto err_out;
	}

	tag_s = jwt_get_grant(jwt,(const char *)jwt_tag_claim.s);
	if (!tag_s) {
		LM_ERR("Failed to find claim %s\n",jwt_tag_claim.s);
		goto err_out;
	}

	LM_DBG("Decoded JWT and found claim %s with value %s \n",jwt_tag_claim.s,tag_s);
	
	tag.s = tag_s;
	tag.len = strlen(tag_s);

	cdb_key.len = snprintf(raw_query_s,RAW_QUERY_BUF_LEN,"jwtcache_%.*s",tag.len,tag.s);
	cdb_key.s = raw_query_s;

	if (cdbf.get(cdbc, &cdb_key, &out) < 0) {
		LM_DBG("cannot retrieve jwt cache\n");
		goto db_query;
	} else
		goto cache_query;

cache_query:
	LM_DBG("Found cache info [%.*s] \n",out.len,out.s);
	memcpy(raw_query_s,out.s,out.len);
	raw_query_s[out.len] = 0;
	cached = cJSON_Parse(raw_query_s);
	if (!cached) {
		LM_ERR("Failed to parse cached json \n");
		goto db_query;
	}
	
	secrets_array = cJSON_GetObjectItem(cached,"secrets");
	if (secrets_array == NULL || secrets_array->type != cJSON_Array) {
		LM_ERR("No secrets in cached info \n");
		cJSON_Delete(cached);
		cached=NULL;
		goto db_query;
	}

	n = cJSON_GetArraySize(secrets_array);

	LM_DBG("Found %d cached secrets \n", n);
	for (i=0;i<n;i++) {
		aux = cJSON_GetArrayItem(secrets_array,i);
		if (aux == NULL || aux->type != cJSON_String) {
			LM_ERR("Found non-string cached secret \n");
			cJSON_Delete(cached);
			cached=NULL;
			goto db_query;
		}

		secret.s = aux->valuestring;
		secret.len = strlen(secret.s);

		LM_DBG("Found secret %.*s \n",secret.len,secret.s);

		if (jwt_dec) {
			jwt_free(jwt_dec);
			jwt_dec = NULL;
		}

		if (jwt_decode(&jwt_dec, jwt_token_buf, secret.s,secret.len) != 0 || 
		jwt_dec == NULL) {
			LM_ERR("Failed to decode jwt [%s] \n",jwt_token_buf);
			LM_ERR("cached secret [%.*s] \n",secret.len,secret.s);
			continue;
		}

		LM_DBG("Succesfully decoded with cached secret \n");

		aux = cJSON_GetObjectItem(cached,username_column.s);
		if (aux == NULL || aux->type != cJSON_String) {
			LM_ERR("Failed to get auth username from cache \n");
			continue;
		}

		pv_val.flags = PV_VAL_STR;
		pv_val.rs.s =  jwt_dump_str(jwt_dec,0);
		pv_val.rs.len = strlen(pv_val.rs.s);
		if (pv_set_value(_msg,decoded_jwt,0,&pv_val) != 0) {
			LM_ERR("Failed to set decoded JWT pvar \n");
			continue;
		} 
	
		pv_val.rs.s = aux->valuestring;
		pv_val.rs.len = strlen(pv_val.rs.s);
		if (pv_set_value(_msg,auth_user,0,&pv_val) != 0) {
			LM_ERR("Failed to set decoded JWT auth user \n");
			continue;
		} 

		for (cred=jwt_credentials; cred; cred=cred->next) {
			memcpy(raw_query_s,cred->attr_name.s,cred->attr_name.len);
			raw_query_s[cred->attr_name.len] = 0;

			aux = cJSON_GetObjectItem(cached,raw_query_s);
			if (!aux) {
				LM_ERR("Failed to fetch attr %s \n",raw_query_s);
				cJSON_Delete(cached);
				pkg_free(out.s);
				goto db_query;
				
			}

			if (aux->type == cJSON_String) {
				ivalue.s.s = aux->valuestring;
				ivalue.s.len = strlen(ivalue.s.s);
				if (add_avp(cred->avp_type|AVP_VAL_STR,
				cred->avp_name,ivalue)!=0){
					LM_ERR("failed to add extra AVP\n");
					cJSON_Delete(cached);
					pkg_free(out.s);
					goto db_query;
				}
			} else {
				ivalue.n = (int)aux->valuedouble;
				if (add_avp(cred->avp_type, 
				cred->avp_name, ivalue)!=0) {
					LM_ERR("failed to add AVP\n");
					cJSON_Delete(cached);
					pkg_free(out.s);
					goto db_query;
				}
			}
		}

		validated = 1;
		break;
	}

	cJSON_Delete(cached);
	pkg_free(out.s);

	if (validated == 1) {
		LM_DBG("Fully validated from cache & loaded attributes \n");
		return 1;
	}	
	
db_query:
	raw_query.s = raw_query_s;
	p = raw_query_s;	
	len = RAW_QUERY_BUF_LEN;

	n = snprintf(p,len,"SELECT a.%.*s,b.%.*s",
	username_column.len,username_column.s,
	secret_column.len,secret_column.s);
	DEC_AND_CHECK_LEN(len, n);
	p+=n;

	for (i=0,cred=jwt_credentials; cred; i++,cred=cred->next) {
		n = snprintf(p,len,",a.%.*s",cred->attr_name.len,cred->attr_name.s);
		DEC_AND_CHECK_LEN(len,n);
		p+=n;
	}
	
	n = snprintf(p,len," from %.*s a inner join %.*s b on a.%.*s = b.%.*s  where a.%.*s=\"%.*s\" and UNIX_TIMESTAMP() >= b.%.*s and UNIX_TIMESTAMP() < b.%.*s",
	profiles_table.len,profiles_table.s,
	secrets_table.len,secrets_table.s,	
	tag_column.len,tag_column.s,
	secret_tag_column.len,secret_tag_column.s,
	tag_column.len,tag_column.s,
	tag.len,tag.s,
	start_ts_column.len,start_ts_column.s,
	end_ts_column.len,end_ts_column.s);

	DEC_AND_CHECK_LEN(len,n);
	p+=n;

	raw_query.len = (int)(p-raw_query_s);
	
	LM_DBG("built JWT raw db query [%.*s]\n",raw_query.len,raw_query.s);
	if (auth_dbf.raw_query(auth_db_handle, &raw_query, &res) < 0) {
		LM_ERR("raw_query failed\n");
		goto err_out;
	}

	if (RES_ROW_N(res) == 0) {
		LM_DBG("No matching JWT profiles for tag [%.*s]\n",tag.len,tag.s);
		goto err_out;
	}

	LM_DBG("Found %d record for tag %.*s\n",RES_ROW_N(res),tag.len,tag.s);
	for (i = 0; i < RES_ROW_N(res); i++) {


		row = RES_ROWS(res) + i;
		secret.s = (char *)VAL_STRING(ROW_VALUES(row) + 1);
		secret.len = strlen(secret.s);

		if (i==0) {
			/* first pass, init caching json */
			cached = cJSON_CreateObject();
			if (!cached) {
				LM_ERR("Failed to init jwt cache \n");
				goto err_out;
			}

			aux = cJSON_CreateStr(VAL_STRING(ROW_VALUES(row)),strlen(VAL_STRING(ROW_VALUES(row))));
			if (!aux)
				goto err_out;
			cJSON_AddItemToObject(cached,username_column.s,aux);

			for (j=2,cred=jwt_credentials; cred; j++,cred=cred->next) {
				switch (res->col.types[j]) {
					case DB_STR:
					case DB_BLOB:
						ivalue.s = VAL_STR(&(res->rows[i].values[j]));
						if (VAL_NULL(&(res->rows[i].values[j])) ||
						ivalue.s.s == NULL || ivalue.s.len==0)
							continue;

						aux = cJSON_CreateStr(ivalue.s.s,ivalue.s.len);
						if (!aux) 
							goto err_out;

						_cJSON_AddItemToObject(cached,&cred->attr_name,aux);

						break;
					case DB_STRING:
						ivalue.s.s = VAL_STRING(&(res->rows[i].values[j]));
						ivalue.s.len = strlen(ivalue.s.s);

						if (VAL_NULL(&(res->rows[i].values[j])) ||
						ivalue.s.s == NULL || ivalue.s.len==0)
							continue;

						aux = cJSON_CreateStr(ivalue.s.s,ivalue.s.len);
						if (!aux) 
							goto err_out;

						_cJSON_AddItemToObject(cached,&cred->attr_name,aux);

						break;
					case DB_INT:
						ivalue.n = (int)VAL_INT(&(res->rows[i].values[j]));
						if (VAL_NULL(&(res->rows[i].values[j])))
							continue;
						aux = cJSON_CreateNumber((double)ivalue.n);
						if (!aux) 
							goto err_out;

						_cJSON_AddItemToObject(cached,&cred->attr_name,aux);

						break;
					default:
						LM_ERR("Unsupported column type \n");
						break;
				}
			}

			secrets_array=cJSON_CreateArray();
			if (!secrets_array)
				goto err_out;
			cJSON_AddItemToObject(cached,"secrets",secrets_array);	
		}

		aux = cJSON_CreateStr(secret.s,secret.len);
		if (!aux)
			goto err_out;
		cJSON_AddItemToArray(secrets_array,aux);

		/* if validated, just iterate to cache all loaded secrets */
		if (validated)
			continue;

		if (jwt_dec) {
			jwt_free(jwt_dec);
			jwt_dec = NULL;
		}

		if (jwt_decode(&jwt_dec, jwt_token_buf, secret.s,secret.len) != 0 || 
		jwt_dec == NULL) {
			LM_ERR("Failed to decode jwt [%s] \n",jwt_token_buf);
			LM_ERR("DB secret [%.*s] \n",secret.len,secret.s);
			continue;
		}

		pv_val.flags = PV_VAL_STR;
		pv_val.rs.s =  jwt_dump_str(jwt_dec,0);
		pv_val.rs.len = strlen(pv_val.rs.s);
		if (pv_set_value(_msg,decoded_jwt,0,&pv_val) != 0) {
			LM_ERR("Failed to set decoded JWT pvar \n");
			auth_dbf.free_result(auth_db_handle, res);
			goto err_out;
		} 
	
		pv_val.rs.s = (char *)VAL_STRING(ROW_VALUES(row));
		pv_val.rs.len = strlen(pv_val.rs.s);
		if (pv_set_value(_msg,auth_user,0,&pv_val) != 0) {
			LM_ERR("Failed to set decoded JWT auth user \n");
			auth_dbf.free_result(auth_db_handle, res);
			goto err_out;
		} 

		for (j=2,cred=jwt_credentials; cred; j++,cred=cred->next) {
			switch (res->col.types[j]) {
				case DB_STR:
				case DB_BLOB:
					ivalue.s = VAL_STR(&(res->rows[i].values[j]));
					if (VAL_NULL(&(res->rows[i].values[j])) ||
					ivalue.s.s == NULL || ivalue.s.len==0)
						continue;

					if (add_avp(cred->avp_type|AVP_VAL_STR,
					cred->avp_name,ivalue)!=0){
						LM_ERR("failed to add extra AVP\n");
						auth_dbf.free_result(auth_db_handle, res);
						goto err_out;
					}

					break;
				case DB_STRING:
					ivalue.s.s = VAL_STRING(&(res->rows[i].values[j]));
					ivalue.s.len = strlen(ivalue.s.s);

					if (VAL_NULL(&(res->rows[i].values[j])) ||
					ivalue.s.s == NULL || ivalue.s.len==0)
						continue;

					if (add_avp(cred->avp_type|AVP_VAL_STR,
					cred->avp_name,ivalue)!=0){
						LM_ERR("failed to add extra AVP\n");
						auth_dbf.free_result(auth_db_handle, res);
						goto err_out;
					}

					break;
				case DB_INT:
					ivalue.n = (int)VAL_INT(&(res->rows[i].values[j]));
					if (VAL_NULL(&(res->rows[i].values[j])))
						continue;
					if (add_avp(cred->avp_type, 
					cred->avp_name, ivalue)!=0) {
						LM_ERR("failed to add AVP\n");
						auth_dbf.free_result(auth_db_handle, res);
						goto err_out;
					}
					break;
				default:
					LM_ERR("Unsupported column type \n");
					break;
			}
		}

		LM_INFO("Validated jwt %s with DB key %.*s\n",jwt_dump_str(jwt_dec,0),secret.len,secret.s);
		validated=1;
	}

	auth_dbf.free_result(auth_db_handle, res);

	cout = cJSON_Print(cached);	
	cJSON_Minify(cout);

	out.s = cout;
	out.len = strlen(cout);

	cdb_key.len = snprintf(raw_query_s,RAW_QUERY_BUF_LEN,"jwtcache_%.*s",tag.len,tag.s);
	cdb_key.s = raw_query_s;

	LM_DBG("Saving [%.*s] with val [%.*s] \n",cdb_key.len,cdb_key.s,out.len,out.s);

	if (cdbf.set(cdbc,&cdb_key,&out,cdb_ttl) < 0)
		LM_ERR("Failed to store jwt cache \n");

err_out:
	if (jwt_token_buf)
		pkg_free(jwt_token_buf);
	if (jwt)
		jwt_free(jwt);
	if (jwt_dec)
		jwt_free(jwt_dec);
	if (cached)
		cJSON_Delete(cached);
	if (cout)
		pkg_free(cout);

	if (validated)
		return 1;

	return -1;
}
