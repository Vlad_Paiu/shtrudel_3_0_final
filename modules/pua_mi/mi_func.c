/*
 * pua_mi module - MI pua module
 *
 * Copyright (C) 2006 Voice Sistem S.R.L.
 *
 * This file is part of opensips, a free SIP server.
 *
 * opensips is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version
 *
 * opensips is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <libxml/parser.h>

#include "../../parser/parse_expires.h"
#include "../../parser/parse_uri.h"
#include  "../../mem/mem.h"
#include "../../mi/mi.h"
#include "../../ut.h"
#include "../../cachedb/cachedb.h"
#include "../../db/db.h"

#include "../pua/pua_bind.h"
#include "../pua/pua.h"
#include "pua_mi.h"
#include "mi_func.h"

extern db_func_t puamidbf;
extern db_con_t *puamidbc;

extern cachedb_funcs puamicdbf;
extern cachedb_con *puamicdbc;

/*
 * mi cmd: pua_publish
 *		<presentity_uri>
 *		<expires>
 *		<event package>
 *		<content_type>     - body type if body of a type different from default
 *                            event content-type or .
 *		<ETag>             - ETag that publish should match or . if no ETag
 *		<extra_headers>    - extra headers to be added to the request or .
 *		<publish_body>     - may not be present in case of update for expire
 */

mi_response_t *mi_pua_publish(const mi_params_t *params,
					struct mi_handler *async_hdl, str *etag, str *extra_headers,
					str *content_type, str *body)
{
	int exp;
	str pres_uri;
	struct sip_uri uri;
	publ_info_t publ;
	str event;
	int result;

	LM_DBG("start\n");

	if (get_mi_string_param(params, "presentity_uri",
		&pres_uri.s, &pres_uri.len) < 0)
		return init_mi_param_error();

	if(pres_uri.s == NULL || pres_uri.s== 0)
	{
		LM_ERR("empty uri\n");
		return init_mi_error(404, MI_SSTR("Empty presentity URI"));
	}
	if(parse_uri(pres_uri.s, pres_uri.len, &uri)<0 )
	{
		LM_ERR("bad uri\n");
		return init_mi_error(404, MI_SSTR("Bad presentity URI"));
	}
	LM_DBG("pres_uri '%.*s'\n", pres_uri.len, pres_uri.s);

	if (get_mi_int_param(params, "expires", &exp) < 0)
		return init_mi_param_error();

	LM_DBG("expires '%d'\n", exp);

	if (get_mi_string_param(params, "event_package", &event.s, &event.len) < 0)
		return init_mi_param_error();

	if(event.s== NULL || event.len== 0)
	{
		LM_ERR("empty event parameter\n");
		return init_mi_error(400, MI_SSTR("Empty event parameter"));
	}
	LM_DBG("event '%.*s'\n",
	    event.len, event.s);

	/* Create the publ_info_t structure */
	memset(&publ, 0, sizeof(publ_info_t));

	publ.pres_uri= &pres_uri;
	if(body)
	{
		publ.body= body;
	}

	publ.event= get_event_flag(&event);
	if(publ.event< 0)
	{
		LM_ERR("unknown event\n");
		return init_mi_error(400, MI_SSTR("Unknown event"));
	}
	if(content_type)
	{
		publ.content_type= *content_type;
	}

	if(etag)
	{
		publ.etag= etag;
	}
	publ.expires= exp;

	if (extra_headers) {
	    publ.extra_headers = extra_headers;
	}

	if (async_hdl)
	{
		publ.source_flag= MI_ASYN_PUBLISH;
		publ.cb_param= (void*)async_hdl;
	}
	else
		publ.source_flag|= MI_PUBLISH;

	publ.outbound_proxy = presence_server;

	result= pua_send_publish(&publ);

	if(result< 0)
	{
		LM_ERR("sending publish failed\n");
		return init_mi_error(500, MI_SSTR("MI/PUBLISH failed"));
	}
	if(result== 418)
		return init_mi_error(418, MI_SSTR("Wrong ETag"));

	if (async_hdl==NULL)
			return init_mi_result_string(MI_SSTR("Accepted"));
	else
			return MI_ASYNC_RPL;
}

mi_response_t *get_ctype_body_params(const mi_params_t *params,
										str *content_type, str *body)
{
	if (get_mi_string_param(params, "content_type",
		&content_type->s, &content_type->len) < 0)
		return init_mi_param_error();

	if(content_type->s== NULL || content_type->len== 0)
	{
		LM_ERR("empty content type\n");
		return init_mi_error(400, MI_SSTR("Empty content type parameter"));
	}
	LM_DBG("content type '%.*s'\n",
	    content_type->len, content_type->s);

	if (get_mi_string_param(params, "body", &body->s, &body->len) < 0)
		return init_mi_param_error();

	if(body->s == NULL || body->s== 0)
	{
		LM_ERR("empty body parameter\n");
		return init_mi_error(400, MI_SSTR("Empty body parameter"));
	}
	LM_DBG("body '%.*s'\n", body->len, body->s);

	return NULL;
}

mi_response_t *get_etag_param(const mi_params_t *params, str *etag)
{
	if (get_mi_string_param(params, "etag", &etag->s, &etag->len) < 0)
		return init_mi_param_error();

	if(etag->s== NULL || etag->len== 0)
	{
		LM_ERR("empty etag parameter\n");
		return init_mi_error(400, MI_SSTR("Empty etag parameter"));
	}
	LM_DBG("etag '%.*s'\n", etag->len, etag->s);

	return NULL;
}

mi_response_t *get_direction_param(const mi_params_t *params, str *dir)
{
	if (get_mi_string_param(params, "direction", &dir->s, &dir->len) < 0)
		return init_mi_param_error();

	if(dir->s== NULL || dir->len== 0)
	{
		dir->s = "initiatior";
		dir->len = 9;
	}

	LM_DBG("direction '%.*s'\n", dir->len, dir->s);

	return NULL;
}

mi_response_t *get_extra_hdrs_param(const mi_params_t *params, str *extra_hdrs)
{
	if (get_mi_string_param(params, "extra_headers",
		&extra_hdrs->s, &extra_hdrs->len) < 0)
		return init_mi_param_error();

	if(extra_hdrs->s== NULL || extra_hdrs->len== 0)
	{
		LM_ERR("empty extra_headers parameter\n");
		return init_mi_error(400, MI_SSTR("Empty extra_headers"));
	}
	LM_DBG("extra_headers '%.*s'\n",
	    extra_hdrs->len, extra_hdrs->s);

	return NULL;
}

mi_response_t *mi_pua_publish_1(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	return mi_pua_publish(params, async_hdl, NULL,NULL,NULL,NULL);
}

mi_response_t *mi_pua_publish_2(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	str etag;
	mi_response_t *err;

	if ((err = get_etag_param(params, &etag)) != NULL)
		return err;

	return mi_pua_publish(params, async_hdl, &etag, NULL,NULL,NULL);
}

mi_response_t *mi_pua_publish_3(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	str extra_hdrs;
	mi_response_t *err;

	if ((err = get_extra_hdrs_param(params, &extra_hdrs)) != NULL)
		return err;

	return mi_pua_publish(params, async_hdl, NULL, &extra_hdrs,NULL,NULL);
}

mi_response_t *mi_pua_publish_4(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	str content_type, body;
	mi_response_t *err;

	if ((err = get_ctype_body_params(params, &content_type, &body)) != NULL)
		return err;

	return mi_pua_publish(params, async_hdl, NULL,NULL, &content_type, &body);
}

mi_response_t *mi_pua_publish_5(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	str etag, extra_hdrs;
	mi_response_t *err;

	if ((err = get_etag_param(params, &etag)) != NULL)
		return err;
	if ((err = get_extra_hdrs_param(params, &extra_hdrs)) != NULL)
		return err;

	return mi_pua_publish(params, async_hdl, &etag, &extra_hdrs,NULL,NULL);
}

mi_response_t *mi_pua_publish_6(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	str etag, content_type, body;
	mi_response_t *err;

	if ((err = get_etag_param(params, &etag)) != NULL)
		return err;
	if ((err = get_ctype_body_params(params, &content_type, &body)) != NULL)
		return err;	

	return mi_pua_publish(params, async_hdl, &etag, NULL, &content_type, &body);
}

mi_response_t *mi_pua_publish_7(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	str extra_hdrs, content_type, body;
	mi_response_t *err;

	if ((err = get_extra_hdrs_param(params, &extra_hdrs)) != NULL)
		return err;
	if ((err = get_ctype_body_params(params, &content_type, &body)) != NULL)
		return err;	

	return mi_pua_publish(params, async_hdl, NULL, &extra_hdrs, &content_type, &body);
}

mi_response_t *mi_pua_publish_8(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	str etag, extra_hdrs, content_type, body;
	mi_response_t *err;

	if ((err = get_etag_param(params, &etag)) != NULL)
		return err;
	if ((err = get_extra_hdrs_param(params, &extra_hdrs)) != NULL)
		return err;
	if ((err = get_ctype_body_params(params, &content_type, &body)) != NULL)
		return err;	

	return mi_pua_publish(params, async_hdl, &etag, &extra_hdrs, &content_type, &body);
}

int mi_publ_rpl_cback( ua_pres_t* hentity, struct sip_msg* reply)
{
	mi_response_t *resp;
	mi_item_t *resp_obj;
	struct mi_handler* mi_hdl= NULL;
	struct hdr_field* hdr= NULL;
	int statuscode;
	int lexpire;
	str etag;
	str reason= {0, 0};

	if(reply== NULL || hentity== NULL)
	{
		LM_ERR("NULL parameter\n");
		return -1;
	}
	if(hentity->cb_param== NULL)
	{
		LM_DBG("NULL callback parameter, probably a refresh\n");
		return -1;
	}
	if(reply== FAKED_REPLY)
	{
		statuscode= 408;
		reason.s= "Request Timeout";
		reason.len= strlen(reason.s);
	}
	else
	{
		statuscode= reply->first_line.u.reply.statuscode;
		reason= reply->first_line.u.reply.reason;
	}

	mi_hdl = (struct mi_handler *)(hentity->cb_param);

	resp = init_mi_result_object(&resp_obj);
	if (!resp)
		goto done;

	if (add_mi_string_fmt(resp_obj, MI_SSTR("reply"), "%d %.*s",
		statuscode, reason.len, reason.s) < 0)
		goto error;

	if(statuscode== 200)
	{
		/* extract ETag and expires */
		lexpire = ((exp_body_t*)reply->expires->parsed)->val;
		LM_DBG("lexpire= %d\n", lexpire);

		hdr = get_header_by_static_name( reply, "SIP-ETag");
		if( hdr==NULL ) /* must find SIP-Etag header field in 200 OK msg*/
		{
			LM_ERR("SIP-ETag header field not found\n");
			goto error;
		}
		etag= hdr->body;

		if (add_mi_string(resp_obj, MI_SSTR("ETag"), etag.s, etag.len) < 0)
			goto error;

		if (add_mi_number(resp_obj, MI_SSTR("Expires"), lexpire) < 0)
			goto error;
	}

done:
	if ( statuscode >= 200)
	{
		mi_hdl->handler_f( resp, mi_hdl, 1);
	}
	else
	{
		mi_hdl->handler_f( resp, mi_hdl, 0 );
	}
	hentity->cb_param = 0;
	return 0;

error:
	return  -1;
}


/*Command parameters:
 * pua_subscribe
 *		<presentity_uri>
 *		<watcher_uri>
 *		<event_package>
 *		<expires>
 * */


mi_response_t *mi_pua_subscribe(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	int exp= 0;
	str pres_uri, watcher_uri;
	struct sip_uri uri;
	subs_info_t subs;
	str event;

	if (get_mi_string_param(params, "presentity_uri",
		&pres_uri.s, &pres_uri.len) < 0)
		return init_mi_param_error();

	if(pres_uri.s == NULL || pres_uri.s== 0)
	{
		return init_mi_error(400, MI_SSTR("Bad uri"));
	}
	if(parse_uri(pres_uri.s, pres_uri.len, &uri)<0 )
	{
		LM_ERR("bad uri\n");
		return init_mi_error(400, MI_SSTR("Bad uri"));
	}

	if (get_mi_string_param(params, "watcher_uri",
		&watcher_uri.s, &watcher_uri.len) < 0)
		return init_mi_param_error();

	if(watcher_uri.s == NULL || watcher_uri.s== 0)
	{
		return init_mi_error(400, MI_SSTR("Bad uri"));
	}
	if(parse_uri(watcher_uri.s, watcher_uri.len, &uri)<0 )
	{
		LM_ERR("bad uri\n");
		return init_mi_error(400, MI_SSTR("Bad uri"));
	}

	if (get_mi_string_param(params, "event",
		&event.s, &event.len) < 0)
		return init_mi_param_error();

	if(event.s== NULL || event.len== 0)
	{
		LM_ERR("empty event parameter\n");
		return init_mi_error(400, MI_SSTR("Empty event parameter"));
	}
	LM_DBG("event '%.*s'\n", event.len, event.s);

	if (get_mi_int_param(params, "expires", &exp) < 0)
		goto error;

	LM_DBG("expires '%d'\n", exp);

	memset(&subs, 0, sizeof(subs_info_t));

	subs.pres_uri= &pres_uri;

	subs.watcher_uri= &watcher_uri;

	subs.contact= &watcher_uri;

	subs.expires= exp;
	subs.source_flag |= MI_SUBSCRIBE;
	subs.event= get_event_flag(&event);
	if(subs.event< 0)
	{
		LM_ERR("unknown event\n");
		return init_mi_error(400, MI_SSTR("Unknown event"));
	}

	if(pua_send_subscribe(&subs)< 0)
	{
		LM_ERR("while sending subscribe\n");
		goto error;
	}

	return init_mi_result_string(MI_SSTR("accepted"));

error:

	return 0;

}

static int get_presentity_etag(str *ext,str *out_etag)
{
	int cdb_ttl=7200,n;
	char query_buf[1024];
	char *dup;
	str query,cdb_key;
	db_res_t* db_res = NULL;

	if (!out_etag)
		return -1;

	/* build query */

	memset(query_buf,0,1024);
	n = snprintf(query_buf,1024,"SELECT etag from opensips.presentity where username='%.*s' and event='dialog' order by received_time DESC LIMIT 0,1",ext->len,ext->s);

	if (n < 0) {
		LM_ERR("Internal error for ext %.*s\n",ext->len,ext->s);
		return -1;
	}

	query.s = query_buf;
	query.len = n;

	/* check DB layer */

	if(puamidbf.raw_query( puamidbc, &query, &db_res)!=0) {
		LM_ERR("Failed to run DB query <%.*s> \n",query.len,query.s);
		goto check_cachedb; 
	}

	if(db_res==NULL || RES_ROW_N(db_res)<=0 || RES_COL_N(db_res)<=0 || RES_ROWS(db_res)[0].values[0].nul) {
		LM_DBG("no result after query\n");
		puamidbf.free_result( puamidbc, db_res );
		goto check_cachedb;
	}

	switch (RES_ROWS(db_res)[0].values[0].type) {
		case DB_STRING:
			dup = (char*)RES_ROWS(db_res)[0].values[0].val.string_val;
			out_etag->len = strlen(dup);
			break;
		case DB_STR:
			dup = (char*)RES_ROWS(db_res)[0].values[0].val.str_val.s;
			out_etag->len = RES_ROWS(db_res)[0].values[0].val.str_val.len;
			break;
		default:
			LM_ERR("Unexpected type for query result %d \n",RES_ROWS(db_res)[0].values[0].type);	
			goto check_cachedb;
	}

	out_etag->s = pkg_malloc(out_etag->len);
	if (out_etag->s == NULL) {
		LM_ERR("No more pkg \n");
		puamidbf.free_result( puamidbc, db_res );
		return -1;
	}

	memcpy(out_etag->s,dup,out_etag->len);
	puamidbf.free_result( puamidbc, db_res );

	/* found & returned ! :D cache extension at cacheDB level */

	n = snprintf(query_buf,1024,"puamiext_%.*s",ext->len,ext->s);
	if (n<0) {
		LM_WARN("Failed to build cdb key \n");
		return 0;
	}

	cdb_key.s = query_buf;
	cdb_key.len = n;

	if (puamicdbf.set(puamicdbc,&cdb_key,out_etag,cdb_ttl) < 0) {
		LM_WARN("Failed to set cdb key\n");
		return 0;
	}
	
	return 0;

check_cachedb:
	n = snprintf(query_buf,1024,"puamiext_%.*s",ext->len,ext->s);
	if (n<0) {
		LM_WARN("Failed to build cdb key \n");
		return 0;
	}

	cdb_key.s = query_buf;
	cdb_key.len = n;

	/* check cacheDB level, DB expired :( */
	if (puamicdbf.get(puamicdbc, &cdb_key, out_etag) < 0) {
		LM_DBG("cannot retrieve key\n");
		return -1;
	}

	/* already pkg alloc, good */	
	return 0;
}

mi_response_t *mi_custom_pua_publish(const mi_params_t *params,
					struct mi_handler *async_hdl, str *dir)
{
	str extension,state,etag,pres_uri,expires,body,direction;
	char temp_buff[4096];
	int len, sign= 1,exp,result;
	struct sip_uri uri;
	publ_info_t publ;
	str event=str_init("dialog");
	str content_type = str_init("application/dialog-info+xml");
	str extra_headers = str_init(".");

	LM_DBG("start\n");

	if (get_mi_string_param(params, "presentity_uri",
		&pres_uri.s, &pres_uri.len) < 0)
		return init_mi_param_error();

	if(pres_uri.s == NULL || pres_uri.len== 0) {
		LM_ERR("empty uri\n");
		return init_mi_error(404, MI_SSTR("Empty presentity URI"));
	}

	if (get_mi_string_param(params, "state",
		&state.s, &state.len) < 0)
		return init_mi_param_error();

	if(state.s == NULL || state.len== 0) {
		LM_ERR("empty state\n");
		return init_mi_error(404, MI_SSTR("Empty state"));
	}

	if (get_mi_string_param(params, "expires",
		&expires.s, &expires.len) < 0)
		return init_mi_param_error();

	if(expires.s == NULL || expires.len== 0) {
		LM_ERR("empty expires\n");
		return init_mi_error(404, MI_SSTR("Empty expires"));
	}

	if (dir == NULL) {
		direction.s = "initiator";
		direction.len = 9;
	} else {
		direction = *dir;
	}

	/* Get presentity URI */
	if(parse_uri(pres_uri.s, pres_uri.len, &uri)<0 ) {
		LM_ERR("bad uri\n");
		return init_mi_error(404, "Bad presentity URI", 18);
	}

	LM_DBG("pres_uri '%.*s'\n", pres_uri.len, pres_uri.s);
	extension = uri.user;

	if (memcmp(state.s,"early",5) != 0) {
		if (get_presentity_etag(&extension,&etag) != 0) {
			LM_ERR("Failed to get etag \n");
			return init_mi_error( 500, MI_SSTR("Internal Error"));
		}
	} else {
		etag.s=".";
		etag.len=1;
	}


	if(expires.s[0]== '-')
	{
		sign= -1;
		expires.s++;
		expires.len--;
	}
	if( str2int(&expires, (unsigned int*) &exp)< 0)
	{
		LM_ERR("invalid expires parameter\n" );
		return init_mi_error( 500, MI_SSTR("Internal Error"));
	}

	exp= exp* sign;
	LM_DBG("expires '%d'\n", exp);

	/* Get event */
	LM_DBG("event '%.*s'\n",
	    event.len, event.s);

	/* Get content type */
	LM_DBG("content type '%.*s'\n",content_type.len, content_type.s);

	LM_DBG("etag '%.*s'\n", etag.len, etag.s);

	/* Get extra_headers */

	LM_DBG("extra_headers '%.*s'\n",extra_headers.len, extra_headers.s);

       if (direction.s[0] == 'i')      
               len = snprintf(temp_buff,4096,"<?xml version=\"1.0\"?><dialog-info xmlns=\"urn:ietf:params:xml:ns:dialog-info\" state=\"partial\" entity=\"sip:%.*s@c7594.ratetel.com\"><dialog id=\"938e7669abdb454384d2fb486c7e92bb\" call-id=\"938e7669abdb454384d2fb486c7e92bb\" direction=\"%.*s\"><state>%.*s</state><remote><identity>sip:alex6@c7594.ratetel.com</identity><target uri=\"sip:alex6@c7594.ratetel.com\"/></remote><local><identity>sip:%.*s@c7594.ratetel.com</identity><target uri=\"sip:%.*s@c7594.ratetel.com\"/></local></dialog></dialog-info>",extension.len,extension.s,direction.len,direction.s,state.len,state.s,extension.len,extension.s,extension.len,extension.s);
       else
               len = snprintf(temp_buff,4096,"<?xml version=\"1.0\"?><dialog-info xmlns=\"urn:ietf:params:xml:ns:dialog-info\" state=\"partial\" entity=\"sip:%.*s@c7594.ratetel.com\"><dialog id=\"938e7669abdb454384d2fb486c7e92bb\" call-id=\"938e7669abdb454384d2fb486c7e92bb\" local-tag=\"ad1e6611\" remote-tag=\"a7r3U3XHX5cac\" direction=\"%.*s\"><state>%.*s</state><remote><identity>sip:alex6@c7594.ratetel.com</identity><target uri=\"sip:alex6@c7594.ratetel.com\"/></remote><local><identity>sip:%.*s@c7594.ratetel.com</identity><target uri=\"sip:%.*s@c7594.ratetel.com\"/></local></dialog></dialog-info>",extension.len,extension.s,direction.len,direction.s,state.len,state.s,extension.len,extension.s,extension.len,extension.s);

	if (len < 0) {
		LM_ERR("Failed to print body \n");
		return init_mi_error( 500, MI_SSTR("Internal Error"));
	}

	body.s = pkg_malloc(len);
	if (!body.s) {
		LM_ERR("Failed to allocate body \n");
		return init_mi_error( 500, MI_SSTR("Internal Error"));
	}

	memcpy(body.s,temp_buff,len);
	body.len = len;

	LM_DBG("body '%.*s'\n", body.len, body.s);

	/* Create the publ_info_t structure */
	memset(&publ, 0, sizeof(publ_info_t));

	publ.pres_uri= &pres_uri;
	if(body.s)
	{
		publ.body= &body;
	}

	publ.event= get_event_flag(&event);
	if(publ.event< 0)
	{
		LM_ERR("unknown event\n");
		return init_mi_error(400, "Unknown event", 13);
	}
	if(content_type.len!= 1)
	{
		publ.content_type= content_type;
	}

	if(! (etag.len== 1 && etag.s[0]== '.'))
	{
		publ.etag= &etag;
	}
	publ.expires= exp;

	if (!(extra_headers.len == 1 && extra_headers.s[0] == '.')) {
	    publ.extra_headers = &extra_headers;
	}

	if (async_hdl!=NULL)
	{
		publ.source_flag= MI_ASYN_PUBLISH;
		publ.cb_param= (void *)async_hdl;
	}
	else
		publ.source_flag|= MI_PUBLISH;

	publ.outbound_proxy = presence_server;

	result= pua_send_publish(&publ);

	if(result< 0)
	{
		LM_ERR("sending publish failed\n");
		return init_mi_error(500, "MI/PUBLISH failed", 17);
	}
	if(result== 418)
		return init_mi_error(418, "Wrong ETag", 10);

	if (async_hdl==NULL)
		return init_mi_result_string(MI_SSTR("Accepted"));
	else
		return MI_ASYNC_RPL;
}

mi_response_t *mi_custom_pua_publish_1(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	return mi_custom_pua_publish(params, async_hdl,NULL);
}

mi_response_t *mi_custom_pua_publish_2(const mi_params_t *params,
								struct mi_handler *async_hdl)
{
	str direction;
	mi_response_t *err;

	if ((err = get_direction_param(params, &direction)) != NULL)
		return err;

	return mi_custom_pua_publish(params, async_hdl,&direction);
}
