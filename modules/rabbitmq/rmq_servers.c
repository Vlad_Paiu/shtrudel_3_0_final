/*
 * Copyright (C) 2017 OpenSIPS Project
 *
 * This file is part of opensips, a free SIP server.
 *
 * opensips is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * opensips is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 *
 * History:
 * ---------
 *  2017-01-24  created (razvanc)
 */

#include "../../mem/shm_mem.h"
#include "../../sr_module.h"
#include "../../lib/list.h"
#include "../../lib/cJSON.h"
#include "../../mod_fix.h"
#include "../../dprint.h"
#include "../../ut.h"
#include "../../pt.h"

#include "rmq_servers.h"

#include <stdlib.h>
#include <amqp_framing.h>

#include <netinet/tcp.h>

extern proc_export_t *mi_procs;

#if defined AMQP_VERSION && AMQP_VERSION >= 0x00040000
#define rmq_parse amqp_parse_url
#else
#include "../../db/db_id.h"
#warning "You are using an old, unsupported RabbitMQ library version - compile on your own risk!"
/* ugly hack to move ptr from id to rmq_uri */
#define PTR_MOVE(_from, _to) \
	do { \
		(_to) = (_from); \
		(_from) = NULL; \
	} while(0)
static inline int rmq_parse(char *url, rmq_uri *uri)
{
	str surl;
	struct db_id *id;

	surl.s = url;
	surl.len = strlen(url);

	if ((id = new_db_id(&surl)) == NULL)
		return -1;

	if (strcmp(id->scheme, "amqps") == 0)
		uri->ssl = 1;

	/* there might me a pkg leak compared to the newer version, but parsing
	 * only happends at startup, so we should not worry about this now */
	if (id->username)
		PTR_MOVE(id->username, uri->user);
	else
		uri->user = "guest";
	if (id->password)
		PTR_MOVE(id->password, uri->password);
	else
		uri->password = "guest";
	if (id->host)
		PTR_MOVE(id->host, uri->host);
	else
		uri->host = "localhost";
	if (id->database)
		PTR_MOVE(id->database, uri->vhost);
	else
		uri->vhost = "/";
	if (id->port)
		uri->port = id->port;
	else if (uri->ssl)
		uri->port = 5671;
	else
		uri->port = 5672;
	free_db_id(id);
	return 0;
}
#endif

static OSIPS_LIST_HEAD(rmq_servers);

enum rmq_func_param_type { RMQT_SERVER, RMQT_PVAR };
struct rmq_func_param {
	enum rmq_func_param_type type;
	void *value;
};

/* function that checks for error */
static int rmq_error(char const *context, amqp_rpc_reply_t x)
{
	amqp_connection_close_t *mconn;
	amqp_channel_close_t *mchan;
#ifndef AMQP_VERSION_v04
	char *errorstr;
#endif

	switch (x.reply_type) {
		case AMQP_RESPONSE_NORMAL:
			return 0;

		case AMQP_RESPONSE_NONE:
			LM_ERR("%s: missing RPC reply type!\n", context);
			break;

		case AMQP_RESPONSE_LIBRARY_EXCEPTION:
#ifndef AMQP_VERSION_v04
			errorstr = amqp_error_string(x.library_error);
			LM_ERR("%s: %s\n", context, errorstr);
			free(errorstr);
#else
			LM_ERR("%s: %s\n", context, amqp_error_string2(x.library_error));
#endif
			break;

		case AMQP_RESPONSE_SERVER_EXCEPTION:
			switch (x.reply.id) {
				case AMQP_CONNECTION_CLOSE_METHOD:
					mconn = (amqp_connection_close_t *)x.reply.decoded;
					LM_ERR("%s: server connection error %d, message: %.*s\n",
							context, mconn->reply_code,
							(int)mconn->reply_text.len,
							(char *)mconn->reply_text.bytes);
					break;
				case AMQP_CHANNEL_CLOSE_METHOD:
						mchan = (amqp_channel_close_t *)x.reply.decoded;
					LM_ERR("%s: server channel error %d, message: %.*s\n",
							context, mchan->reply_code,
							(int)mchan->reply_text.len,
							(char *)mchan->reply_text.bytes);
					break;
				default:
					LM_ERR("%s: unknown server error, method id 0x%08X\n",
							context, x.reply.id);
					break;
			}
			break;
	}
	return -1;
}

/* function used to get a rmq_server based on a cid */
struct rmq_server *rmq_get_server(str *cid)
{
	struct list_head *it;
	struct rmq_server *srv;

	list_for_each(it, &rmq_servers) {
		srv = container_of(it, struct rmq_server, list);
		if (srv->cid.len == cid->len && memcmp(srv->cid.s, cid->s, cid->len) == 0)
			return srv;
	}
	return NULL;
}

static void rmq_close_server(struct rmq_server *srv)
{
	switch (srv->state) {
	case RMQS_ON:
	case RMQS_CONN:
		rmq_error("closing channel",
				amqp_channel_close(srv->conn, 1, AMQP_REPLY_SUCCESS));
	case RMQS_INIT:
		rmq_error("closing connection",
				amqp_connection_close(srv->conn, AMQP_REPLY_SUCCESS));
		if (amqp_destroy_connection(srv->conn) < 0)
			LM_ERR("cannot destroy connection\n");
	case RMQS_OFF:
		break;
	default:
		LM_WARN("Unknown rmq server state %d\n", srv->state);
	}
	srv->state = RMQS_OFF;
}

#if 0
static void rmq_destroy_server(struct rmq_server *srv)
{
	rmq_close_server(srv);
	if (srv->exchange.bytes)
		amqp_bytes_free(srv->exchange);
	pkg_free(srv);
}
#endif

/*
 * function used to reconnect a RabbitMQ server
 */
int rmq_reconnect(struct rmq_server *srv)
{
#if defined AMQP_VERSION_v04
	amqp_socket_t *amqp_sock;
#endif
	int socket;
	struct timeval tv;
	
	tv.tv_sec = (int)(srv->ms_timeout / 1000);
	tv.tv_usec = (int)(1000 * (srv->ms_timeout % 1000));

	LM_DBG("XXX - timeout is %d seconds and %d microseconds \n",(int)(tv.tv_sec),(int)(tv.tv_usec));

	switch (srv->state) {
	case RMQS_OFF:
		srv->conn = amqp_new_connection();
		if (!srv->conn) {
			LM_ERR("cannot create amqp connection!\n");
			return -1;
		}
#if defined AMQP_VERSION_v04
		amqp_sock = amqp_tcp_socket_new(srv->conn);
		if (!amqp_sock) {
			LM_ERR("cannot create AMQP socket\n");
			goto clean_rmq_conn;
		}
		socket = amqp_socket_open_noblock(amqp_sock, srv->uri.host, srv->uri.port,&tv);
		if (socket < 0) {
			LM_ERR("cannot open AMQP socket\n");
			goto clean_rmq_conn;
		}

		amqp_set_handshake_timeout(srv->conn,&tv);
		amqp_set_rpc_timeout(srv->conn,&tv);
#else
		socket = amqp_open_socket(srv->uri.host, srv->uri.port,&tv);
		if (socket < 0) {
			LM_ERR("cannot open AMQP socket\n");
			goto clean_rmq_conn;
		}

		/* TODO - not tested here - no support from library */
		optval = srv->ms_timeout;
                if (setsockopt(socket,SOL_TCP,TCP_USER_TIMEOUT,&optval,sizeof(optval))<0){
                        LM_WARN("setsockopt failed to enable TCP_USER_TIMEOUT: %s\n",
                                strerror(errno));
                }
		amqp_set_sockfd(srv->conn, socket);
#endif
		srv->state = RMQS_INIT;
	case RMQS_INIT:
		if (rmq_error("Logging in", amqp_login(
				srv->conn,
				(srv->uri.vhost ? srv->uri.vhost: "/"),
				0,
				srv->max_frames,
				srv->heartbeat,
				AMQP_SASL_METHOD_PLAIN,
				srv->uri.user,
				srv->uri.password)))
			goto clean_rmq_server;
		/* all good - return success */
		srv->state = RMQS_CONN;
	case RMQS_CONN:
		/* don't use more than 1 channel */
		amqp_channel_open(srv->conn, 1);
		if (rmq_error("Opening channel", amqp_get_rpc_reply(srv->conn)))
			goto clean_rmq_server;
		LM_DBG("[%.*s] successfully connected!\n", srv->cid.len, srv->cid.s);
		srv->state = RMQS_ON;
	case RMQS_ON:
		return 0;
	default:
		LM_WARN("Unknown rmq server state %d\n", srv->state);
		return -1;
	}
clean_rmq_server:
	rmq_close_server(srv);
	return -2;
clean_rmq_conn:
	if (amqp_destroy_connection(srv->conn) < 0)
		LM_ERR("cannot destroy connection\n");
	return -1;
}

#define IS_WS(_c) ((_c) == ' ' || (_c) == '\t' || (_c) == '\r' || (_c) == '\n')

/*
 * function used to add a RabbitMQ server
 */
int rmq_server_add(modparam_t type, void * val)
{
	struct rmq_server *srv;
	str param, s, cid;
	str suri = {0, 0};
	char uri_pending = 0;
	unsigned flags = 0;
	char *uri;
	int retries,timeout=0;
	int max_frames = RMQ_DEFAULT_FRAMES;
	int heartbeat = RMQ_DEFAULT_HEARTBEAT;
	str exchange = {0, 0};
	str rcvq = {0, 0};
	str topic = {0, 0};
	enum rmq_parse_param { RMQP_NONE, RMQP_URI, RMQP_FRAME, RMQP_HBEAT, RMQP_IMM,
		RMQP_MAND, RMQP_EXCH, RMQP_RETRY, RMQP_TIMEOUT, RMQP_NOPER, RMQP_RCVQ, RMQP_TOPIC } state;

	if (type != STR_PARAM) {
		LM_ERR("invalid parameter type %d\n", type);
		return -1;
	}
	s.s = (char *)val;
	s.len = strlen(s.s);

	for (; s.len > 0; s.s++, s.len--)
		if (!IS_WS(*s.s))
			break;
	if (s.len <= 0 || *s.s != '[') {
		LM_ERR("cannot find connection id start: %.*s\n", s.len, s.s);
		return -1;
	}
	cid.s = s.s + 1;
	for (s.s++, s.len--; s.len > 0; s.s++, s.len--)
		if (*s.s == ']')
			break;
	if (s.len <= 0 || *s.s != ']') {
		LM_ERR("cannot find connection id end: %.*s\n", s.len, s.s);
		return -1;
	}
	cid.len = s.s - cid.s;

	/* check if the server was already defined */
	if (rmq_get_server(&cid)) {
		LM_ERR("Connection ID %.*s already defined! Please use different "
				"names for different connections!\n", cid.len, cid.s);
		return -1;
	}
#define IF_IS_PARAM(_p, _s, _l) \
	do { \
		if (s.len >= (sizeof(_p) - 1) && strncasecmp(s.s, (_p), sizeof(_p) - 1) == 0) { \
			LM_DBG("[%.*s] found parameter %s\n", cid.len, cid.s, (_p)); \
			s.s += (sizeof(_p) - 1); \
			s.len -= (sizeof(_p) - 1); \
			state = _s; \
			goto _l; \
		} \
	} while (0)

	/* server not found - parse this one */
	for (s.s++, s.len--; s.len > 0; s.s++, s.len--) {
		if (IS_WS(*s.s))
			continue;
		param = s;
		state = RMQP_NONE;
		IF_IS_PARAM("uri", RMQP_URI, value);
		IF_IS_PARAM("frames", RMQP_FRAME, value);
		IF_IS_PARAM("retries", RMQP_RETRY, value);
		IF_IS_PARAM("timeout", RMQP_TIMEOUT, value);
		IF_IS_PARAM("exchange", RMQP_EXCH, value);
		IF_IS_PARAM("heartbeat", RMQP_HBEAT, value);
		IF_IS_PARAM("immediate", RMQP_IMM, no_value);
		IF_IS_PARAM("mandatory", RMQP_MAND, no_value);
		IF_IS_PARAM("non-persistent", RMQP_NOPER, no_value);
		IF_IS_PARAM("receiving_queue", RMQP_RCVQ, value);
		IF_IS_PARAM("topic", RMQP_TOPIC, value);

		/* there is no known parameter here */
		goto no_value;

#undef IF_IS_PARAM

value:
		/* found a valid parameter - if uri has started, it should be ended */
		if (uri_pending) {
			suri.len -= param.len;
			uri_pending = 0;
		}
		/* skip spaces before = */
		for (; s.len > 0; s.s++, s.len--)
			if (!IS_WS(*s.s))
				break;
		if (s.len <= 0 || *s.s != '=') {
			LM_ERR("[%.*s] cannot find uri equal: %.*s\n", cid.len, cid.s,
					param.len, param.s);
			return -1;
		}
		s.s++;
		s.len--;
		param = s; /* start of the parameter */

no_value:
		/* search for the next ';' */
		for (; s.len > 0; s.s++, s.len--)
			if (*s.s == ';')
				break;
		if (state != RMQP_URI)
			param.len -= s.len;
		trim_len(param.len, param.s, param);

		/* here is the end of parameter  - handle it */
		switch (state) {
		case RMQP_URI:
			/* remember where the uri starts */
			suri = param;
			uri_pending = 1;
			break;
		case RMQP_NONE:
			/* we eneded up in a place that has ';' - if we haven't found
			 * the end of the uri, this is also part of the uri. otherwise it
			 * is an error and we shall report it */
			if (!uri_pending) {
				LM_ERR("[%.*s] Unknown parameter: %.*s\n", cid.len, cid.s,
						param.len, param.s);
				return -1;
			}
			break;
		case RMQP_FRAME:
			if (str2int(&param, (unsigned int *)&max_frames) < 0) {
				LM_ERR("[%.*s] frames must be a number: %.*s\n",
						cid.len, cid.s, param.len, param.s);
				return -1;
			}
			if (max_frames < RMQ_MIN_FRAMES) {
				LM_WARN("[%.*s] number of frames is %d - less than expected %d! "
						"setting to expected\n", cid.len, cid.s, max_frames, RMQ_MIN_FRAMES);
				max_frames = RMQ_MIN_FRAMES;
			} else {
				LM_DBG("[%.*s] setting frames to %d\n", cid.len, cid.s, max_frames);
			}
			break;
		case RMQP_HBEAT:
			if (str2int(&param, (unsigned int *)&heartbeat) < 0) {
				LM_ERR("[%.*s] heartbeat must be the number of seconds, not %.*s\n",
						cid.len, cid.s, param.len, param.s);
				return -1;
			}
			if (heartbeat < 0) {
				LM_WARN("[%.*s] invalid number of heartbeat seconds %d! Using default!\n",
						cid.len, cid.s, heartbeat);
				heartbeat = RMQ_DEFAULT_HEARTBEAT;
			} else {
				LM_DBG("[%.*s] setting heartbeat to %d\n", cid.len, cid.s, heartbeat);
			}
			break;
		case RMQP_RETRY:
			if (str2int(&param, (unsigned int *)&retries) < 0) {
				LM_ERR("[%.*s] retries must be a number, not %.*s\n",
						cid.len, cid.s, param.len, param.s);
				return -1;
			}
			if (retries < 0) {
				LM_WARN("[%.*s] invalid number of retries %d! Using default!\n",
						cid.len, cid.s, retries);
				retries = RMQ_DEFAULT_RETRIES;
			} else {
				LM_DBG("[%.*s] %d number of retries in case of error\n",
						cid.len, cid.s, heartbeat);
			}
			break;
		case RMQP_TIMEOUT:
			if (str2int(&param, (unsigned int *)&timeout) < 0) {
				LM_ERR("[%.*s] timeout must be a number, not %.*s\n",
						cid.len, cid.s, param.len, param.s);
				return -1;
			}
			break;
		case RMQP_IMM:
			flags |= RMQF_IMM;
			break;
		case RMQP_MAND:
			flags |= RMQF_MAND;
			break;
		case RMQP_NOPER:
			flags |= RMQF_NOPER;
			break;
		case RMQP_EXCH:
			exchange = param;
			LM_DBG("[%.*s] setting exchange '%.*s'\n", cid.len, cid.s,
					exchange.len, exchange.s);
			break;
		case RMQP_RCVQ:
			rcvq = param;
			LM_DBG("[%.*s] setting rcvq '%.*s'\n", cid.len, cid.s,
					rcvq.len, rcvq.s);
			break;
		case RMQP_TOPIC:
			topic = param;
			LM_DBG("[%.*s] setting topic '%.*s'\n", cid.len, cid.s,
					topic.len, topic.s);
		}
	}
	/* if we don't have an uri, we forfeit */
	if (!suri.s) {
		LM_ERR("[%.*s] cannot find an uri!", cid.len, cid.s);
		return -1;
	}
	/* trim the last spaces and ';' of the uri */
	trim_len(suri.len, suri.s, suri);
	if (suri.s[suri.len - 1] == ';')
		suri.len--;
	trim_len(suri.len, suri.s, suri);

	if ((srv = pkg_malloc(sizeof *srv + suri.len + 1)) == NULL) {
		LM_ERR("cannot alloc memory for rabbitmq server\n");
		return -1;
	}
	memset(srv, 0, sizeof *srv);
	uri = ((char *)srv) + sizeof *srv;
	memcpy(uri, suri.s, suri.len);
	uri[suri.len] = 0;

	if (rmq_parse(uri, &srv->uri) != 0) {
		LM_ERR("[%.*s] cannot parse rabbitmq uri: %s\n", cid.len, cid.s, uri);
		goto free;
	}

	if (srv->uri.ssl) {
		LM_WARN("[%.*s] we currently do not support ssl connections!\n", cid.len, cid.s);
		goto free;
	}

	if (exchange.len) {
		srv->exchange = amqp_bytes_malloc(exchange.len);
		if (!srv->exchange.bytes) {
			LM_ERR("[%.*s] cannot allocate echange buffer!\n", cid.len, cid.s);
			goto free;
		}
		memcpy(srv->exchange.bytes, exchange.s, exchange.len);
	} else
		srv->exchange = RMQ_EMPTY;

	if (rcvq.len) {
		srv->rcvq = amqp_bytes_malloc(rcvq.len);
		if (!srv->rcvq.bytes) {
			LM_ERR("[%.*s] cannot allocate echange buffer!\n", cid.len, cid.s);
			goto free;
		}
		memcpy(srv->rcvq.bytes, rcvq.s, rcvq.len);
	} else
		srv->rcvq = RMQ_EMPTY;

	if (topic.len) {
		srv->topic = amqp_bytes_malloc(topic.len);
		if (!srv->topic.bytes) {
			LM_ERR("[%.*s] cannot allocate echange buffer!\n", cid.len, cid.s);
			goto free;
		}
		memcpy(srv->topic.bytes, topic.s, topic.len);
	} else
		srv->topic = RMQ_EMPTY;

	srv->state = RMQS_OFF;
	srv->cid = cid;

	srv->flags = flags;
	srv->retries = retries;
	srv->max_frames = max_frames;
	srv->heartbeat = heartbeat;
	srv->ms_timeout = timeout;

	list_add(&srv->list, &rmq_servers);
	LM_DBG("[%.*s] new AMQP host=%s:%u timeout =%d ms\n", srv->cid.len, srv->cid.s,
			srv->uri.host, srv->uri.port,srv->ms_timeout);

	/* parse the url */
	return 0;
free:
	pkg_free(srv);
	return -1;
}
#undef IS_WS

/*
 * fixup function for rmq_server
 */
int fixup_rmq_server(void **param)
{
	*param = rmq_get_server((str*)*param);
	if (!(*param)) {
		LM_ERR("unknown connection id=%.*s\n",
			((str*)*param)->len, ((str*)*param)->s);
		return E_CFG;
	}

	return 0;
}

/*
 * function to connect all rmq servers
 */
void rmq_connect_servers(void)
{
	struct list_head *it;
	struct rmq_server *srv;

	list_for_each(it, &rmq_servers) {
		srv = container_of(it, struct rmq_server, list);
		if (rmq_reconnect(srv) < 0)
			LM_ERR("cannot connect to RabbitMQ server %s:%u\n",
					srv->uri.host, srv->uri.port);
	}
}

static inline int amqp_check_status(struct rmq_server *srv, int r, int* retry)
{
#ifndef AMQP_VERSION_v04
	if (r != 0) {
		LM_ERR("[%.*s] unknown AMQP error [%d] while sending\n",
				srv->cid.len, srv->cid.s, r);
		/* we close the connection here to be able to re-connect later */
		/* TODO: close the connection */
		return r;
	}
	return 0;
#else
	switch (r) {
		case AMQP_STATUS_OK:
			return 0;

		case AMQP_STATUS_TIMER_FAILURE:
			LM_ERR("[%.*s] timer failure\n", srv->cid.len, srv->cid.s);
			goto no_close;

		case AMQP_STATUS_NO_MEMORY:
			LM_ERR("[%.*s] no more memory\n", srv->cid.len, srv->cid.s);
			goto no_close;

		case AMQP_STATUS_TABLE_TOO_BIG:
			LM_ERR("[%.*s] a table in the properties was too large to fit in "
					"a single frame\n", srv->cid.len, srv->cid.s);
			goto no_close;

		case AMQP_STATUS_HEARTBEAT_TIMEOUT:
			LM_ERR("[%.*s] heartbeat timeout\n", srv->cid.len, srv->cid.s);
			break;

		case AMQP_STATUS_CONNECTION_CLOSED:
			LM_ERR("[%.*s] connection closed\n", srv->cid.len, srv->cid.s);
			break;

		/* this should not happened since we do not use ssl */
		case AMQP_STATUS_SSL_ERROR:
			LM_ERR("[%.*s] SSL error\n", srv->cid.len, srv->cid.s);
			break;

		case AMQP_STATUS_TCP_ERROR:
			LM_ERR("[%.*s] TCP error: %s(%d)\n", srv->cid.len, srv->cid.s,
					strerror(errno), errno);
			break;

		/* This is happening on rabbitmq server restart */
		case AMQP_STATUS_SOCKET_ERROR:
			LM_WARN("[%.*s] socket error: %s(%d)\n",
					srv->cid.len, srv->cid.s, strerror(errno), errno);
			break;

		default:
			LM_ERR("[%.*s] unknown AMQP error[%d]: %s(%d)\n",
					srv->cid.len, srv->cid.s, r, strerror(errno), errno);
			break;
	}
	/* we close the connection here to be able to re-connect later */
	rmq_close_server(srv);
no_close:
	if (retry && *retry > 0) {
		(*retry)--;
		return 1;
	}
	return r;
#endif
}

#define RMQ_ALLOC_STEP 2

int rmq_send(struct rmq_server *srv, str *rkey, str *body, str *ctype,
		int *names, int *values)
{
	int nr;
	int_str v;
	int ret = -1;
	amqp_bytes_t akey;
	amqp_bytes_t abody;
	amqp_basic_properties_t props;
	int retries = srv->retries;
	static int htable_allocated = 0;
	static amqp_table_entry_t *htable = NULL;
	struct usr_avp *aname = NULL, *aval = NULL;
	amqp_table_entry_t *htmp = NULL;

	akey.len = rkey->len;
	akey.bytes = rkey->s;
	abody.len = body->len;
	abody.bytes = body->s;
	memset(&props, 0, sizeof props);

	/* populates props based on the names and values */
	if (names && values) {
		/* count the number of avps */
		nr = 0;
		for (;;) {
			aname = search_first_avp(0, *names, &v, aname);
			if (!aname)
				break;
			if (nr >= htable_allocated) {
				htmp = pkg_realloc(htable, (htable_allocated + RMQ_ALLOC_STEP) *
						sizeof(amqp_table_entry_t));
				if (!htmp) {
					LM_ERR("out of pkg memory for headers!\n");
					return -1;
				}
				htable_allocated += RMQ_ALLOC_STEP;
				htable = htmp;
			}
			if (aname->flags & AVP_VAL_STR) {
				htable[nr].key.len = v.s.len;
				htable[nr].key.bytes = v.s.s;
			} else {
				htable[nr].key.bytes = int2str(v.n, (int *)&htable[nr].key.len);
			}
			aval = search_first_avp(0, *values, &v, aval);
			if (!aval) {
				LM_ERR("names and values number mismatch!\n");
				break;
			}
			if (aval->flags & AVP_VAL_STR) {
				htable[nr].value.kind = AMQP_FIELD_KIND_UTF8;
				htable[nr].value.value.bytes.bytes = v.s.s;
				htable[nr].value.value.bytes.len = v.s.len;
			} else {
				htable[nr].value.kind = AMQP_FIELD_KIND_I32;
				htable[nr].value.value.i32 = v.n;
			}
			LM_DBG("added key no. %d %.*s type %s\n", nr + 1,
					(int)htable[nr].key.len, (char *)htable[nr].key.bytes,
					(htable[nr].value.kind == AMQP_FIELD_KIND_UTF8 ? "string":"int"));
			nr++;
		}
		LM_DBG("doing a rabbitmq query with %d headers\n", nr);
		props.headers.entries = htable;
		props.headers.num_entries = nr;
		props._flags |= AMQP_BASIC_HEADERS_FLAG;
	}

	if (ctype) {
		props._flags |= AMQP_BASIC_CONTENT_TYPE_FLAG;
		props.content_type.len = ctype->len;
		props.content_type.bytes = ctype->s;
	}
	if (!(srv->flags & RMQF_NOPER)) {
		props.delivery_mode = 2;
		props._flags |= AMQP_BASIC_DELIVERY_MODE_FLAG;
	}

	do {
		if (rmq_reconnect(srv) < 0) {
			LM_ERR("[%.*s] cannot send RabbitMQ message\n",
					srv->cid.len, srv->cid.s);
			return ret;
		}

		ret = amqp_basic_publish(srv->conn, 1, srv->exchange, akey, \
				(srv->flags & RMQF_MAND), (srv->flags & RMQF_IMM),
				&props, abody);
		ret = amqp_check_status(srv, ret, &retries);
	} while (ret > 0);

	return ret;
}

int rabbitmq_process_mi(cJSON* j_obj,str *out_rpl_id,cJSON** j_output)
{
	cJSON *aux;
	str command = {NULL, 0};
	str params = {NULL, 0};
	struct mi_cmd *mi_f = NULL;
	struct mi_handler *hdl = NULL;

	mi_request_t req_item;
	mi_response_t *resp = NULL;

	aux = cJSON_GetObjectItem(j_obj, "name");

	if (aux == NULL || aux->type != cJSON_String || (command.s = aux->valuestring) == NULL) {
		LM_ERR("No command name received \n");
		return -1;
	}

	command.len = strlen(command.s);
	LM_INFO("XXXX - gotta run MI command %.*s\n",command.len,command.s);

	mi_f = lookup_mi_cmd(command.s,command.len);
	if (mi_f == NULL) {
		LM_ERR("Unrecognized MI command name %.*s\n",command.len,command.s);
		return -1;
	}

	LM_INFO("XXXX  - found it \n");
	memset(&req_item, 0, sizeof req_item);

	req_item.req_obj = cJSON_CreateObject();
	if (!req_item.req_obj) {
		LM_ERR("Failed to build temporary json request\n");
		goto error;
	}

	hdl = NULL;

	/* we might need params - check if any were provided */
	aux = cJSON_GetObjectItem(j_obj, "params");
	if (aux == NULL || aux->type != cJSON_Array || aux->child == NULL ) {
		LM_INFO("XXXX - no params \n");
	} else {
		LM_INFO("XXXX - got params\n");
		req_item.params = aux;
	}

	LM_INFO("XXX - running cmd \n");

	resp = handle_mi_request(&req_item, mi_f, hdl);
	LM_DBG("got mi response = [%p]\n", resp);

	req_item.params = NULL;

	if (resp == NULL) {
		LM_ERR("failed to process the command\n");
		goto error;
	}

	if (resp == MI_ASYNC_RPL) {
		*j_output = cJSON_CreateObject();
		if (req_item.req_obj)
			cJSON_Delete(req_item.req_obj);
		if (hdl) shm_free(hdl);
		return 0;
		
	}

	if (req_item.req_obj)
		cJSON_Delete(req_item.req_obj);

	*j_output = resp; 

	return 0;

error:
	if (req_item.req_obj)
		cJSON_Delete(req_item.req_obj);
	if (hdl) shm_free(hdl);
	return -1;
}

int rabbitmq_process_route(cJSON* j_obj,str *out_rpl_id,cJSON** j_output)
{
	struct usr_avp **old_avps=NULL;
	struct usr_avp *avps=NULL,*avp=NULL,*last_avp=NULL;
	struct sip_msg req;
	int route_idx,avp_id,ret=0;
	cJSON *aux,*child,*arr;
	str route_name = {NULL, 0};
	str avp_name=str_init("param");
	str ret_avp_name=str_init("ret");
	int_str is_val;

	aux = cJSON_GetObjectItem(j_obj, "name");

	if (aux == NULL || 
	aux->type != cJSON_String || 
	(route_name.s = aux->valuestring) == NULL) {
		LM_ERR("No route name received \n");
		return -1;
	}

	route_name.len = strlen(route_name.s);

	LM_DBG("ZZZZ - processing route %s \n",route_name.s);

	route_idx = get_script_route_ID_by_name( route_name.s, sroutes->request, RT_NO);
	if (route_idx==-1) {
		LM_ERR("route <%s> not defined in script\n",route_name.s);
		return -1;
	}

	aux = cJSON_GetObjectItem(j_obj, "params");
	if (aux == NULL || aux->type != cJSON_Object) {
		LM_DBG("ZZZ - no params \n");
		/* no params */
		avps = NULL;
	} else {
		child = aux->child;
		while (child) {
			LM_DBG("got new child %s with type %d\n",child->string,child->type);
			if (child->type != cJSON_Array) {
				LM_ERR("Found params entry which is not array \n");
				return -1;
			}
		
			avp_name.s=child->string;
			avp_name.len=strlen(avp_name.s);

			/* get an AVP name matching the param name */
			if (parse_avp_spec( &avp_name, &avp_id)<0) {
				LM_ERR("cannot get AVP ID for name <%.*s>, skipping..\n",
				avp_name.len, avp_name.s);
				return -1;
			}
		
			arr = child->child;
			while (arr) {
				if (arr->type != cJSON_String) {
					LM_ERR("Found params entry which is not string within array \n");
					return -1;
				}

				LM_INFO("XXXX - got value %s\n",arr->valuestring);

				is_val.s.s = arr->valuestring;
				is_val.s.len = strlen(is_val.s.s);

				if (avps == NULL) {
					avps = new_avp( AVP_VAL_STR, avp_id, is_val);
					if (avps == NULL) {
						LM_ERR("cannot get create new AVP name <%.*s>, skipping..\n",avp_name.len, avp_name.s);
						return -1;
					}

					avps->next = NULL;
				} else {
					avp = new_avp( AVP_VAL_STR, avp_id, is_val);
					if (avp == NULL) {
						LM_ERR("cannot get create new AVP name <%.*s>, skipping..\n",avp_name.len, avp_name.s);
						return -1;
					}

					for( last_avp=avps ; last_avp && last_avp->next ; last_avp=last_avp->next);
	
					last_avp->next = avp;
					avp->next = NULL;
				}
				
				arr=arr->next;
			}
			
			child=child->next;
		}

	}

	old_avps = set_avp_list( &avps );

	/* prepare a fake/dummy request */
	memset( &req, 0, sizeof(struct sip_msg));
	req.first_line.type = SIP_REQUEST;
	req.first_line.u.request.method.s= "DUMMY";
	req.first_line.u.request.method.len= 5;
	req.first_line.u.request.uri.s= "sip:user@domain.com";
	req.first_line.u.request.uri.len= 19;
	req.rcv.src_ip.af = AF_INET;
	req.rcv.dst_ip.af = AF_INET;

	/* route the notification route */
	set_route_type( REQUEST_ROUTE );
	run_top_route( sroutes->request[(int)(long)route_idx].a , &req);

	/* get RET AVP name matching the param name */
	if (parse_avp_spec( &ret_avp_name, &avp_id)<0) {
		LM_ERR("cannot get AVP ID for name <%.*s>, skipping..\n",
		avp_name.len, avp_name.s);
		return -1;
	}

	avp=search_first_avp(AVP_VAL_STR,avp_id,&is_val,NULL);
	if (avp == NULL) {
		LM_DBG("No reply from the route \n");
		ret = 0;
		goto cleanup;
	}
	
	LM_DBG("Got reply: %.*s\n",is_val.s.len,is_val.s.s);

	*j_output = cJSON_Parse(is_val.s.s);
	if (*j_output == NULL) {
		LM_ERR("Failed to parse json return \n");
		ret = -1;
	}
	
cleanup:
	/* cleanup over route execution */
	set_avp_list( old_avps );
	free_sip_msg( &req );

	/* destroy everything */
	destroy_avp_list( &avps );

	return ret;
}

typedef int (*rabbit_process_f) (cJSON* j_obj,str *out_rpl_id,cJSON** j_output);

typedef struct rabbitmq_command_processing {
	char *name;
	int len;
	rabbit_process_f process; 
} rabbitmq_command_processing_t;

rabbitmq_command_processing_t rabbit_commands[] = {
	{"MI", 		2,	rabbitmq_process_mi	},
	{"ROUTE",	5,	rabbitmq_process_route	},
	{0,0,0}
};

static int process_json_cmd(char * body,int len,str *out_rpl_id,cJSON** j_output)
{
	cJSON *j_obj = NULL,*aux,*aux2;
	str command,out_entry_id;
	rabbitmq_command_processing_t *entry;
	int ret=0,retries,htable_entries=0;
	struct rmq_server *out_srv;
	static amqp_table_entry_t htable[16];
	char *p=NULL;

	*j_output = NULL;
	
	amqp_bytes_t akey;
	amqp_bytes_t abody;
	amqp_basic_properties_t props;

	LM_INFO("RMQ:RECEIVER: Processing %s\n",body);

	j_obj = cJSON_Parse(body);
	if (j_obj == NULL) {
		LM_ERR("Failed to parse JSON obj \n");
		return -1;
	}

	aux = cJSON_GetObjectItem(j_obj, "type");
	if (aux == NULL || aux->type != cJSON_String || (command.s = aux->valuestring) == NULL) {
		LM_ERR("No command type received \n");
		ret = -1;
		*j_output = NULL;
		goto err_free;
	}

	command.len = strlen(command.s);
	LM_INFO("XXX - got command [%.*s]\n",command.len,command.s);

	/* look it up */
	for(entry=rabbit_commands;entry && entry->name ; entry++)
		if (memcmp(entry->name,command.s,entry->len) == 0)
			break;

	if (entry == NULL || entry->name == NULL) {
		LM_ERR("Unsupported command %.*s\n",command.len,command.s);
		ret = -1;
		*j_output = NULL;
		goto err_free;
	}

	ret = entry->process(j_obj,out_rpl_id,j_output);
	if (ret < 0) {
		LM_ERR("Failed to process Rabbit command \n");
		goto err_free;
	}

	LM_INFO("Got reply from function %s\n",cJSON_Print(*j_output));

	aux = cJSON_GetObjectItem(j_obj, "get_output");
	if (aux == NULL || aux->type != cJSON_String || (out_entry_id.s = aux->valuestring) == NULL) {
		LM_INFO("No out reply requested \n");
		*j_output = NULL;
		goto ok_free;
	}

	out_entry_id.len = strlen(out_entry_id.s);
	out_srv = rmq_get_server(&out_entry_id);
	if (!out_srv) {
		LM_ERR("cannot find a RabbitMQ server connection for %.*s\n",
			out_entry_id.len,out_entry_id.s);
		goto ok_free;
	}

	LM_ERR("Reply wanted on %.*s\n",out_srv->cid.len,out_srv->cid.s);

	if (out_srv->exchange.bytes) {
		akey = out_srv->exchange;
	} else {
		akey.len = 4;
		akey.bytes = "test";
	}

	LM_ERR("Reply going to key %.*s\n",(int)akey.len,(char *)akey.bytes);

	p = cJSON_Print(*j_output);
	if (p == NULL) {
		LM_ERR("No more pkg :( \n");
		goto err_free;
	}

	LM_INFO("XXXX - got out %s\n",p);
	cJSON_Minify(p);
	LM_INFO("XXXX - minified to %s\n",p);

	abody.len = strlen(p);
	abody.bytes = p;

	LM_INFO("Publishing info %.*s\n",(int)strlen(p),p);
	memset(&props, 0, sizeof props);

	props._flags |= AMQP_BASIC_CONTENT_TYPE_FLAG;
	props.content_type.len = 16;
	props.content_type.bytes = "application/json";

	aux = cJSON_GetObjectItem(j_obj, "headers_output");
	if (aux == NULL || aux->type != cJSON_Object) {
		props.headers.num_entries = 1;
		htable[0].key.len = 8;
		htable[0].key.bytes = "RMQREPLY";
		htable[0].value.kind = AMQP_FIELD_KIND_UTF8;

		htable[0].value.value.bytes.len = len;
		htable[0].value.value.bytes.bytes = body;
	} else {
		LM_INFO("XXX - should reply headers , type = %d\n",aux->type);

		htable_entries = 0;
		aux2 = aux->child;
		LM_INFO("XXX - aux2 = %p \n",aux2);
		while (aux2) {
			if (aux2->type == cJSON_String) {
				htable[htable_entries].key.bytes = aux2->string; 
				htable[htable_entries].key.len = strlen(aux2->string);

				htable[htable_entries].value.kind = AMQP_FIELD_KIND_UTF8;
				htable[htable_entries].value.value.bytes.bytes = aux2->valuestring;
				htable[htable_entries].value.value.bytes.len = strlen(aux2->valuestring);

				htable_entries++;
			} else if (aux2->type == cJSON_Number) {
				htable[htable_entries].key.bytes = aux2->string; 
				htable[htable_entries].key.len = strlen(aux2->string);

				htable[htable_entries].value.kind = AMQP_FIELD_KIND_I32;
				htable[htable_entries].value.value.i32 = aux2->valueint;

				htable_entries++;
			} else {
				LM_ERR("XXX - unsupported type %d\n",aux2->type);
			}

			aux2 = aux2->next;
		}	

		props.headers.num_entries = htable_entries;
	}

	props._flags |= AMQP_BASIC_HEADERS_FLAG;
	props.headers.entries = htable;
	
	LM_INFO("XXX - populated props \n");
	retries = out_srv->retries;

	do {
		if (rmq_reconnect(out_srv) < 0) {
			LM_ERR("[%.*s] cannot send RabbitMQ message\n",
					out_srv->cid.len, out_srv->cid.s);
			goto err_free;
		}

		ret = amqp_basic_publish(out_srv->conn, 1, out_srv->exchange, akey, \
				(out_srv->flags & RMQF_MAND), (out_srv->flags & RMQF_IMM),
				&props, abody);
		ret = amqp_check_status(out_srv, ret, &retries);
	} while (ret > 0);
ok_free:
//	if (*j_output != NULL) {
//		cJSON_Delete(*j_output);
//	}

	if (p)
		pkg_free(p);

	cJSON_Delete(j_obj);
	return 0;

err_free:
//	if (*j_output != NULL) {
//		cJSON_Delete(*j_output);
//	}
	if (p)
		pkg_free(p);

	cJSON_Delete(j_obj);
	return ret;
}

void rabbitmq_receiver_process(int rank)
{
	struct list_head *it;
	struct rmq_server *entry=NULL;
	int pos=0;
	amqp_basic_consume_ok_t *result;
	amqp_queue_declare_ok_t *q_res;
	amqp_envelope_t envelope;
	amqp_rpc_reply_t rpc_reply;
	amqp_bytes_t queue_bytes;
	str out_rpl_id;
	cJSON *j_output=NULL;

	list_for_each(it, &rmq_servers) {
		entry = container_of(it, struct rmq_server, list);
		if (entry->rcvq.len == 0)
			continue;

		pos++; 

		if (pos == process_no)
			break;
	}

	LM_INFO("XXXX - we are taking care of %.*s\n",entry->cid.len,entry->cid.s);

	
	while (rmq_reconnect(entry) < 0) {
		LM_ERR("[%.*s] cannot reconnect to RabbitMQ\n",
				entry->cid.len, entry->cid.s);
		usleep(500000);
	}

	/* TODO - what if no exchange ? */

	queue_bytes = entry->rcvq;
	
	LM_INFO("XXX - listening on queue [%.*s] ; topic [%.*s] on rmq [%s] \n",(int)queue_bytes.len,(char *)queue_bytes.bytes,
				(int)entry->topic.len,(char *)entry->topic.bytes,entry->uri.host);
	q_res = amqp_queue_declare(entry->conn, 1, queue_bytes, 0, 0, 0, 0, amqp_empty_table);
	if (q_res == NULL) {
		if (rmq_error("Declaring queue", amqp_get_rpc_reply(entry->conn)))
			return; 
	}

	LM_INFO("XXXX - fetched %.*s\n",(int)q_res->queue.len,(char *)q_res->queue.bytes);

	if (!amqp_queue_bind(entry->conn, 1, queue_bytes, 
	entry->exchange,entry->topic.bytes ? entry->topic : entry->exchange,amqp_empty_table)) {
		LM_ERR("Failed to bind to queue \n");
		return;
	}

	result = amqp_basic_consume(entry->conn, 1, queue_bytes,
	amqp_empty_bytes,0,1,0,amqp_empty_table);

	if (result == NULL) {
		LM_ERR("No result after basic consume :( \n");
		return;
	}

	LM_INFO("now pooling on connection %.*s \n",entry->cid.len, entry->cid.s);

	while (1) {
		if (rmq_reconnect(entry) < 0) {
			LM_ERR("[%.*s] cannot reconnect to RabbitMQ\n",
					entry->cid.len, entry->cid.s);
			return;
		}

		amqp_maybe_release_buffers(entry->conn);

		rpc_reply = amqp_consume_message(entry->conn, &envelope, NULL, 0);
		if (rpc_reply.reply_type != AMQP_RESPONSE_NORMAL) {
			LM_ERR("Not normal :( - %d \n",rpc_reply.reply_type);
			continue;
		}

		LM_DBG("Delivery %u, exchange %.*s routingkey %.*s\n",
		(unsigned)envelope.delivery_tag,
		(int)envelope.exchange.len, (char *)envelope.exchange.bytes,
		(int)envelope.routing_key.len, (char *)envelope.routing_key.bytes);

		if (envelope.message.properties._flags & AMQP_BASIC_CONTENT_TYPE_FLAG) {
			if (memcmp(
			(char *)envelope.message.properties.content_type.bytes,
			"application/json",16) != 0) {

				LM_ERR("Invalid content type %.*s\n",
					(int)envelope.message.properties.content_type.len,
					(char *)envelope.message.properties.content_type.bytes);
				amqp_destroy_envelope(&envelope);
				continue;
			}
		}

		LM_INFO("DBG: %.*s\n",
			(int)envelope.message.body.len, 
			(char *)envelope.message.body.bytes);

		
		pos = process_json_cmd((char *)envelope.message.body.bytes,
			(int)envelope.message.body.len,&out_rpl_id,&j_output);
		if (pos < 0) {
			LM_ERR("Failed to process JSON body %.*s\n",
				(int)envelope.message.body.len, 
				(char *)envelope.message.body.bytes);
		}

		amqp_destroy_envelope(&envelope);
	}
}

int count_listener_processes(void)
{
	struct list_head *it;
	struct rmq_server *srv;
	int i=0;

	list_for_each(it, &rmq_servers) {
		srv = container_of(it, struct rmq_server, list);
		if (srv->rcvq.len > 0)
			i++;
	}

	return i;
}
